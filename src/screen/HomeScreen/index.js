import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, ImageBackground, KeyboardAvoidingView, TextInput } from 'react-native';
import { styles } from './styles';
import { Artist_TabEvents } from '../../components/Artist_TabEvents'
import { ArtistTabVideo } from '../../components/ArtistTabVideo'
import {  SimilarEvent } from '../../components/SimilarEvent'
import { Similar_artist } from '../../components/Similar_artist'
import {  Genres_round_view } from '../../components/Genres_round_view'
import {  GenreOptionBtn } from '../../components/GenreOptionBtn'
import {  } from '../../components/'
import { Images } from '../../common/Images'
import Foundation from 'react-native-vector-icons/dist/Foundation';
import AntDesign from 'react-native-vector-icons/dist/AntDesign';
import { scrollInterpolator, animatedStyles } from '../../Utils/animations';
import Carousel from 'react-native-snap-carousel';
const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.9);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 1.2);

export default class HomeScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            musicOff: true,
            CardList: [
                {
                    album_name: 'Single Ladies ',
                    singer_name: 'Beyonce',
                },
                {
                    album_name: 'Single Ladies ',
                    singer_name: 'Beyonce'
                },
                {
                    album_name: 'Single Ladies ',
                    singer_name: 'Beyonce'
                },
                {
                    album_name: 'Single Ladies ',
                    singer_name: 'Beyonce',
                },
                {
                    album_name: 'Single Ladies ',
                    singer_name: 'Beyonce'
                },
                {
                    album_name: 'Single Ladies ',
                    singer_name: 'Beyonce'
                },
            ],
            artistData: [
                {
                    'artistName': 'Taylor Swift',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://schnncdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Taylor_Swift_003_20200226074119_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Martin Garrix',
                    'eventName': 'Summer Tour',
                    'artistProfileImage': 'https://sklktcdnems01.cdnsrv.jio.com/c.saavncdn.com/artists/Martin_Garrix_004_20200303120820_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'David Guetta',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://sklktcdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/David_Guetta_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Selena Gomez',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://sklktecdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Selena_Gomez_002_20200226073835_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Demi Lovato',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://schnncdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Demi_Lovato_002_20200312120805_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Slash',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://snoidcdnems01.cdnsrv.jio.com/c.saavncdn.com/607/You-re-A-Lie-English-2012-500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
            ],
            ShowTitleData: [
                { songName: 'Single Ladies', singerName: 'Beyonce' },
                { songName: 'Halo', singerName: 'Beyonce' },
                { songName: 'XO', singerName: 'Beyonce' },
                { songName: 'Baby Boy', singerName: 'Beyonce' },
                { songName: 'Ring the Alarm', singerName: 'Beyonce' },
                { songName: 'Sorry', singerName: 'Beyonce' },
            ],
            genreOption: [
                { optionName: 'All', isSelectoption: 1 },
                { optionName: 'Pop' },
                { optionName: 'Electronic' },
            ],
            featureFanList:[
                {name:'Alice Janes', address:'Minnessota, US',img_url:'https://sklktecdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Selena_Gomez_002_20200226073835_500x500.jpg'},
                {name:'Paul Pogba', address:'Nashville, US',img_url:'https://sklktcdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/David_Guetta_500x500.jpg'},
                {name:'Alice Janes', address:'Minnessota, US',img_url:'https://schnncdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Demi_Lovato_002_20200312120805_500x500.jpg'},
                {name:'Alice Janes', address:'Minnessota, US',img_url:'https://sklktcdnems01.cdnsrv.jio.com/c.saavncdn.com/artists/Martin_Garrix_004_20200303120820_500x500.jpg'},
            ]
        }
        global.myNavigation = this.props.navigation
        global.selectedRoom = 'Global Chat'
        global.isAdmin = false
    }

    componentDidMount = () => {
        setTimeout(() => {
            this.timer_count()
        }, 10000)
    }

    handelValue = (value) => {
        this.setState({ SelectedValue: value })
        global.selectedRoom = value
    }


    timer_count = () => {
        if (this.state.timer_counting == false) {
            this.setState({ timer_counting: true }, () => {
                this.interval = setInterval(
                    () => this.setState((prevState) => ({ timer: prevState.timer - 1 })),
                    1000
                );
            });
        }
    }

    componentDidUpdate() {
        if (this.state.timer === 0) {
            clearInterval(this.interval);
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }
    check_login = () => {
        if (this.state.login === true) {
            alert("you have successfully bought ticket");
            this.setState({ bought_ticket: true });
            this.RBSheet.close();
            this.timer_count()
        } else {
            alert("Please Login");
        }
    }
    buying_ticket = () => {
        alert("you have successfully bought ticket");
        this.setState({ bought_ticket: true });
        this.RBSheet.close();
    }
    loginInfo = () => {
        alert("you have successfully logged in");
        this.setState({ login: true });
        this.RBSheet.close();
    }

    openSheet = () => {
        this.RBSheet.open();
    }

    closeSheet = () => {
        this.RBSheet.close();
    }

    openChat = () => {
        // this.RBSheet_chat.open();
        this.props.navigation.navigate('ChatRoom')
    }

    closeChat = () => {
        this.RBSheet_chat.close();
    }
    past_event_tab_view = () => {
        this.setState({ past_event_tab: true, video_tab: false, gellery_tab: false });
        // this.setState({ video_tab: true });
        // this.setState({ gellery_tab: true });
    }
    video_tab_view = () => {
        this.setState({ video_tab: true, past_event_tab: false, gellery_tab: false });
    }
    gellery_tab_view = () => {
        this.setState({ gellery_tab: true, past_event_tab: false, video_tab: false });
    }
    buy_album_tab_view = () => {
        this.setState({ buy_album_tab: true, merchandise: false });
    }
    Merchandise_tab_view = () => {
        this.setState({ merchandise: true, buy_album_tab: false });
    }
    changeRoom = () => {

    }
    addRoom = () => {

    }
    volumProcess = () => {
        this.setState({ musicOff: !this.state.musicOff })
    }
    _renderItem = ({ item, index }) => {
        return (
            <ImageBackground source={Images.lady_gaga_concer} imageStyle={{ width: '100%', }} style={{ flex: 1, justifyContent: "center", alignItems: 'center' }}>
                <View style={styles.singer_name_event_name_view}>
                    <Text style={styles.singer_name_event_name_text_style}>{item.album_name} • {item.singer_name}</Text>
                </View>
            </ImageBackground>
        );
    }
    render() {
        return (
            <KeyboardAvoidingView behaviour="height" style={styles.main_container}>
                <ScrollView contentContainerStyle={styles.scrlView_container}>
                    <View style={styles.header_container_style}>
                        <TouchableOpacity style={styles.header_user_img_style}></TouchableOpacity>
                    </View>
                    <ImageBackground source={Images.mainBackground} style={styles.img_bg_style}>
                        <View style={styles.Show_name_and_sound}>
                            <TouchableOpacity onPress={() => this.volumProcess()} style={styles.sound_icon_view}>
                                <Foundation name={this.state.musicOff === false ? 'volume-strike' : 'volume'} size={30} color='#ddd'></Foundation>
                            </TouchableOpacity>
                            <View style={styles.Show_title_view}>
                                <Text style={styles.title_text_style}>The Metallica Show</Text>
                            </View>
                            <View style={styles.go_btn_view}>
                                <TouchableOpacity style={styles.go_btn_round_style}>
                                    <AntDesign name='arrowright' size={15} color='#ddd'></AntDesign>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.genre_type_text_view}>
                            <Text style={styles.genre_type_text_style}>Rock • Pop • Punk Rock</Text>
                        </View>
                        <View style={styles.Video_view_slide}>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                <View style={styles.video_thumb_n_name_container}>
                                    <View style={styles.thumb_img_style}>
                                    </View>
                                    <Text style={styles.thumb_title_text_style}>The Metallica Show</Text>
                                    <View style={styles.watched_video_line}>
                                        <View style={{ height: 2, width: '10%', backgroundColor: '#fff', borderTopLeftRadius: 30, borderBottomLeftRadius: 30 }}>
                                        </View>
                                    </View>
                                </View>

                                <View style={styles.video_thumb_n_name_container}>
                                    <View style={styles.thumb_img_style}>
                                    </View>
                                    <Text style={styles.thumb_title_text_style}>The Metallica Show</Text>
                                    <View style={styles.watched_video_line}>
                                        <View style={{ height: 2, width: '30%', backgroundColor: '#fff', borderTopLeftRadius: 30, borderBottomLeftRadius: 30 }}>
                                        </View>
                                    </View>
                                </View>

                                <View style={styles.video_thumb_n_name_container}>
                                    <View style={styles.thumb_img_style}>
                                    </View>
                                    <Text style={styles.thumb_title_text_style}>The Metallica Show</Text>
                                    <View style={styles.watched_video_line}>
                                        <View style={{ height: 2, width: '90%', backgroundColor: '#fff', borderTopLeftRadius: 30, borderBottomLeftRadius: 30 }}>
                                        </View>
                                    </View>
                                </View>

                                <View style={styles.video_thumb_n_name_container}>
                                    <View style={styles.thumb_img_style}>
                                    </View>
                                    <Text style={styles.thumb_title_text_style}>The Metallica Show</Text>
                                    <View style={styles.watched_video_line}>
                                        <View style={{ height: 2, width: '50%', backgroundColor: '#fff', borderTopLeftRadius: 30, borderBottomLeftRadius: 30 }}>
                                        </View>
                                    </View>
                                </View>

                                <View style={styles.video_thumb_n_name_container}>
                                    <View style={styles.thumb_img_style}>
                                    </View>
                                    <Text style={styles.thumb_title_text_style}>The Metallica Show</Text>
                                    <View style={styles.watched_video_line}>
                                        <View style={{ height: 2, width: '10%', backgroundColor: '#fff', borderTopLeftRadius: 30, borderBottomLeftRadius: 30 }}>
                                        </View>
                                    </View>
                                </View>

                                <View style={styles.video_thumb_n_name_container}>
                                    <View style={styles.thumb_img_style}>
                                    </View>
                                    <Text style={styles.thumb_title_text_style}>The Metallica Show</Text>
                                    <View style={styles.watched_video_line}>
                                        <View style={{ height: 2, width: '10%', backgroundColor: '#fff', borderTopLeftRadius: 30, borderBottomLeftRadius: 30 }}>
                                        </View>
                                    </View>
                                </View>
                            </ScrollView>
                        </View>
                        <View style={styles.upcoming_event_title_view}>
                            <Text style={styles.upcoming_event_text_style}>Upcoming Events</Text>
                            <Text style={styles.see_all_text_style}>See All</Text>
                        </View>
                    </ImageBackground>
                    <View style={{ flexDirection: 'row', marginLeft: 16 }}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <SimilarEvent artistData={this.state.artistData} />
                        </ScrollView>
                    </View>

                    <View style={styles.slider_event_that_broke_internet_view}>
                        <Text style={styles.event_that_broke_internet_text_style}>Events that broke the internet</Text>
                        <View style={{ marginTop: 16, }}>
                            <Carousel
                                ref={(c) => this.carousel = c}
                                data={this.state.CardList}
                                renderItem={this._renderItem}
                                sliderWidth={SLIDER_WIDTH}
                                itemWidth={ITEM_WIDTH}
                                inactiveSlideShift={0}
                                onSnapToItem={index => this.setState({ activeIndex: index })}
                                scrollInterpolator={scrollInterpolator}
                                slideInterpolatedStyle={animatedStyles}
                                useScrollView={true}
                            />

                        </View>
                    </View>
                    <View style={styles.event_for_you_text_view}>
                        <Text style={styles.event_for_you_text_style}>Events for you</Text>
                        <Artist_TabEvents ShowTitleData={this.state.ShowTitleData} />
                    </View>
                    <View style={styles.similat_atist_view}>
                        <Similar_artist />
                    </View>
                    <ImageBackground source={Images.mainBackground} style={styles.img_bg_style}>
                        <Text style={styles.img_background_two_artist_text}>Bruno Mar’s</Text>
                        <Text style={styles.img_background_two_event_text}>Finesse World Tour…</Text>
                        <Text style={styles.img_background_two_event_time_text}>Aug 25  4:30 PM (PST)</Text>
                        <TouchableOpacity style={styles.img_background_two_buy_ticket_style}>
                            <Text style={styles.img_background_two_buy_ticket__text_style}>Buy Tickets</Text>
                        </TouchableOpacity>
                    </ImageBackground>
                    <View style={styles.explor_the_genre_view}>
                        <Genres_round_view homeScreenUse={true} />
                    </View>
                    <View style={styles.explor_gigs_view}>
                        <Text style={styles.upcoming_event_text_style}>Explore Gigs</Text>
                        <View style={styles.genre_option_btn_view}>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ marginLeft: 16,width:'100%' }}>
                                <GenreOptionBtn genreOption={this.state.genreOption} />
                            </ScrollView>
                            <View style={{ marginTop: 17 }}>
                            <ArtistTabVideo ShowTitleData={this.state.ShowTitleData} />
                            </View>
                        </View>
                    </View>
                    <View style={styles.featured_Fans_view}>
                        <Text style={styles.upcoming_event_text_style}>Featured Fans</Text>
                        <View style={{flexDirection:'row',marginLeft:40,marginTop:28}}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} >
                        {this.state.featureFanList.map((item, index) => {
                            return (
                                <View style={styles.featureFanView}>
                                    <Image source={{uri:item.img_url}} style={styles.featureFanUserImg}></Image>
                                    <View style={{justifyContent:'center',alignItems:'center'}}>
                                        <Text style={styles.feature_Fan_name_text_style}>{item.name}</Text>
                                        <Text style={styles.feature_Fan_address_text_style}>{item.address}</Text>
                                    </View>
                              </View>
                            )
                        })}
                        </ScrollView>
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }
}


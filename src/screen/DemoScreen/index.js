import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, ImageBackground, KeyboardAvoidingView, TextInput } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
export default class DemoScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            step: 1
        }
    }
    handelNavigation = (index) => {
        this.props.navigation.navigate(index)
    }
    render() {
        const { step } = this.state
        return (
            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, }}>
                <LinearGradient colors={['#08203e', '#557c93',]} style={{opacity: 1,flex: 1,width: '100%',justifyContent: 'center',alignItems: 'center'}}>
                <ScrollView contentContainerStyle={{justifyContent:'center',alignItems:'center'}} style={{flexGrow:1}}>
                <View style={{ justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{fontSize:35,color:'#ddd',marginVertical:50,marginTop:150}}>Name of screen</Text>
                    <TouchableOpacity onPress={() => {
                        this.handelNavigation('EventDetailScreen')}}>
                        <Text style={{ fontSize: 30, color: '#ddd', marginVertical: 15 }}>EventDetail Screen</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        this.handelNavigation('TicketFlowScreen')}}>
                        <Text style={{ fontSize: 30, color: '#ddd', marginVertical: 15 }}>TicketingScreen Screen</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        this.handelNavigation('ArtistScreen')}}>
                        <Text style={{ fontSize: 30, color: '#ddd', marginVertical: 15 }}>ArtistScreen Screen</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        this.handelNavigation('ProfileScreen')}}>
                        <Text style={{ fontSize: 30, color: '#ddd', marginVertical: 15 }}>ProfileScreen Screen</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        this.handelNavigation('HomeScreen')}}>
                        <Text style={{ fontSize: 30, color: '#ddd', marginVertical: 15 }}>HomeScreen Screen</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        this.handelNavigation('ArtistListingScreen')}}>
                        <Text style={{ fontSize: 30, color: '#ddd', marginVertical: 15 }}>ArtistListingScreen Screen</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        this.handelNavigation('EventListingScreen')}}>
                        <Text style={{ fontSize: 30, color: '#ddd', marginVertical: 15 }}>EventtListingScreen Screen</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
    </LinearGradient>
            </View>
        );
    }
}
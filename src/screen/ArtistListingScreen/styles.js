import { StyleSheet, Dimensions } from 'react-native';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import { Fonts } from '../../common/fonts';
export const styles = StyleSheet.create({
    scrlView_container: {
        flexGrow: 1,

    },
    main_container: {
        flex: 1,

    },
    body_contianer: {
        backgroundColor: '#101113',
    },
    header_container_style: {
        backgroundColor: 'transparent',
        position: 'absolute',
        backgroundColor: 'transparent',
        zIndex: 100,
        top: 0,
        left: 0,
        right: 0,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0,
        marginTop: 10
    },
    header_user_img_style: {
        width: 24,
        height: 24,
        borderRadius: 24 / 2,
        backgroundColor: '#ddd',
        marginLeft: 16,
        marginVertical: 12
    },
    img_bg_style: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',

    },
    artist_dp_view: {
        marginTop: 55,
        marginBottom: 0
    },
    dp_ring_view: {
        width: 274, height: 276, justifyContent: 'center', alignItems: 'center'
    },
    dp_inside_ring_view: {
        width: 248, height: 248, borderRadius: 248 / 2, justifyContent: 'center', alignItems: 'center', backgroundColor: '#7070708c'
    },
    artist_img_style: {
        resizeMode: 'contain', width: 248, height: 248, borderRadius: 248 / 2
    },
    three_dot_view: {
        flexDirection: 'row',
        marginTop: 15
    },
    slide_round_view: {
        width: 8,
        height: 8,
        borderRadius: 8 / 2,
        borderColor: '#c4c4c4',
        borderWidth: 0.5,
        marginRight: 10
    },
    artist_title_view: {
        marginTop: 29,
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        marginLeft: 16
    },
    artis_with_new_event_text_style: {
        fontFamily: Fonts.OpenSans_Light,
        fontSize: 14,
        color: '#ddd',
    },
    artis_name_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 18,
        color: '#d6d6d6'
    },
    main_view: {
        flex: 2,
        backgroundColor: '#101012',
        paddingBottom: 100
    },
    count_follow_view: {
        flexDirection: 'row',
        marginTop: 24,
        marginHorizontal: 17
    },
    count_box_view: {
        flex: 1
    },
    count_box_title_text_style: {
        fontFamily: Fonts.OpenSans_Light,
        fontSize: 12,
        color: '#ddd',
    },
    count_box_count_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 18,
        color: '#d6d6d6',
        marginTop: 8
    },
    know_more_view: {
        marginTop: 16,
        marginLeft: 16,
        flexDirection: 'row',
        alignSelf: 'flex-start'
    },
    know_more_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 18,
        color: '#ddd',
        marginRight: 16
    },
    right_btn_view: {
        width: 32,
        height: 32,
        borderRadius: 32 / 2,
        backgroundColor: '#242527',
        justifyContent: 'center',
        alignItems: 'center'
    },
    tab_view_container: {
        marginTop: 32,
        marginLeft: 16
    },
    tab_title_text_style: {
        fontFamily: Fonts.OpenSans_Light,
        fontSize: 16,
        color: '#ddd',
        marginBottom: 25
    },
    top_artist_view: {
        flexDirection: 'row'
    },
    View_title_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 18,
        color: '#d6d6d6',
        marginLeft: 16,
        marginBottom: 16,
        marginTop: 24
    },
    top_artist_name_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        color: '#d6d6d6',
    },
    top_artits_bright_ring_img_style: {
        height: 161,
        width: 161,
        justifyContent: 'center',
        alignItems: 'center'
    },
    top_artist_entity_view: {
        justifyContent: 'center',
        alignItems: 'center',

    },
    top_artist_img_style: {
        height: 116,
        width: 116,
        borderRadius: 116 / 2,
        marginBottom: 3
    },
    artist_you_follow_entity_view: {
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 20
    },
    Artists_on_Gigs_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 24,
        color: '#d6d6d6',
        marginLeft: 16,
        marginBottom: 16,
        marginTop: 24
    },
    Showing_Results_text_style: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 14,
        color: '#777',
        marginLeft: 16,
        marginBottom: 16,
        marginTop: 8
    },
    sorting_btn_view: {
        flexDirection: 'row',
        marginLeft: 16,
        marginTop: 16
    },
    filter_btn_style: {
        borderWidth: 1,
        borderColor: '#fff',
        borderRadius: 7,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        marginRight: 8
    },
    Filter_title_btn_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 16,
        color: '#ddd',
        marginRight: 16
    },
    icon_btn_sorting_style: {
        marginVertical: 8,
        marginRight: 8,
        marginLeft: 16
    },
    artist_result_img_style: {
        height: 160,
        width: 160,
        borderRadius: 160 / 2,
        marginBottom: 3
    },
    linearGradient: {
        borderRadius: 10,
        marginHorizontal: 16,
        marginTop:20
    },
    inside_linear_view:{
        flexDirection:'row',
        margin:16,
        marginBottom:19,
        justifyContent:'space-between',
        alignItems:'center'
    },
    linear_text_data_view:{
    },
    artist_with_new_event_text_style_box:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#777',
        marginBottom:5
    },
    artist_name_text_style_box:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 24,
        color: '#ddd',
        marginBottom:16
    },
    linear_img_data_view:{
        justifyContent:'center',
        alignItems:'center'
    },
    /// rbsheet Filter
    filter_main_view_RB:{
        flex:1,
        
    },
    title_view_filter_RB:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    Filters_text_title_RB:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 24,
        color: '#ddd',
        marginLeft:16,
        marginBottom:16
    },
    genre_n_anime_view:{
        backgroundColor:'#252628',
        flexDirection:'row',
        justifyContent:'flex-start',
        alignContent:'center'
    },
    genre_text_view:{
        flex:1,
        justifyContent:'flex-start',
        alignSelf:'flex-start',
        alignItems:'center',
        borderRightWidth:1,
        borderColor:'#fff'
    },
    Genre_text_style:{
        marginLeft:16,
        marginVertical:9,
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 16,
        color: '#ddd',
        alignSelf:'flex-start'
    },
    Anime_text_view:{
        flex:2,
        justifyContent:'flex-start',
        
    },
    Anime_text_style:{
        marginLeft:24,
        marginVertical:9,
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 14,
        color: '#d4d5d5',
    },
    category_n_list_view:{
        flex:1,
        flexDirection:'row'
    },
    category_view:{
        flex:1,
        justifyContent:'flex-start',
        alignItems:'center',
        backgroundColor:'#212225'
    },
    category_btn_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 16,
        color: '#777',
        marginVertical:9,
        marginLeft:16
    },
    category_btn_view:{
        justifyContent:'flex-start',width:'100%',
    },
    list_view:{
        flex:3,
        justifyContent:'space-between',
        backgroundColor:'#252628'
    },
    list_btn_view:{
        justifyContent:'space-between',
        alignItems:'center',
        width:'100%',
        flexDirection:'row'
    },
    apply_n_reset_view:{
        borderTopWidth:1,
        borderColor:'#777'
    },
    Showing_Results_text_style_RB:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#777',
        margin:8,
        alignSelf:'flex-end'
    },
    apply_reset_btn_RB_view:{
        // width:width -50,
        flexDirection:'row',
        // backgroundColor:'red'
    },
    ///rb sheet done
    // pop_up_sorting
    sort_box_view:{
        borderRadius:10,
        backgroundColor:'#252628',
        width:214
    },
    A_z_text_style_view:{
        justifyContent:'flex-start',
        alignItems:'center',
        backgroundColor:'red',
        borderRadius:10
    },
    A_z_text_style:{
        alignSelf:'flex-start',
        marginVertical:9,
        marginLeft:16,
        color:'#fff'
    },
    ///
    uh_oh_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 24,
        color: '#ddd',
        textAlign:'center'
    },
    odd_filter_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 18,
        color: '#777',
        textAlign:'center',
        marginTop:8,
        marginBottom:79
    }
})
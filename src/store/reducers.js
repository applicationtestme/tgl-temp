import {combineReducers} from 'redux'
import EventDetatilStore from './EventDetatilStore/reducer'

const rootReducer = combineReducers({
  EventDetatilStore
})

export default rootReducer
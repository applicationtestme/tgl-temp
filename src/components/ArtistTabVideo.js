




import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Fonts } from '../common/fonts';
import { Images } from '../common/Images';
import { GalleryArtistTab } from "./GalleryArtistTab";
import { MerchandiseArtistTab } from "./MerchandiseArtistTab";
import { AboutUsArtistTab } from "./AboutUsArtistTab";
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
export class ArtistTabVideo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            past_event_tab: true,
            buy_album_tab: true,
            video_tab: false,
            gellery_tab: false,
            merchandise: false,
            aboutus: false
        }

    }
    past_event_tab_view = () => {
        this.setState({ past_event_tab: true, video_tab: false, gellery_tab: false, aboutus:false });
        // this.setState({ video_tab: true });
        // this.setState({ gellery_tab: true });
    }
    video_tab_view = () => {
        this.setState({ video_tab: true, past_event_tab: false, gellery_tab: false,aboutus:false });
    }
    gellery_tab_view = () => {
        this.setState({ gellery_tab: true, past_event_tab: false, video_tab: false,aboutus:false });
    }
    about_us=()=>{
        this.setState({ aboutus: true, past_event_tab: false, video_tab: false, gellery_tab:false });
    }
    render() {
        return (
            
               <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
               <View>
                   <TouchableOpacity style={styles.video_view_style}>
                       <Image style={{}} source={Images.play_btn}></Image>
                   </TouchableOpacity>
                   <Text style={styles.past_event_video_title_text}>The Oman Show</Text>
               </View>
               <View>
                   <TouchableOpacity style={styles.video_view_style}>
                       <Image style={{}} source={Images.play_btn}></Image>
                   </TouchableOpacity>
                   <Text style={styles.past_event_video_title_text}>The Oman Show</Text>
               </View>
               <View>
                   <TouchableOpacity style={styles.video_view_style}>
                       <Image style={{}} source={Images.play_btn}></Image>
                   </TouchableOpacity>
                   <Text style={styles.past_event_video_title_text}>The Oman Show</Text>
               </View>
               <View>
                   <TouchableOpacity style={styles.video_view_style}>
                       <Image style={{}} source={Images.play_btn}></Image>
                   </TouchableOpacity>
                   <Text style={styles.past_event_video_title_text}>The Oman Show</Text>
               </View> 
           </ScrollView>
                  )
                }
            }

const styles = StyleSheet.create({
    tab_view_container: {
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: "center",
        marginHorizontal: 16,
        width:'100%'
    },
    tab_btn_text_style: {
        color: '#fff',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
        marginVertical: 10,
    },
    tab_btn_style: {
        borderColor: '#fff',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 3
    },
    tab_containt_view: {
        flexDirection: 'row',
        marginLeft: 16
    },
    video_view_style: {
        width: 202,
        height: 114,
        backgroundColor: '#1b1c20',
        borderRadius: 10,
        marginVertical: 8,
        marginRight: 8,
        justifyContent: 'center',
        alignItems: 'center'
    },
    past_event_video_title_text: {
        color: '#fff',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
    },
})

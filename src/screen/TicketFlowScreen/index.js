import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, ImageBackground, KeyboardAvoidingView, TextInput } from 'react-native';
import { styles } from './styles';
import { Header } from '../../components'
import { Images } from '../../common/Images'
import StepIndicator from 'react-native-step-indicator';
import Carousel from 'react-native-snap-carousel';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import Entypo from 'react-native-vector-icons/dist/Entypo';
import { scrollInterpolator, animatedStyles } from '../../Utils/animations';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

const labels = ["Select Ticket & Quantity", "Checkout", "Generate ticket",];
const customStyles = {
    stepIndicatorSize: 1,
    currentStepIndicatorSize: 10,
    separatorStrokeWidth: 1,
    currentStepStrokeWidth: 5,
    stepStrokeCurrentColor: '#fff',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#fe7013',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#fe7013',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#fe7013',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 12,
    currentStepIndicatorLabelFontSize: 12,
    stepIndicatorLabelCurrentColor: '#fe7013',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 12,
    currentStepLabelColor: '#ddd',

}
const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.9);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 1.2);

const DATA = [];
for (let i = 0; i < 2; i++) {
    DATA.push(i)
}


export default class TicketFlowScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            currentPosition: 0,
            index: 0,
            show: true,
            activeIndex: 0,
            selectedCardIndex: 0,
            ShowCardList: [

                {
                    concertName: 'The Mrs. Carter Show World Tour',
                    TicketType: 'Regular',
                    ConcertPoint: [{ Points: 'Live Concert Pass' }, { Points: 'Live Concert Pass' }],
                    AmountTicket: 7,
                    clicks: 0,
                    isAvailable: 10
                },
                {
                    concertName: 'The Mrs. Carter Show World Tour',
                    TicketType: 'Early Bird',
                    ConcertPoint: 'Live Concert Pass',
                    ConcertPoint: [{ Points: 'Live Concert Pass' }, { Points: 'Live Concert Pass' },],
                    AmountTicket: 5,
                    clicks: 0,
                    isAvailable: 0
                },

            ]
        }

    }
    IncrementItem = (index, click, Available) => {
        var arr = this.state.ShowCardList;
        if (Available > click) {
            arr[index].clicks = click + 1
        }
        this.setState({ ShowCardList: arr, selectedCardIndex: index });
    }
    DecreaseItem = (index, click, Available) => {
        var arr = this.state.ShowCardList;
        if (click > 0) {
            arr[index].clicks = click - 1
        }
        this.setState({ ShowCardList: arr, selectedCardIndex: index });
    }
    click() {
        alert("CClick")
    }
    _renderItem = ({ item, index }) => {
        return (
            <View style={{ width: ITEM_WIDTH, height: ITEM_HEIGHT, backgroundColor: 'transparent', borderRadius: 10 }}>
                {/* <Text style={styles.itemLabel}>{` ${item}`}</Text> */}
                <View style={styles.card_main_view_container}>
                    <View style={styles.head_view_container}>
                        <View style={styles.compnay_title_view_container}>
                            <Text style={styles.TGL_Presents_style}>TGL Presents  </Text>
                            <View style={styles.line_view_style}></View>
                        </View>
                        <Text style={styles.tour_name_title_style}>{item.concertName}</Text>
                        <View style={styles._regular_n_price_container}>
                            <View style={styles.regular_n_live_concert_text_style_view}>
                                <Text style={styles.event_ticket_type_text_style}>{item.TicketType}</Text>
                                <View style={styles.live_concert_view}>
                                    {
                                        item.ConcertPoint.map((itm, idx) =>
                                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                                <Text style={styles.event_ticket_type_text_style}>• </Text>
                                                {/* <Text style={styles.live_concert_pass_text_style}> {item.ConcertPoint[0].Points}</Text> */}
                                                <Text style={styles.live_concert_pass_text_style}> {itm.Points}</Text>
                                            </View>
                                        )
                                    }
                                </View>
                            </View>
                            <View style={styles.price_view}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.doller_sign_text_style}>$ </Text>
                                    <Text style={styles.amount_text_style}>{item.AmountTicket}</Text>
                                </View>
                                <Text style={styles.per_ticket_text_style}>Per Ticket</Text>
                            </View>
                        </View>
                        <View style={{ justifyContent: 'center', marginTop: 0 }}>
                            <Text style={styles.time_text_style}>25 Aug, 6:30 PST</Text>
                        </View>
                    </View>
                    <View style={{ width: '100%', borderStyle: 'dashed', borderBottomWidth: 1, borderColor: '#ddd', borderRadius: 1, height: 1 }}></View>
                    {
                        item.isAvailable > 0 ?
                            <View style={styles.body_view_container}>
                                <View style={styles.select_quality_container}>
                                    <Text style={styles.select_Quantity_text_style}>Select Quantity</Text>
                                </View>
                                <View style={styles.select_btn_container}>
                                    {item.clicks == 0 ?

                                        <View onPress={() => this.DecreaseItem(index, item.clicks)} style={[styles.close_btn_round_view_style, { backgroundColor: 'gray' }]}>
                                            <Entypo name='minus' size={18} color='#dddddd'></Entypo>
                                        </View>
                                        :
                                        <TouchableOpacity onPress={() => this.DecreaseItem(index, item.clicks, item.isAvailable)} style={styles.close_btn_round_view_style}>
                                            <Entypo name='minus' size={18} color='#dddddd'></Entypo>
                                        </TouchableOpacity>
                                    }
                                    <View style={{ backgroundColor: '#212223', }}>
                                        <Text style={styles.selected_amount_text_style}>{item.clicks}</Text>
                                    </View>
                                    <TouchableOpacity onPress={() => { this.IncrementItem(index, item.clicks, item.isAvailable) }} style={styles.close_btn_round_view_style}>
                                        <Entypo name='plus' size={18} color='#dddddd'></Entypo>
                                    </TouchableOpacity>
                                </View>

                                <View style={styles.select_quality_container}>
                                    <Text style={styles.few_left_text_style}>Hurry! Only  {item.isAvailable}  Left</Text>
                                </View>
                            </View>
                            :
                            <View style={styles.body_view_container}>
                                <View style={styles.sold_out_view}>
                                    <Image resizeMethod={"auto"} source={Images.sold_out}></Image>
                                </View>
                            </View>
                    }
                </View>
            </View >

        );
    }
    render() {
        return (
            <KeyboardAvoidingView behaviour="height" style={styles.main_container}>
                <ScrollView contentContainerStyle={styles.scrlView_container}>
                    <Header />
                    <View style={styles.ticket_text_n_close_btn_view}>
                        <View style={styles.blank_view}></View>
                        <View style={styles.ticket_title_view}>
                            <Text style={styles.tickets_text_style}>Tickets</Text>
                        </View>
                        <View style={styles.close_btn_view}>
                            <TouchableOpacity onPress={this.goBack} style={{ marginRight: 23 }}>
                                <Ionicons name='close' size={18} color='#dddddd'></Ionicons>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ backgroundColor: '#101113', marginHorizontal: 44 }}>
                        <StepIndicator
                            customStyles={customStyles}
                            currentPosition={this.state.currentPosition}
                            labels={labels}
                            stepCount={3}
                        />
                    </View>
                    <View style={{ marginTop: 23 }}>
                        <Carousel
                            ref={(c) => this.carousel = c}
                            data={this.state.ShowCardList}
                            renderItem={this._renderItem}
                            sliderWidth={SLIDER_WIDTH}
                            itemWidth={ITEM_WIDTH}
                            inactiveSlideShift={0}
                            onSnapToItem={index => this.setState({ activeIndex: index })}
                            scrollInterpolator={scrollInterpolator}
                            slideInterpolatedStyle={animatedStyles}
                            useScrollView={true}
                        />
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <View style={styles.total_ticket_n_amount_view}>
                            <View style={styles.total_price_view}>
                                <Text style={styles.total_Tickets_text_style}>Tickets</Text>
                                <Text style={styles.total_amout_text_style}>{this.state.ShowCardList[this.state.selectedCardIndex].clicks}</Text>
                            </View>
                            <View style={{ borderWidth: 0.5, height: 40, marginTop: 40, borderColor: '#2b2c2d' }}></View>
                            <View style={styles.total_amount_view}>
                                <Text style={styles.total_Tickets_text_style}>Amount</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.doller_text_style}>$ </Text>
                                    <Text style={styles.total_amout_text_style}>{this.state.ShowCardList[this.state.selectedCardIndex].clicks * this.state.ShowCardList[this.state.selectedCardIndex].AmountTicket}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.checkout_view}>
                            <TouchableOpacity style={styles.checkout_btn_style} 
                                onPress = {()=>{
                                    this.props.navigation.navigate('SelectTicketScreen')
                                }}
                            >
                                <Text style={styles.checkout_text_style}>Checkout</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.FAQ_view}>
                            <View style={styles.FAQ_title_view}>
                                <Text style={styles.FAQ_title_text}>Frequently Answered Questions</Text>
                            </View>
                            <View style={styles.FAQ_question_view}>
                                <Text style={styles.que_1_text_style}>Can I watch a livestream after it airs?</Text>
                                <Text style={styles.ans_1_text_style}>Our livestreams are genuinely live events. It’s
                                fully up to the artist’s discretion to leave a stream
                                up. If they choose to do so, it will be announced
                                via their social media.</Text>
                            </View>
                            <View style={styles.FAQ_question_view}>
                                <Text style={styles.que_1_text_style}>Can I get a refund on my ticket?</Text>
                                <Text style={styles.ans_1_text_style}>All sales are final once purchased. If the event is
                                cancelled and NOT rescheduled, your artist will
                                then issue refunds. Tickets will not be refunded
                                if the event is rescheduled. If you’d like Veeps to
                                review your case, please email
                                support@veeps.com</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }
}


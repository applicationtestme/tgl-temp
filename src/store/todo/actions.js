export function addNewTask(newTask) {
  return {
    type: "ADD_TASK",
    newTask: newTask,
  }
}
export function loadTask(newTask) {
  return {
    type: "LOAD_TASK"
  }
}
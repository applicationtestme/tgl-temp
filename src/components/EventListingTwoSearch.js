import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, StyleSheet,ImageBackground } from 'react-native';
import { Fonts } from '../common/fonts';
import { Images } from '../common/Images';
import LinearGradient from 'react-native-linear-gradient';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
export class EventListingTwoSearch extends Component {

    render() {
        return (
            this.props.eventDataTwoSearchData.map((item, index) => {
                return (
                    <TouchableOpacity style={styles.similar_artist_box_view__style}>
                        <ImageBackground source={{ uri: item.artistProfileImage }} imageStyle={{}} style={styles.similar_artist_img__style}>
                            <LinearGradient colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0,0.81)', 'rgba(0, 0, 0,0.91)']} style={styles.linearGradient2}>
                                <View style={styles.inside_contant}>
                                    <Text style={styles.similar_artist_name_text__style}>{item.artistName}</Text>
                                    <Text style={styles.similar_artist_event_name_text__style}>{item.eventName}</Text>
                                    <Text style={styles.similar_artist_event_time_name_text__style}>{item.eventTime}</Text>
                                    <TouchableOpacity style={styles.similar_artist_event_buy_ticket_btn__style}>
                                        <Text style={styles.similar_artist_event_buy_ticket_text_btn__style}>Buy Tickets</Text>
                                    </TouchableOpacity>
                                </View>
                            </LinearGradient>
                        </ImageBackground>
                    </TouchableOpacity>
                )
            })
        )
    }
}

const styles = StyleSheet.create({
    similar_artist_box_view__style:{
        justifyContent:'center',
        alignItems:'center',
        borderRadius:4,
        overflow:'hidden',
        margin:8,
        width:140,
        backgroundColor:'red'
    },
    similar_artist_img__style:{
        width:140,
        height:231,
        borderRadius:10,
        justifyContent:'flex-end',
        alignItems:'center'
    },
    linearGradient2:{
        opacity:1,
        flex: 1,
        width:'100%',
        justifyContent:'flex-end',
        alignItems:'center'
    },
    inside_contant:{
        justifyContent:'flex-end',
        alignItems:'center'
    },
    similar_artist_name_text__style:{
        color:'#dddddd',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
    },
    similar_artist_event_name_text__style:{
        color:'#dddddd',
        fontFamily: Fonts.PlayfairDisplay_Black,
        fontSize: 16,
        marginTop:4
    },
    similar_artist_event_time_name_text__style:{
        color:'#dddddd',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        marginTop:4
    },
    similar_artist_event_buy_ticket_btn__style:{
        borderWidth:1,
        borderColor:'grey',
        borderRadius:10,
        marginTop:11,
        marginBottom:10
    },
    similar_artist_event_buy_ticket_text_btn__style:{
        color:'#dddddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        marginVertical:10,
        marginHorizontal:28
    },
})



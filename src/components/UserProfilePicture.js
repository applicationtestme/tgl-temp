import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, StyleSheet } from 'react-native';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
export class UserProfilePicture extends Component {
    render() {
        return (
            <TouchableOpacity style={[styles.profileCircle, this.props.profileCircle]} />
        );
    }
}

const styles = StyleSheet.create({
    profileCircle: {
        flexDirection: 'row',
        backgroundColor: '#2d2d2d',
        height: 30,
        width: 30,
        borderRadius: 30,
        marginRight: 10
    },
})


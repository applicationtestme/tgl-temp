import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Fonts } from '../common/fonts';
import { Images } from '../common/Images';
import { RippleView, UserProfilePicture } from '../components';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
export class PendingRequestRow extends Component {
   
    aproveOrRejectRequest = () => {

    }

    render() {
        const { item, aproveRequest, rejectRequest } = this.props
        return (
            <View style={styles.pendingRequestChip} >
                <UserProfilePicture profileCircle={styles.profileCircleBig} />
                <View style={styles.reqestSenderDetail}>
                    <Text style={styles.groupNameStyle}>{item.groupName}</Text>
                    <Text style={styles.groupInviteTextStyle}>{item.invitedBy} has invited you</Text>
                </View>
                <View style={styles.requestActionButtonsContainer}>
                    <RippleView onPress={aproveRequest} style={styles.actionAcceptButton}>
                        <Image source={Images.acceptIcon} />
                        <Text style={styles.acceptrequestText}>Accept</Text>
                    </RippleView>
                    <RippleView onPress={rejectRequest} style={styles.actionRejectButton}>
                        <Image source={Images.rejectIcon} />
                        <Text style={styles.rejectRequestText}>Reject</Text>
                    </RippleView>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    pendingRequestChip: {
        paddingHorizontal: 16,
        paddingVertical: 10,
        backgroundColor: '#252628',
        flexDirection: 'row',
        alignItems: 'center'
    },
    profileCircleBig: {
        flexDirection: 'row',
        backgroundColor: '#48494b',
        height: 30,
        width: 30,
        borderRadius: 30,
        marginRight: 10
    },
    groupNameStyle: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        color: '#fff'
    },
    groupInviteTextStyle: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#fff'
    },
    reqestSenderDetail: {
        flex: 1
    },
    requestActionButtonsContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    actionAcceptButton: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    actionRejectButton: {
        marginLeft: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    acceptrequestText: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        color: '#dddddd'
    },
    rejectRequestText: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        color: '#48494b'
    },
})


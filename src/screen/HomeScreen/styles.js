import { StyleSheet, Dimensions } from 'react-native';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import { Fonts } from '../../common/fonts';
export const styles = StyleSheet.create({
    scrlView_container: {
        flexGrow: 1,
        backgroundColor: '#101113',
        paddingBottom: 50
    },
    main_container: {
        flex: 1,
        backgroundColor: '#101113'
    },
    img_bg_style: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    header_container_style: {
        backgroundColor: 'transparent',
        position: 'absolute',
        backgroundColor: 'transparent',
        zIndex: 100,
        top: 0,
        left: 0,
        right: 0,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0,
        marginTop: 10
    },
    header_user_img_style: {
        width: 24,
        height: 24,
        borderRadius: 24 / 2,
        backgroundColor: '#ddd',
        marginLeft: 16,
        marginVertical: 12
    },
    Show_name_and_sound: {
        flexDirection: 'row',
        marginTop: 322
    },
    sound_icon_view: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    Show_title_view: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 4,
        
    },
    go_btn_view: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        flex: 1
    },
    title_text_style: {
        fontFamily: 'PlayfairDisplaySC-Bold',
        fontSize: 24,
        color: '#dddddd',
    },
    go_btn_round_style: {
        width: 24,
        height: 24,
        borderRadius: 24 / 2,
        borderWidth: 1,
        borderColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    genre_type_text_view: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
    },
    genre_type_text_style: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#ddd',
    },
    Video_view_slide: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-evenly',
        marginTop: 50,
        marginLeft: '15%'
    },
    video_thumb_n_name_container: {
        marginRight: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    thumb_img_style: {
        width: 72,
        height: 40,
        backgroundColor: '#aaa'
    },
    thumb_title_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        color: '#ffffff',
        marginTop: 10
    },
    watched_video_line: {
        height: 2,
        width: '100%',
        borderColor: '#c4c4c4',
        borderRadius: 30,
        backgroundColor: 'transparent',
        marginTop: 10
    },
    upcoming_event_title_view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 30,
        width: '100%',
        marginBottom: 14
    },
    upcoming_event_text_style: {
        fontFamily: 'PlayfairDisplaySC-Bold',
        fontSize: 18,
        color: '#dddddd',
        marginLeft: 16
    },
    see_all_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        color: '#ddd',
        marginRight: 16
    },
    slider_event_that_broke_internet_view: {
        marginTop: 29
    },
    event_that_broke_internet_text_style: {
        fontFamily: 'PlayfairDisplaySC-Bold',
        fontSize: 18,
        color: '#dddddd',
        marginLeft: 16
    },
    singer_name_event_name_view:{
        justifyContent:'center',
        alignItems:'center',
        marginTop:138,
        marginBottom:8
    },
    singer_name_event_name_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        color: '#ddd',
    },
    event_for_you_text_view:{
        marginTop:41
    },
    event_for_you_text_style:{
        fontFamily: 'PlayfairDisplaySC-Bold',
        fontSize: 18,
        color: '#dddddd',
        marginLeft: 16
    },
    similat_atist_view:{
        marginLeft:16
    },
    img_background_two_artist_text:{
        marginTop:377,
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 18,
        color: '#dddddd',
    },
    img_background_two_event_text:{
        fontFamily: 'PlayfairDisplaySC-Bold',
        fontSize: 28,
        color: '#dddddd',
        marginTop:3
    },
    img_background_two_event_time_text:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
        color: '#777',
        marginTop:8
    },
    img_background_two_buy_ticket_style:{
        backgroundColor:'#fff',
        borderRadius:10,
        justifyContent:'center',
        alignItems:'center',
        marginTop:16,
        marginBottom:48
    },
    img_background_two_buy_ticket__text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 18,
        color: '#1b1c20',
        marginHorizontal:42,
        marginVertical:10
    },
    explor_the_genre_view:{

    },
    explor_gigs_view:{
    },
    genre_option_btn_view:{
        width:'100%',
        marginVertical:18
    },
    featured_Fans_view:{
        
    },
    featureFanView:{
        marginRight:65
    },
    featureFanUserImg:{
        width:96,
        height:96,
        borderRadius:96/2
    },
    feature_Fan_name_text_style:{
        fontFamily: 'PlayfairDisplaySC-Bold',
        fontSize: 12,
        color: '#fff',
        marginTop:3
    },
    feature_Fan_address_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 11,
        color: '#fff',
        marginTop:5
    }

})
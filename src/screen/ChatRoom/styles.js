import { StyleSheet, Dimensions } from 'react-native';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import { Fonts } from '../../common/fonts';
const padding_horizontal = 16
export const styles = StyleSheet.create({
    scrollContainer: {
        height: height - 225
    },
    scrlView_container: {
        flexGrow: 1
    },
    main_container: {
        flex: 1,
        backgroundColor: '#0c0d0f'
    },
    vipSupporterMainView: {
        paddingVertical: 10,
    },
    supporterListHeader: {
        flexDirection: 'row',
        paddingHorizontal: padding_horizontal
    },
    vipMarkStyle: {
        position: 'absolute',
        right: 10,
        top: 0
    },
    supporterIconStyle: {
        height: 18,
        width: 18,
        resizeMode: 'contain'
    },
    supporterListHeaderTitle: {
        color: '#fff',
        fontSize: 14,
        fontFamily: Fonts.OpenSans_semibold,
        marginLeft: 10
    },
    supporterListContainer: {
        marginTop: 12,
        flexDirection: 'row',
        alignItems: 'center'
    },
    messagesMainContainer: {
        marginTop: 12
    },

    //
    chatDetailsContainer: {
        backgroundColor: '#101113',
        borderBottomColor: '#979797',
        borderBottomWidth: 1,
        paddingTop: 16,
        paddingHorizontal: 16,
    },
    chatDetailsContainerLeftView: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    chatRoomDetailRightIcons: {
        flexDirection: 'row'
    },
    global__title_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 16,
        color: '#dddddd',
        flex: 1
    },
    participarts_view_container: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 9,
        paddingBottom: 18,
    },
    participarts_profile_pic: {
        flexDirection: 'row'
    },
    participants_count_text_style: {
        textDecorationLine: 'underline',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        color: '#dddddd',
        right: 3
    },
    user_budge_pic_small_style: {
        width: 10,
        height: 10,
        borderRadius: 10 / 2,
        backgroundColor: '#363636'
    },
    addIconStyle: {
        height: 24,
        width: 24,
        borderRadius: 24,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#212123'
    },
    RIconStyle: {
        height: 24,
        width: 24,
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#191919',
        marginLeft: 20
    },
    RIconTextStyle: {
        color: '#ddd',
        fontSize: 15,
        fontFamily: Fonts.OpenSans_semibold
    },
    vipUserProfilePictureStyle: {
        height: 30,
        width: 30,
        borderRadius: 30,
        marginRight: 10,
        backgroundColor: '#48494b',
        borderWidth: 1
    },
    absolute: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
      }
})
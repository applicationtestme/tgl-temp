import { StyleSheet, Dimensions } from 'react-native';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import { Fonts } from '../../../../common/fonts';
export const styles = StyleSheet.create({
    main_container: {
        margin: 16,
    },
    titleView: {
        flexDirection: 'row'
    },
    title: {
        fontSize: 12,
        color: '#ddd',
        fontFamily: Fonts.OpenSans_semibold,
        marginHorizontal: 5
    },
    formMainContent: {
        backgroundColor: '#1b1c1e',
        paddingHorizontal: 16,
        marginTop: 20,
        paddingVertical: 30,
        borderRadius: 10
    },
    formTitle: {
        fontSize: 16,
        color: '#ddd',
        fontFamily: Fonts.OpenSans_semibold,
        marginBottom: 15
    },
    formLabelStyle: {
        fontSize: 10,
        color: '#ddd',
        fontFamily: Fonts.OpenSans_semibold,
        marginTop: 13,
        marginLeft: 5
    },
    textInputStyle: {
        backgroundColor: '#262729',
        borderRadius: 10,
        marginTop: 5,
        color: '#ddd',
        fontFamily: Fonts.OpenSans_regular,
        paddingLeft: 10
    },
    submitButton: {
        backgroundColor: '#8d8d8e',
        borderRadius: 10,
        width: '80%',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginVertical: 30,
        paddingVertical: 18
    },
    submitButtonText: {
        fontSize: 14,
        color: '#1b1c20',
        fontFamily: Fonts.OpenSans_semibold,
    },
    outerRadio: {
        height: 16,
        width: 16,
        borderRadius: 16,
        borderWidth: 1,
        borderColor: '#ddd',
        marginTop: 13,
        marginLeft: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    radioGroup: {
        flexDirection: 'row'
    },
    innnerRadio: {
        height: 10,
        width: 10,
        borderRadius: 10,
        backgroundColor: '#ddd',
    },
    radioItem: {
        flexDirection: 'row',
        marginRight: 50
    }
})
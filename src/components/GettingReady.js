import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Header } from '../components'
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
export class GettingReady extends Component {
    render() {
            return (
                <View style={styles.main_container}>
                    <Header />
                    <View style={styles.loadingView}>
                        < ActivityIndicator color={'#fff'} />
                    </View>
                </View>
            )
        }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        backgroundColor: '#0c0d0f'
    },
    loadingView: {
        flex: 1,
        justifyContent: 'center'
    }
})


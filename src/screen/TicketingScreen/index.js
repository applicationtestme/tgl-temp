import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, ImageBackground, KeyboardAvoidingView, TextInput } from 'react-native';
import { styles } from './styles';
import { Header } from '../../components'
import { TshirtForm } from './components/TshirtForm'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { increment } from '../../actions'
import { addNewTask, loadTask } from '../../store/todo/actions'
export class TicketingScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            step: 1
        }
    }

    componentDidMount = () => {

    }

    addRoom = () => {

    }

    loadTask = () => {
        this.props.loadTask('new one')
    }

    render() {
        const { step } = this.state
        return (
            <View style={styles.main_container}>
                <Header />
                {/* <TouchableOpacity onPress={this.loadTask}>
                    <Text style={{ color: '#fff' }}>{this.props.title}</Text>
                    {this.props.EventDetatilStore.userData !== undefined && this.props.EventDetatilStore.userData !== null &&
                    <Text style={{ color: '#fff' }}>{this.props.EventDetatilStore.userData.total}</Text>}
                </TouchableOpacity> */}
                <ScrollView contentContainerStyle={styles.scrlView_container}>
                    {
                        step === 1 &&
                        <TshirtForm />
                    }
                </ScrollView>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        // title: state.todo.title,
        // todo: state.todo,
        // EventDetatilStore: state.EventDetatilStore
    };
}
function matchDispatchToProps(dispatch) {
    //return bindActionCreators({ loadTask }, dispatch)
    return {
        addNewTask: (newTask) => dispatch(addNewTask(newTask)),
        loadTask: (newTask) => dispatch(loadTask(newTask))
    }
}
export default connect(mapStateToProps, matchDispatchToProps)(TicketingScreen);

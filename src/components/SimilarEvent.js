import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, StyleSheet,ImageBackground } from 'react-native';
import { Fonts } from '../common/fonts';
import { Images } from '../common/Images';
import LinearGradient from 'react-native-linear-gradient';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
export class SimilarEvent extends Component {

    render() {
        return (
            this.props.artistData.map((item, index) => {
                return (
                    <TouchableOpacity style={styles.similar_artist_box_view__style}>
                        <ImageBackground source={{ uri: item.artistProfileImage }} style={styles.similar_artist_img__style}>
                            <LinearGradient colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0,0.91)', '#000']} style={styles.linearGradient2}>
                                <View style={styles.inside_contant}>
                                    <Text style={styles.similar_artist_name_text__style}>{item.artistName}</Text>
                                    <Text style={styles.similar_artist_event_name_text__style}>{item.eventName}</Text>
                                    <Text style={styles.similar_artist_event_time_name_text__style}>{item.eventTime}</Text>
                                    <TouchableOpacity style={styles.similar_artist_event_buy_ticket_btn__style}>
                                        <Text style={styles.similar_artist_event_buy_ticket_text_btn__style}>Buy Tickets</Text>
                                    </TouchableOpacity>
                                </View>
                            </LinearGradient>
                        </ImageBackground>
                    </TouchableOpacity>
                )
            })
        )
    }
}

const styles = StyleSheet.create({
    similar_artist_box_view__style:{
        justifyContent:'center',
        alignItems:'center',
        marginRight:10,  
    },
    similar_artist_img__style:{
        width:118,
        height:170,
        borderRadius:10
    },
    linearGradient2:{
        opacity:1,
        flex: 1,
        width:'100%',
        justifyContent:'center',
        alignItems:'center'
    },
    inside_contant:{
        marginTop:50,
        justifyContent:'center',
        alignItems:'center'
    },
    similar_artist_name_text__style:{
        color:'#dddddd',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
    },
    similar_artist_event_name_text__style:{
        color:'#dddddd',
        fontFamily: Fonts.PlayfairDisplay_Black,
        fontSize: 16,
        marginTop:4
    },
    similar_artist_event_time_name_text__style:{
        color:'#dddddd',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        marginTop:4
    },
    similar_artist_event_buy_ticket_btn__style:{
        borderWidth:1,
        borderColor:'grey',
        borderRadius:10,
        marginTop:11
    },
    similar_artist_event_buy_ticket_text_btn__style:{
        color:'#dddddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        marginVertical:10,
        marginHorizontal:28
    },
})



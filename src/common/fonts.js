export const Fonts = {
    OpenSans_regular: 'OpenSans-Regular',
    OpenSans_semibold: 'OpenSans-Semibold',
    OpenSans_Bold: 'OpenSans-Bold',
    PlayfairDisplay_Bold: 'PlayfairDisplay-Bold',
    PlayfairDisplay_Black:"PlayfairDisplay-Black",
    PlayfairDisplay_Regular:"PlayfairDisplay-Regular",
    OpenSans_Light:"OpenSans-Light"
}
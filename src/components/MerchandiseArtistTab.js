import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Fonts } from '../common/fonts';
import { Images } from '../common/Images';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export class MerchandiseArtistTab extends Component {

    constructor(props) {
        super(props)
        this.state = {

        }

    }

    render(){
        return(
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <View>
                    <TouchableOpacity style={styles.video_view_style}>
                        <Image style={{height:85,width:65}} source={{uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTEqLS43eH0dB0gTKeDhtMIjy8VlLZNfhioNw&usqp=CAU'}}></Image>
                    </TouchableOpacity>
                    <Text style={styles.past_event_video_title_text}>AWG-All Concert Gear {"\n"}Men's Regular T-shirt</Text>
                    <Text style={styles.past_event_video_title_text}>$46</Text>
                </View>
                <View>
                    <TouchableOpacity style={styles.video_view_style}>
                        <Image style={{height:85,width:65}} source={{uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTEqLS43eH0dB0gTKeDhtMIjy8VlLZNfhioNw&usqp=CAU'}}></Image>
                    </TouchableOpacity>
                    <Text style={styles.past_event_video_title_text}>AWG-All Concert Gear {"\n"}Men's Regular T-shirt</Text>
                    <Text style={styles.past_event_video_title_text}>$46</Text>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    video_view_style: {
        width: 150,
        height: 150,
        backgroundColor: '#1b1c20',
        borderRadius: 10,
        marginVertical: 8,
        marginRight: 8,
        alignItems:'center',
        justifyContent:'center'
    },
    past_event_video_title_text: {
        color: '#fff',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 14,
    
    },
})
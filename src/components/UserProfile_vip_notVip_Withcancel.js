import React, { Component } from 'react';
import { Dimensions, Text, StyleSheet, View, Image, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import { Fonts } from '../common/fonts';
import { Images } from '../common/Images';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
export class UserProfile_vip_notVip_Withcancel extends Component {
    render() {
        const { isVip, showCancelButton = false, onRemove } = this.props
        return (
            <TouchableOpacity style={styles.userProfileImage}>
                {
                    isVip === 1 &&
                    <Image source={Images.vipSuporterIcon} style={styles.vipSuporterIcon} />
                }
                {
                    showCancelButton &&
                    <TouchableOpacity style={styles.deleteIconStyle} onPress={onRemove}>
                        <Image source={Images.delete} />
                    </TouchableOpacity>
                }
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    userProfileImage: {
        height: 42,
        width: 42,
        borderRadius: 42,
        backgroundColor: '#262729'
    },
    vipSuporterIcon: {
        position: 'absolute',
        bottom: -5,
        alignSelf: 'center'
    },
    deleteIconStyle: {
        left: 30,
        top: 0
    }
})


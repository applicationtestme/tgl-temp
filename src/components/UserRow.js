import React, { Component } from 'react';
import { Dimensions, Text, StyleSheet, View, Image, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import { Fonts } from '../common/fonts';
import { Images } from '../common/Images';
import { UserProfile_vip_notVip_Withcancel } from '../components';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
export class UserRow extends Component {
    render() {
        const { user, creatingRoom, onInvite, roomInfo = false, participants_screens, follow, onfollow, onRemove } = this.props
        return (
            <View style={styles.userRowStyle}>
                <UserProfile_vip_notVip_Withcancel isVip={user.isVip} />
                <View style={styles.userDetailsStyle}>
                    <Text style={styles.userNameTextStyle}>{user.userName}</Text>
                    <Text style={styles.userOtherDetailTextStyle}>{user.badges}</Text>
                    <Text style={styles.userOtherDetailTextStyle}>{user.address}</Text>
                </View>
                <View style={styles.rightSideView}>
                    {
                        creatingRoom &&
                        <TouchableOpacity style={styles.inviteButtonStyle} onPress={onInvite}>
                            <Text style={styles.inviteButtonTextStyle}>Invite</Text>
                        </TouchableOpacity>
                    }
                    {
                        roomInfo ?
                            user.isAdmin === 0 ?
                                <TouchableOpacity style={styles.inviteButtonStyle} onPress={onRemove}>
                                    <Text style={styles.inviteButtonTextStyle}>Remove</Text>
                                </TouchableOpacity>
                                :
                                <Text style={styles.inviteButtonTextStyle}>Admin</Text>
                            :
                            null
                    }
                    {
                        participants_screens === true ?
                            user.follow === 0 ?
                                < TouchableOpacity style={styles.inviteButtonStyle} onPress={onfollow}>
                                    <Text style={styles.followButtonTextStyle}>Follow</Text>
                                </TouchableOpacity> :
                                user.follow === 1 ?
                                    <TouchableOpacity onPress={onfollow}>
                                        <Text style={styles.followingButtonTextStyle}>Following</Text>
                                    </TouchableOpacity>
                                    : null : null


                    }
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    userRowStyle: {
        flexDirection: 'row',
        paddingVertical: 10,
        borderBottomColor: '#979797',
        borderBottomWidth: 0.5,
        alignItems: 'center'
    },
    userProfileImage: {
        height: 42,
        width: 42,
        borderRadius: 42,
        backgroundColor: '#262729'
    },
    vipSuporterIcon: {
        position: 'absolute',
        bottom: -5,
        alignSelf: 'center'
    },
    userDetailsStyle: {
        marginLeft: 20,
        flex: 1
    },
    userNameTextStyle: {
        color: '#fff',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
    },
    userOtherDetailTextStyle: {
        color: '#ddd',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
    },
    inviteButtonStyle: {
        backgroundColor: '#747475',
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 8
    },
    inviteButtonTextStyle: {
        color: '#ddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12
    },
    inviteButtonTextStyle: {
        color: '#ddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12
    },
    followingButtonTextStyle: {
        color: '#fff',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12
    },
})


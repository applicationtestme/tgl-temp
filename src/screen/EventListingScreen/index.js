import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, ImageBackground, KeyboardAvoidingView, TextInput } from 'react-native';
import { styles } from './styles';
import { Header } from '../../components/Header'
import { SimilarEvent } from '../../components/SimilarEvent'
import { Genres_round_view } from '../../components/Genres_round_view'
import { ArtistTabVideo } from '../../components/ArtistTabVideo'
import { EventListingTwoSearch } from '../../components/EventListingTwoSearch'
import LinearGradient from 'react-native-linear-gradient';
import { Images } from '../../common/Images'
import { Popover, PopoverController } from 'react-native-modal-popover';
import RBSheet from "react-native-raw-bottom-sheet";
import { Fonts } from '../../common/fonts';
import Entypo from 'react-native-vector-icons/dist/Entypo';
import Fontisto from 'react-native-vector-icons/dist/Fontisto';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import { scrollInterpolator, animatedStyles } from '../../Utils/animations';
import Carousel, { Pagination } from 'react-native-snap-carousel'
const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.9);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 1.2);

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export default class EventListingScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            NoresultFound: false,
            activeTab: 0,
            CardList: [
                {
                    album_name: 'Single Ladies ',
                    singer_name: 'Beyonce',
                },
                {
                    album_name: 'Single Ladies ',
                    singer_name: 'Beyonce'
                },
                {
                    album_name: 'Single Ladies ',
                    singer_name: 'Beyonce'
                },
                {
                    album_name: 'Single Ladies ',
                    singer_name: 'Beyonce',
                },
                {
                    album_name: 'Single Ladies ',
                    singer_name: 'Beyonce'
                },
                {
                    album_name: 'Single Ladies ',
                    singer_name: 'Beyonce'
                },
            ],
            EventListingTwoSearchDAta: [
                {
                    'artistName': 'Taylor Swift',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://schnncdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Taylor_Swift_003_20200226074119_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Martin Garrix',
                    'eventName': 'Summer Tour',
                    'artistProfileImage': 'https://sklktcdnems01.cdnsrv.jio.com/c.saavncdn.com/artists/Martin_Garrix_004_20200303120820_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'David Guetta',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://sklktcdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/David_Guetta_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Selena Gomez',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://sklktecdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Selena_Gomez_002_20200226073835_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Demi Lovato',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://schnncdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Demi_Lovato_002_20200312120805_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Taylor Swift',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://schnncdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Taylor_Swift_003_20200226074119_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Martin Garrix',
                    'eventName': 'Summer Tour',
                    'artistProfileImage': 'https://sklktcdnems01.cdnsrv.jio.com/c.saavncdn.com/artists/Martin_Garrix_004_20200303120820_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'David Guetta',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://sklktcdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/David_Guetta_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Selena Gomez',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://sklktecdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Selena_Gomez_002_20200226073835_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Demi Lovato',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://schnncdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Demi_Lovato_002_20200312120805_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Taylor Swift',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://schnncdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Taylor_Swift_003_20200226074119_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Martin Garrix',
                    'eventName': 'Summer Tour',
                    'artistProfileImage': 'https://sklktcdnems01.cdnsrv.jio.com/c.saavncdn.com/artists/Martin_Garrix_004_20200303120820_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'David Guetta',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://sklktcdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/David_Guetta_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Selena Gomez',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://sklktecdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Selena_Gomez_002_20200226073835_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Demi Lovato',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://schnncdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Demi_Lovato_002_20200312120805_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Taylor Swift',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://schnncdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Taylor_Swift_003_20200226074119_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Martin Garrix',
                    'eventName': 'Summer Tour',
                    'artistProfileImage': 'https://sklktcdnems01.cdnsrv.jio.com/c.saavncdn.com/artists/Martin_Garrix_004_20200303120820_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'David Guetta',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://sklktcdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/David_Guetta_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Selena Gomez',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://sklktecdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Selena_Gomez_002_20200226073835_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Demi Lovato',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://schnncdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Demi_Lovato_002_20200312120805_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
            ],
            EventBannerTypeData: [
                {
                    'artistName': 'Taylor Swift',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://schnncdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Taylor_Swift_003_20200226074119_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Martin Garrix',
                    'eventName': 'Summer Tour',
                    'artistProfileImage': 'https://sklktcdnems01.cdnsrv.jio.com/c.saavncdn.com/artists/Martin_Garrix_004_20200303120820_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'David Guetta',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://sklktcdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/David_Guetta_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Selena Gomez',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://sklktecdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Selena_Gomez_002_20200226073835_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Demi Lovato',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://schnncdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Demi_Lovato_002_20200312120805_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
            ],
            TopArtistList: [
                { name: 'Beyoncé', img_url: 'https://sdlhivkcdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Beyonce_500x500.jpg' },
                { name: 'Selena Gomez', img_url: 'https://sklktecdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Selena_Gomez_002_20200226073835_500x500.jpg' },
                { name: 'David Guetta', img_url: 'https://sklktcdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/David_Guetta_500x500.jpg' },
                { name: 'Demi lovato', img_url: 'https://schnncdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Demi_Lovato_002_20200312120805_500x500.jpg' },
                { name: 'Martin Garrix', img_url: 'https://sklktcdnems01.cdnsrv.jio.com/c.saavncdn.com/artists/Martin_Garrix_004_20200303120820_500x500.jpg' },
                { name: 'Beyoncé', img_url: 'https://sdlhivkcdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Beyonce_500x500.jpg' },
                { name: 'Selena Gomez', img_url: 'https://sklktecdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Selena_Gomez_002_20200226073835_500x500.jpg' },
                { name: 'David Guetta', img_url: 'https://sklktcdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/David_Guetta_500x500.jpg' },
                { name: 'Demi lovato', img_url: 'https://schnncdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Demi_Lovato_002_20200312120805_500x500.jpg' },
                { name: 'Martin Garrix', img_url: 'https://sklktcdnems01.cdnsrv.jio.com/c.saavncdn.com/artists/Martin_Garrix_004_20200303120820_500x500.jpg' },
            ],
            genreSortingResultList: [
                { name: 'Anime' },
                { name: 'Blues' },
                { name: 'Children' },
                { name: 'Classical' },
                { name: 'Comedy' },
                { name: 'Country' },
                { name: 'Dance' },
                { name: 'Electronic' },
                { name: 'Enka' },
                { name: 'French Pop' },
                { name: 'French Pop' },
            ],
            sortingcategoryList: [
                { name: 'Genre' },
                { name: 'Location' },
                { name: 'Date' },
                { name: 'Show Type' },
            ],
            linearBoxData: [
                { name: 'Y&I', img_url: 'https://schnncdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Demi_Lovato_002_20200312120805_500x500.jpg' },
                { name: 'Bonsai Trees', img_url: 'https://sklktcdnems01.cdnsrv.jio.com/c.saavncdn.com/artists/Martin_Garrix_004_20200303120820_500x500.jpg' },
            ],
            applyBtn: [
                { name: 'Reset' },
                { name: 'Apply' },
            ],
            sortBtnList: [
                { name: 'A - Z' },
                { name: 'Z - A' },
                { name: 'Newest First' },
            ]
        };

    }
    Noresult = () => {
        this.setState({ NoresultFound: !this.state.NoresultFound })
    }
    openFilter = () => {
        this.RBSheet_filter.open();
    }

    closeFilter = () => {
        this.RBSheet_filter.close();
    }
    isSelectedcategorybtn = (index) => {
        this.setState({ selectedIndex: index });
    }
    isSelectedListbtn = (index) => {
        this.setState({ selectedListEntity: index });
    }
    isSelectedApplyResetbtn = (index) => {
        this.setState({ selectedApplyReset: index });
    }
    isSelectedSortOption = (index) => {
        this.setState({ selectedSortOption: index });
    }
    _renderItem = ({ item, index }) => {
        return (
            <View style={styles.event_for_view_container}>
                <ImageBackground source={Images.lady_gaga_concer} imageStyle={{ borderRadius: 10 }} style={styles.lady_gaga_concert_img_bg_style}>
                    <LinearGradient colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0,0.35)', '#000']} style={styles.linearGradient2}>

                        <View style={styles.artist_name_n_concert_name_container}>
                            <View style={{ marginLeft: 25 }}>
                                <Image style={styles.lady_gaga_user_pic_style} source={{ uri: item.artistProfileImage }}></Image>
                            </View>
                            <View style={{ alignItems: 'flex-start', marginTop: 10, marginLeft: 10 }}>
                                <Text style={styles.artist_name_title_text_event_for_you_style}>{item.artistName}</Text>
                                <Text style={styles.concert_title_text_style}>{item.eventName}</Text>
                                <Text style={styles.concert_timing_text_style}>{item.eventTime}</Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '68%', }}>
                                    <View>
                                        <TouchableOpacity style={styles.buy_tickets_concert_artist_btn_style}>
                                            <Text style={styles.buy_ticket_concert_artist_text_style}>Buy Tickets</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View >
                                        <TouchableOpacity style={{}}>
                                            <Image style={{}} source={Images.right_arrow_padding}></Image>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>

                    </LinearGradient>
                </ImageBackground>
            </View>
        );
    }
    get pagination() {
        const { EventBannerTypeData, activeSlide } = this.state;
        return (
            <Pagination
                dotsLength={EventBannerTypeData.length}
                activeDotIndex={activeSlide}
                containerStyle={{ backgroundColor: 'tranparent' }}
                dotStyle={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    marginHorizontal: 8,
                    backgroundColor: 'rgba(255, 255, 255, 0.92)'
                }}
                inactiveDotStyle={{
                    // Define styles for inactive dots here
                }}
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.6}
            />
        );
    }
    _renderItem2 = ({ item, index }) => {
        return (
            <ImageBackground source={Images.lady_gaga_concer} imageStyle={{ width: '100%', }} style={{ flex: 1, justifyContent: "center", alignItems: 'center' }}>
                <View style={styles.singer_name_event_name_view}>
                    <Text style={styles.singer_name_event_name_text_style}>{item.album_name} • {item.singer_name}</Text>
                </View>
            </ImageBackground>
        );
    }
    render() {

        return (
            <KeyboardAvoidingView behaviour="height" style={styles.main_container}>
                <ScrollView contentContainerStyle={styles.scrlView_container}>
                    <Header />
                    <View style={styles.img_bg_style}>
                        <Text style={styles.main_title_upcoming_event_for_u_text_style}>Upcoming Events for you!</Text>
                        <View style={styles.artist_dp_view}>
                            <Carousel
                                ref={(c) => this.carousel = c}
                                data={this.state.EventBannerTypeData}
                                renderItem={this._renderItem}
                                sliderWidth={SLIDER_WIDTH}
                                itemWidth={ITEM_WIDTH}
                                inactiveSlideShift={0}
                                onSnapToItem={(index) => this.setState({ activeSlide: index })}
                                scrollInterpolator={scrollInterpolator}
                                slideInterpolatedStyle={animatedStyles}
                                useScrollView={true}
                            />
                            {this.pagination}
                        </View>
                    </View>
                    <View style={styles.main_view}>
                        <View style={styles.watchlist_event_view}>
                            <View style={styles.watchlist_event_titil_view}>
                                <Text style={styles.View_title_text_style}>Watchlist Event</Text>
                                <Text style={styles.see_all_text_style}>See all</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                    <SimilarEvent artistData={this.state.EventBannerTypeData} />
                                </ScrollView>
                            </View>
                        </View>
                        <View style={styles.slider_event_that_broke_internet_view}>
                            <View style={{ marginTop: 16, }}>
                                <Carousel
                                    ref={(r) => this.carousel = r}
                                    data={this.state.CardList}
                                    renderItem={this._renderItem2}
                                    sliderWidth={SLIDER_WIDTH}
                                    itemWidth={ITEM_WIDTH}
                                    inactiveSlideShift={0}
                                    onSnapToItem={index => this.setState({ activeIndex: index })}
                                    scrollInterpolator={scrollInterpolator}
                                    slideInterpolatedStyle={animatedStyles}
                                    useScrollView={true}
                                />

                            </View>
                        </View>
                        <Text style={styles.Event_text_style}>Events</Text>
                        <Text style={styles.Showing_Results_text_style}>Showing 20 Results</Text>
                        <View style={styles.sorting_btn_view}>
                            <TouchableOpacity onPress={() => this.openFilter()} style={styles.filter_btn_style}>
                                <MaterialIcons name='filter-list' style={styles.icon_btn_sorting_style} size={24} color='#fff'></MaterialIcons>
                                <Text style={styles.Filter_title_btn_text_style}>Filter</Text>
                            </TouchableOpacity>
                            {/* /// */}
                            <PopoverController  >
                                {({ openPopover, closePopover, popoverVisible, setPopoverAnchor, popoverAnchorRect }) => (
                                    <React.Fragment>
                                        <TouchableOpacity ref={setPopoverAnchor} onPress={openPopover} style={styles.filter_btn_style}>
                                            <MaterialIcons name='sort' style={styles.icon_btn_sorting_style} size={24} color='#fff'></MaterialIcons>
                                            <Text style={styles.Filter_title_btn_text_style}>Sort</Text>
                                        </TouchableOpacity>
                                        <Popover
                                            contentStyle={{ backgroundColor: '#252628', borderRadius: 10 }}
                                            arrowStyle={{ borderTopColor: 'transparent', }}
                                            backgroundStyle={{ backgroundColor: '#252628a6' }}
                                            visible={popoverVisible}
                                            onClose={closePopover}
                                            placement='auto'
                                            fromRect={popoverAnchorRect}
                                            supportedOrientations={['portrait', 'landscape']}
                                        >
                                            <View style={styles.sort_box_view}>
                                                {this.state.sortBtnList.map((item, index) => {
                                                    return (
                                                        <TouchableOpacity onPress={() => this.isSelectedSortOption(index)} style={[styles.A_z_text_style_view, { backgroundColor: this.state.selectedSortOption === index ? '#373839' : null }]}>
                                                            <Text style={[styles.A_z_text_style, { fontFamily: this.state.selectedApplyReset === index ? 'OpenSans_semibold' : 'OpenSans_regular' }]}>{item.name}</Text>
                                                        </TouchableOpacity>
                                                    )
                                                })}

                                            </View>
                                        </Popover>
                                    </React.Fragment>
                                )}
                            </PopoverController>

                        </View>
                        {this.state.NoresultFound === false ?
                            <View>
                                <View style={{ marginTop: 34, }}>
                                    <View style={{ flexWrap: 'wrap', justifyContent: 'space-evenly', alignItems: 'center', flexDirection: 'row' }} >
                                        <EventListingTwoSearch eventDataTwoSearchData={this.state.EventListingTwoSearchDAta} />
                                    </View>
                                </View>
                            </View>
                            :

                            <LinearGradient colors={['#2d2d2d', '#000', '#000', '#000',]} style={{ height: '100%' }}>
                                <Image style={{ width: 250, height: 185, alignSelf: 'center', marginTop: 77 }} source={{ uri: 'https://cdn.zeplin.io/5f0c51a79180598d5834f369/assets/E8EA2DDA-49FF-4116-A717-68B7914E1404.png' }}></Image>
                                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 24 }}>
                                    <Text style={styles.uh_oh_text_style}>Uh Oh! Nothing found</Text>
                                    <Text style={styles.odd_filter_text_style}>Those were some really odd filters{"\n"}Try a different filter</Text>
                                </View>
                            </LinearGradient>

                        }
                    </View>
                    <RBSheet
                        ref={ref => {
                            this.RBSheet_filter = ref;
                        }}
                        closeOnDragDown={true}
                        closeOnPressMask={true}
                        height={height}
                        openDuration={250}
                        customStyles={{
                            container: {
                                backgroundColor: '#212225',
                                opacity: 1,
                            },
                            wrapper: {
                                backgroundColor: "transparent"
                            },
                            draggableIcon: {
                                backgroundColor: "transparent"
                            }
                        }}
                    >
                        <View style={styles.filter_main_view_RB}>
                            <View style={styles.title_view_filter_RB}>
                                <Text style={styles.Filters_text_title_RB}>Filters</Text>
                                <MaterialIcons name='close' style={styles.icon_btn_sorting_style} size={24} color='#fff'></MaterialIcons>
                            </View>
                            <View style={styles.category_n_list_view}>
                                <View style={styles.category_view}>
                                    {this.state.sortingcategoryList.map((item, index) => {
                                        return (
                                            <TouchableOpacity onPress={() => this.isSelectedcategorybtn(index)} style={[styles.category_btn_view, { backgroundColor: this.state.selectedIndex === index ? '#252628' : null }]}>
                                                <Text style={[styles.category_btn_text_style, { color: this.state.selectedIndex === index ? '#fff' : '#777' }]}>{item.name}</Text>
                                            </TouchableOpacity>
                                        )
                                    })}

                                </View>
                                <View style={styles.list_view}>
                                    <View style={{ justifyContent: 'flex-start', alignItems: 'center', }}>
                                        {this.state.genreSortingResultList.map((item, index) => {
                                            return (
                                                <TouchableOpacity onPress={() => this.isSelectedListbtn(index)} style={[styles.list_btn_view, { backgroundColor: this.state.selectedListEntity === index ? '#373839' : null }]}>
                                                    <Text style={[styles.category_btn_text_style, { color: this.state.selectedListEntity === index ? '#fff' : '#777' }]}>{item.name}</Text>
                                                    {this.state.selectedListEntity === index ?
                                                        <MaterialIcons name='close' style={[styles.icon_btn_sorting_style, { marginRight: 18 }]} size={15} color='#fff'></MaterialIcons>
                                                        : null
                                                    }
                                                </TouchableOpacity>
                                            )
                                        })}
                                    </View>
                                    <View style={styles.apply_n_reset_view}>
                                        <Text style={styles.Showing_Results_text_style_RB}>Showing 10 Results</Text>
                                        <View style={styles.apply_reset_btn_RB_view}>
                                            {this.state.applyBtn.map((item, index) => {
                                                return (
                                                    <TouchableOpacity onPress={() => this.isSelectedApplyResetbtn(index)} style={[styles.category_btn_view, { flex: 1, justifyContent: 'center', alignItems: 'center', marginBottom: 21, borderRadius: 7, marginHorizontal: 7, backgroundColor: this.state.selectedApplyReset === index ? '#fff' : null }]}>
                                                        <Text style={[styles.category_btn_text_style, { marginLeft: 0, color: this.state.selectedApplyReset === index ? '#000' : '#777' }]}>{item.name}</Text>
                                                    </TouchableOpacity>
                                                )
                                            })}
                                        </View>
                                    </View>
                                </View>

                            </View>
                        </View>
                    </RBSheet>
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}


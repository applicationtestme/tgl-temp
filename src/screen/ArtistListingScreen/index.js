import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, ImageBackground, KeyboardAvoidingView, TextInput } from 'react-native';
import { styles } from './styles';
import { SimilarEvent } from '../../components/SimilarEvent'
import { Genres_round_view } from '../../components/Genres_round_view'
import { ArtistTabVideo } from '../../components/ArtistTabVideo'
import LinearGradient from 'react-native-linear-gradient';
import { Images } from '../../common/Images'
import { Popover, PopoverController } from 'react-native-modal-popover';
import RBSheet from "react-native-raw-bottom-sheet";
import { Fonts } from '../../common/fonts';
import Entypo from 'react-native-vector-icons/dist/Entypo';
import Fontisto from 'react-native-vector-icons/dist/Fontisto';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export default class ArtistListingScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            NoresultFound:false,
            TopArtistList: [
                { name: 'Beyoncé', img_url: 'https://sdlhivkcdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Beyonce_500x500.jpg' },
                { name: 'Selena Gomez', img_url: 'https://sklktecdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Selena_Gomez_002_20200226073835_500x500.jpg' },
                { name: 'David Guetta', img_url: 'https://sklktcdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/David_Guetta_500x500.jpg' },
                { name: 'Demi lovato', img_url: 'https://schnncdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Demi_Lovato_002_20200312120805_500x500.jpg' },
                { name: 'Martin Garrix', img_url: 'https://sklktcdnems01.cdnsrv.jio.com/c.saavncdn.com/artists/Martin_Garrix_004_20200303120820_500x500.jpg' },
                { name: 'Beyoncé', img_url: 'https://sdlhivkcdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Beyonce_500x500.jpg' },
                { name: 'Selena Gomez', img_url: 'https://sklktecdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Selena_Gomez_002_20200226073835_500x500.jpg' },
                { name: 'David Guetta', img_url: 'https://sklktcdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/David_Guetta_500x500.jpg' },
                { name: 'Demi lovato', img_url: 'https://schnncdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Demi_Lovato_002_20200312120805_500x500.jpg' },
                { name: 'Martin Garrix', img_url: 'https://sklktcdnems01.cdnsrv.jio.com/c.saavncdn.com/artists/Martin_Garrix_004_20200303120820_500x500.jpg' },
            ],
            genreSortingResultList: [
                { name: 'Anime' },
                { name: 'Blues' },
                { name: 'Children' },
                { name: 'Classical' },
                { name: 'Comedy' },
                { name: 'Country' },
                { name: 'Dance' },
                { name: 'Electronic' },
                { name: 'Enka' },
                { name: 'French Pop' },
                { name: 'French Pop' },
            ],
            sortingcategoryList: [
                { name: 'Genre' },
                { name: 'Location' },
                { name: 'Date' },
                { name: 'Show Type' },
            ],
            linearBoxData: [
                { name: 'Y&I', img_url: 'https://schnncdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Demi_Lovato_002_20200312120805_500x500.jpg' },
                { name: 'Bonsai Trees', img_url: 'https://sklktcdnems01.cdnsrv.jio.com/c.saavncdn.com/artists/Martin_Garrix_004_20200303120820_500x500.jpg' },
            ],
            applyBtn: [
                { name: 'Reset' },
                { name: 'Apply' },
            ],
            sortBtnList: [
                { name: 'A - Z' },
                { name: 'Z - A' },
                { name: 'Newest First' },
            ]
        };

    }
    Noresult = () => {
        this.setState({ NoresultFound: !this.state.NoresultFound })
    }
    openFilter = () => {
        this.RBSheet_filter.open();
    }

    closeFilter = () => {
        this.RBSheet_filter.close();
    }
    isSelectedcategorybtn = (index) => {
        this.setState({ selectedIndex: index });
    }
    isSelectedListbtn = (index) => {
        this.setState({ selectedListEntity: index });
    }
    isSelectedApplyResetbtn = (index) => {
        this.setState({ selectedApplyReset: index });
    }
    isSelectedSortOption = (index) => {
        this.setState({ selectedSortOption: index });
    }
    render() {

        return (
            <KeyboardAvoidingView behaviour="height" style={styles.main_container}>
                <ScrollView contentContainerStyle={styles.scrlView_container}>
                    <ImageBackground source={Images.artist_lisning} resizeMethod='auto' imageStyle={{ width: '100%', }} style={styles.img_bg_style}>
                        <View style={styles.header_container_style}>
                            <TouchableOpacity style={styles.header_user_img_style}></TouchableOpacity>
                        </View>
                        <View style={styles.artist_dp_view}>
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <ImageBackground source={Images.Dp_rings_img} resizeMode={'cover'} imageStyle={{ resizeMode: 'contain' }} style={styles.dp_ring_view}>
                                    <View style={styles.inside_dp_ring_view}>
                                        <Image source={{ uri: 'https://beta.thegigs.live/sites/default/files/styles/300x300/public/uploads/arists/l3-artist-headshot.png?itok=Apty1Qud' }} style={styles.artist_img_style}></Image>
                                    </View>
                                </ImageBackground>
                                <View style={styles.three_dot_view}>
                                    <TouchableOpacity style={[styles.slide_round_view, { backgroundColor: '#fff' }]} onPress={() => this.selectionSlide(1)}></TouchableOpacity>
                                    <TouchableOpacity style={[styles.slide_round_view, {}]} onPress={() => this.selectionSlide(1)}></TouchableOpacity>
                                    <TouchableOpacity style={[styles.slide_round_view, {}]} onPress={() => this.selectionSlide(1)}></TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View style={styles.artist_title_view}>
                            <Text style={styles.artis_with_new_event_text_style}>Artists with new events</Text>
                            <Text style={styles.artis_name_text_style}>Kendell Marvel</Text>
                        </View>
                        <View style={styles.count_follow_view}>
                            <View style={styles.count_box_view}>
                                <Text style={styles.count_box_title_text_style}>LIVE EVENTS</Text>
                                <Text style={styles.count_box_count_text_style}>353</Text>
                            </View>
                            <View style={styles.count_box_view}>
                                <Text style={styles.count_box_title_text_style}>FAN FOLLOWS</Text>
                                <Text style={styles.count_box_count_text_style}>233</Text>
                            </View>
                            <View style={styles.count_box_view}>
                                <Text style={styles.count_box_title_text_style}>SOC IAL RANK</Text>
                                <Text style={styles.count_box_count_text_style}>353</Text>
                            </View>
                        </View>
                        <View style={styles.know_more_view}>
                            <Text style={styles.know_more_text_style}>Know more</Text>
                            <TouchableOpacity onPress={() => this.Noresult()} style={styles.right_btn_view}>
                                <Fontisto style={styles.social_icon_style} name='arrow-right' size={12} color='#919293'></Fontisto>
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                    <View style={styles.main_view}>
                        <View style={styles.tab_view_container}>
                            <Text style={styles.tab_title_text_style}>SUPERHIT EVENTS FROM THE ARTIST </Text>
                            <ArtistTabVideo ShowTitleData={this.state.ShowTitleData} />
                        </View>
                        <Text style={styles.View_title_text_style}>Top Artists</Text>
                        <View style={styles.top_artist_view}>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                {this.state.TopArtistList.map((item, index) => {
                                    return (
                                        <View style={styles.top_artist_entity_view}>
                                            <ImageBackground resizeMode='contain' imageStyle={{}} source={Images.bright_ring_img} style={styles.top_artits_bright_ring_img_style}>
                                                <Image source={{ uri: item.img_url }} style={styles.top_artist_img_style}></Image>
                                            </ImageBackground>
                                            <Text style={styles.top_artist_name_text_style}>{item.name}</Text>
                                        </View>
                                    )
                                })}
                            </ScrollView>
                        </View>
                        <Text style={styles.View_title_text_style}>Artists You Follow</Text>
                        <View style={styles.top_artist_view}>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                {this.state.TopArtistList.map((item, index) => {
                                    return (
                                        <View style={styles.artist_you_follow_entity_view}>
                                            <Image source={{ uri: item.img_url }} style={styles.top_artist_img_style}></Image>
                                            <Text style={styles.top_artist_name_text_style}>{item.name}</Text>
                                        </View>
                                    )
                                })}
                            </ScrollView>
                        </View>
                        <Text style={styles.Artists_on_Gigs_text_style}>Artists on Gigs</Text>
                        <Text style={styles.Showing_Results_text_style}>Showing 20 Results</Text>
                        <View style={styles.sorting_btn_view}>
                            <TouchableOpacity onPress={() => this.openFilter()} style={styles.filter_btn_style}>
                                <MaterialIcons name='filter-list' style={styles.icon_btn_sorting_style} size={24} color='#fff'></MaterialIcons>
                                <Text style={styles.Filter_title_btn_text_style}>Filter</Text>
                            </TouchableOpacity>
                            {/* /// */}
                            <PopoverController  >
                                {({ openPopover, closePopover, popoverVisible, setPopoverAnchor, popoverAnchorRect }) => (
                                    <React.Fragment>
                                        <TouchableOpacity ref={setPopoverAnchor} onPress={openPopover} style={styles.filter_btn_style}>
                                                    <MaterialIcons name='sort' style={styles.icon_btn_sorting_style} size={24} color='#fff'></MaterialIcons>
                                                    <Text style={styles.Filter_title_btn_text_style}>Sort</Text>
                                                </TouchableOpacity>
                                        <Popover
                                            contentStyle={{backgroundColor:'#252628',borderRadius:10}}
                                            arrowStyle={{borderTopColor:'transparent',}}
                                            backgroundStyle={{backgroundColor:'#252628a6'}}
                                            visible={popoverVisible}
                                            onClose={closePopover}
                                            placement='auto'
                                            fromRect={popoverAnchorRect}
                                            supportedOrientations={['portrait', 'landscape']}
                                        >
                                            <View style={styles.sort_box_view}>
                                            {this.state.sortBtnList.map((item, index) => {
                                                return (
                                               <TouchableOpacity onPress={() => this.isSelectedSortOption(index)} style={[styles.A_z_text_style_view, { backgroundColor: this.state.selectedSortOption === index ? '#373839' : null }]}>
                                                   <Text style={[styles.A_z_text_style,{ fontFamily: this.state.selectedApplyReset === index ? 'OpenSans_semibold' : 'OpenSans_regular' }]}>{item.name}</Text>
                                               </TouchableOpacity>
                                                 )
                                                })}
                                           
                                            </View>
                                        </Popover>
                                    </React.Fragment>
                                )}
                            </PopoverController>

                        </View>
                       {  this.state.NoresultFound === false ?
                        <View>
                       <View style={{ marginTop: 34, }}>
                            <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center' }} >
                                {this.state.TopArtistList.map((item, index) => {
                                    return (
                                        <View style={[styles.artist_you_follow_entity_view, {width:width/2 -50, marginTop: 8,marginHorizontal:20 }]}>
                                        <Image source={{ uri: item.img_url }} style={[styles.artist_result_img_style,{  height: 140,width: 140,borderRadius: 140 / 2,}]}></Image>
                                        <Text style={styles.top_artist_name_text_style}>{item.name}</Text>
                                    </View>
                                    )
                                })}
                            </View>
                        </View>
                        {this.state.linearBoxData.map((item, index) => {
                            return (
                                <LinearGradient colors={['#2d2d2d', 'rgba(33, 33, 33, 0.5)']} style={styles.linearGradient}>
                                    <View style={styles.inside_linear_view}>
                                        <View style={styles.linear_text_data_view}>
                                            <Text style={styles.artist_with_new_event_text_style_box}>Artists With new events</Text>
                                            <Text style={styles.artist_name_text_style_box}>{item.name}</Text>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Text style={styles.know_more_text_style}>Know more</Text>
                                                <TouchableOpacity style={styles.right_btn_view}>
                                                    <Fontisto style={styles.social_icon_style} name='arrow-right' size={12} color='#919293'></Fontisto>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        <View style={styles.linear_img_data_view}>
                                            <Image source={{ uri: 'https://beta.thegigs.live/sites/default/files/styles/300x300/public/uploads/arists/l3-artist-headshot.png?itok=Apty1Qud' }} style={{ height: 100, width: 100, borderRadius: 100 / 2 }}></Image>
                                        </View>
                                    </View>
                                </LinearGradient>
                            )
                        })}
                        <View style={{ marginTop: 34,justifyContent:'center',alignItems:'center', }}>
                            <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center', }} >
                                {this.state.TopArtistList.map((item, index) => {
                                    return (
                                        <View style={[styles.artist_you_follow_entity_view, {width:width/2 -50, marginTop: 8,marginHorizontal:20 }]}>
                                            <Image source={{ uri: item.img_url }} style={[styles.artist_result_img_style,{  height: 140,width: 140,borderRadius: 140 / 2,}]}></Image>
                                            <Text style={styles.top_artist_name_text_style}>{item.name}</Text>
                                        </View>
                                    )
                                })}
                            </View>
                        </View>
                        </View>
                        :
                        
                        <LinearGradient colors={['#2d2d2d','#000','#000','#000',]} style={{height:'100%'}}>
                            <Image style={{width:250,height:185,alignSelf:'center',marginTop:77}} source={{uri:'https://cdn.zeplin.io/5f0c51a79180598d5834f369/assets/E8EA2DDA-49FF-4116-A717-68B7914E1404.png'}}></Image>
                            <View style={{justifyContent:'center',alignItems:'center',marginTop:24}}>
                                <Text style={styles.uh_oh_text_style}>Uh Oh! Nothing found</Text>
                            <Text style={styles.odd_filter_text_style}>Those were some really odd filters{"\n"}Try a different filter</Text>
                            </View>
                          </LinearGradient>
                        
                        }
                    </View>
                    <RBSheet
                        ref={ref => {
                            this.RBSheet_filter = ref;
                        }}
                        closeOnDragDown={true}
                        closeOnPressMask={true}
                        height={height}
                        openDuration={250}
                        customStyles={{
                            container: {
                                backgroundColor: '#212225',
                                opacity: 1,
                            },
                            wrapper: {
                                backgroundColor: "transparent"
                            },
                            draggableIcon: {
                                backgroundColor: "transparent"
                            }
                        }}
                    >
                        <View style={styles.filter_main_view_RB}>
                            <View style={styles.title_view_filter_RB}>
                                <Text style={styles.Filters_text_title_RB}>Filters</Text>
                                <MaterialIcons name='close' style={styles.icon_btn_sorting_style} size={24} color='#fff'></MaterialIcons>
                            </View>
                            <View style={styles.category_n_list_view}>
                                <View style={styles.category_view}>
                                    {this.state.sortingcategoryList.map((item, index) => {
                                        return (
                                            <TouchableOpacity onPress={() => this.isSelectedcategorybtn(index)} style={[styles.category_btn_view, { backgroundColor: this.state.selectedIndex === index ? '#252628' : null }]}>
                                                <Text style={[styles.category_btn_text_style, { color: this.state.selectedIndex === index ? '#fff' : '#777' }]}>{item.name}</Text>
                                            </TouchableOpacity>
                                        )
                                    })}

                                </View>
                                <View style={styles.list_view}>
                                    <View style={{ justifyContent: 'flex-start', alignItems: 'center', }}>
                                        {this.state.genreSortingResultList.map((item, index) => {
                                            return (
                                                <TouchableOpacity onPress={() => this.isSelectedListbtn(index)} style={[styles.list_btn_view, { backgroundColor: this.state.selectedListEntity === index ? '#373839' : null }]}>
                                                    <Text style={[styles.category_btn_text_style, { color: this.state.selectedListEntity === index ? '#fff' : '#777' }]}>{item.name}</Text>
                                                    {this.state.selectedListEntity === index ?
                                                        <MaterialIcons name='close' style={[styles.icon_btn_sorting_style, { marginRight: 18 }]} size={15} color='#fff'></MaterialIcons>
                                                        : null
                                                    }
                                                </TouchableOpacity>
                                            )
                                        })}
                                    </View>
                                    <View style={styles.apply_n_reset_view}>
                                        <Text style={styles.Showing_Results_text_style_RB}>Showing 10 Results</Text>
                                        <View style={styles.apply_reset_btn_RB_view}>
                                            {this.state.applyBtn.map((item, index) => {
                                                return (
                                                    <TouchableOpacity onPress={() => this.isSelectedApplyResetbtn(index)} style={[styles.category_btn_view, { flex: 1, justifyContent: 'center', alignItems: 'center', marginBottom: 21, borderRadius: 7, marginHorizontal: 7, backgroundColor: this.state.selectedApplyReset === index ? '#fff' : null }]}>
                                                        <Text style={[styles.category_btn_text_style, {marginLeft:0, color: this.state.selectedApplyReset === index ? '#000' : '#777' }]}>{item.name}</Text>
                                                    </TouchableOpacity>
                                                )
                                            })}
                                        </View>
                                    </View>
                                </View>

                            </View>
                        </View>
                    </RBSheet>
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}


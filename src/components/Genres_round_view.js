import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Fonts } from '../common/fonts';
import { Images } from '../common/Images';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
export class Genres_round_view extends Component {
    constructor() {
        super()
            this.state = {
            
        }
    }
    render() {
        return (
            <View style={{ width: '100%', backgroundColor: '#101012' }}>
             { this.props.titleVisible === false ? null :
                <View style={styles.event_for_you_title_view}>
                    <View style={{ flex: 1 }}>
                        <Text style={styles.events_for_you_text_title}>{this.props.homeScreenUse === true ? 'Explore the Genres' : 'Genres' }</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-end', marginRight: 20 }}>
                        <TouchableOpacity>
                            <Text style={styles.see_all_text_title}>See All</Text>
                        </TouchableOpacity>
                    </View>
                </View>}
                <View style={styles.similar_artist_suggestion_view}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        <TouchableOpacity style={[styles.similar_artist_box_view__style, { marginLeft: 20 }]}>
                            <Image source={Images.rock_genre} style={styles.similar_artist_img__style}>
                            </Image>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.similar_artist_box_view__style}>
                            <Image source={Images.eletronic} style={styles.similar_artist_img__style}>
                            </Image>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.similar_artist_box_view__style}>
                            <Image source={Images.instrument} style={styles.similar_artist_img__style}>
                            </Image>
                        </TouchableOpacity>

                    </ScrollView>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    event_for_you_title_view: {
        flexDirection: 'row',
        marginLeft: 20
    },
    events_for_you_text_title: {
        fontFamily: Fonts.PlayfairDisplay_Bold,
        color: '#dddddd',
        fontSize: 18,
        marginTop: 24,
    },
    see_all_text_title: {
        color: '#dddddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        marginTop: 31,
    },
    similar_artist_suggestion_view: {
        flexDirection: 'row',
        marginVertical: 20,
        alignSelf: 'flex-start'
    },
    similar_artist_img__style: {
        width: 150,
        height: 129,
        // borderRadius: 150 / 2,
    },
    similar_artist_box_view__style: {
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 0,
        height:129
    },
})



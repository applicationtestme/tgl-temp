import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, Pressable, KeyboardAvoidingView, TextInput } from 'react-native';
import { styles } from './styles';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import { TshirtPaymnet } from '../TshirtPaymnet'
export class TshirtForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            fullName: '',
            mobileNumber: '',
            addressType: 'Home',
            addressLine1: 'A-916, Siddhivinayak Tower',
            area_street: 'Makarba',
            landMark: 'Near Kataria house',
            city: 'Ahmedabad',
            state: 'Gujarat',
            country: 'India',
            formSubmitted: false
        }
    }

    submit = () => {
        this.setState({ formSubmitted: true })
    }

    setAddressType = (addressType) => {
        this.setState({ addressType })
    }

    render() {
        const { formSubmitted, fullName, mobileNumber, addressType, addressLine1, area_street, landMark, city, state, country, } = this.state
        return (
            <View style={styles.main_container}>
                <View style={styles.titleView}>
                    <Icon name="ios-chevron-back" color={'#ddd'} size={15} />
                    <Text style={styles.title}>Select Tshirt size</Text>
                </View>
                {
                    !formSubmitted ?
                    <View style={styles.formMainContent}>
                        <Text style={styles.formTitle}>Add New Address</Text>
                        <FormLabel label={'Full Name *'} />
                        <FormTextBox value={fullName} onChangeText={(value) => this.setState({ fullName: value })} />

                        <FormLabel label={'Mobile Number *'} />
                        <FormTextBox value={mobileNumber} onChangeText={(value) => this.setState({ mobileNumber: value })} />

                        <FormLabel label={'Address Type *'} />
                        <View style={styles.radioGroup}>
                            <Radio label="Home" isSelected={addressType === "Home"} selectRadio={() => this.setAddressType("Home")} />
                            <Radio label="Work" isSelected={addressType === "Work"} selectRadio={() => this.setAddressType("Work")} />
                        </View>

                        <FormLabel label={'Flat, House No., Building, Company, Apartment'} />
                        <FormTextBox value={addressLine1} onChangeText={(value) => this.setState({ addressLine1: value })} />

                        <FormLabel label={'Area, Street'} />
                        <FormTextBox value={area_street} onChangeText={(value) => this.setState({ area_street: value })} />

                        <FormLabel label={'Landmark'} />
                        <FormTextBox value={landMark} onChangeText={(value) => this.setState({ landMark: value })} />

                        <FormLabel label={'Town / City'} />
                        <FormTextBox value={city} onChangeText={(value) => this.setState({ city: value })} />

                        <FormLabel label={'State'} />
                        <FormTextBox value={state} onChangeText={(value) => this.setState({ state: value })} />

                        <FormLabel label={'Country'} />
                        <FormTextBox value={country} onChangeText={(value) => this.setState({ country: value })} />

                        <Pressable onPress={this.submit}
                            style={({ pressed }) => [
                                styles.submitButton, pressed && { backgroundColor: '#767676' }
                            ]}
                        >
                            <Text style={styles.submitButtonText}>Deliver to this Address</Text>
                        </Pressable >

                        <FormLabel label={'Disclaimer'} formLabelStyle={{ marginLeft: 0, fontSize: 12 }} />
                        <FormLabel label={'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore etdolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute iruredolor in reprehenderit in voluptate velit esse cillumdolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia'} formLabelStyle={{ marginLeft: 0, fontSize: 12 }} />
                    </View>
                    :
                    <TshirtPaymnet 
                        address={`${addressType}: ${addressLine1}\n${area_street}, ${landMark}\n${city}\n${state}, ${country}`} 
                        onAddressChange={()=> {
                            this.setState({formSubmitted: false})                            
                    }}
                    />
                }
            </View>
        );
    }
}

class FormLabel extends Component {
    render() {
        return (
            <Text style={[styles.formLabelStyle, this.props.formLabelStyle]}>{this.props.label}</Text>
        );
    }
}

class FormTextBox extends Component {
    render() {
        return (
            <TextInput
                style={styles.textInputStyle}
                value={this.props.value}
                onChangeText={(value) => this.props.onChangeText(value)}
            />
        );
    }
}

class Radio extends Component {
    render() {
        const { isSelected, label } = this.props
        return (
            <TouchableOpacity style={styles.radioItem} onPress={this.props.selectRadio}>
                <TouchableOpacity style={styles.outerRadio} onPress={this.props.selectRadio}>
                    {
                        isSelected &&
                        <TouchableOpacity style={styles.innnerRadio} onPress={this.props.selectRadio} />
                    }
                </TouchableOpacity>
                <FormLabel label={label} />
            </TouchableOpacity>
        );
    }
}


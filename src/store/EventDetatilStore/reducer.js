import { act } from "react-test-renderer"

let initialState = {
  userData: null,
  vipSupporters: [],
  type: '',
}

export default function (state = initialState, action) {
  switch (action.type) {

    case "EVENTDETAIL_ON_LOAD_BEGIN":
      {
        return {
          isLoading: true
        }
      }
    case "EVENTDETAIL_ON_LOAD_SUCCESS":
      {
        return {
          ...state,
          type: action.type,
          userData: action.userData,
          commentList: action.commentList,
          EventsForYou: action.EventsForYou,
          getUserByAlbum: action.getUserByAlbum.getUserBuyAlbum,
          getArtistPastEventList: action.getArtistPastEventList.pastEvents,
          getSimilerArtistList: action.getSimilerArtistList.similerArtist,
          getEventSchedule: action.getEventSchedule.getEventSchedule,
          isLoading: false,

        }
      }

    case "EVENTDETAIL_ON_LOAD_FAIL":
      {
        return {
          ...state,
          err: action.err,
          isLoading: false,
        }
      }


    case "EVENTDETAIL_FOLLOW_ARTIST_BEGIN":
      {
        return {
          ...state,
          isLoading: true
        }
      }
    case "EVENTDETAIL_FOLLOW_ARTIST_SUCCESS":
      {
        return {
          ...state,
          type: action.type,
          followArtist: action.followArtist,
          isLoading: false,

        }
      }

    case "EVENTDETAIL_FOLLOW_ARTIST_FAIL":
      {
        return {
          ...state,
          err: action.err,
          isLoading: false,
        }
      }

    case "EVENTDETAIL_BUY_TICKET_BEGIN":
      {
        return {
          ...state,
          isLoading: true
        }
      }
    case "EVENTDETAIL_BUY_TICKET_SUCCESS":
      {
        return {
          ...state,
          type: action.type,
          buyTicket: action.buyTicket,
          getTicket: action.getTicket,
          isLoading: false,

        }
      }

    case "EVENTDETAIL_BUY_TICKET_FAIL":
      {
        return {
          ...state,
          err: action.err,
          isLoading: false,
        }
      }

    case "EVENTDETAIL_SEND_COMMENT_BEGIN":
      {
        return {
          ...state,
          isLoading: false
        }
      }
    case "EVENTDETAIL_SEND_COMMENT_SUCCESS":
      {
        return {
          ...state,
          type: action.type,
          sendComment: action.sendComment,
          isLoading: false,
        }
      }

    case "EVENTDETAIL_SEND_COMMENT_FAIL":
      {
        return {
          ...state,
          err: action.err,
          isLoading: false,
        }
      }


    case "GET_EVENT_VIPSUPPORTERS_SUCCESS":
      return {
        ...state,
        vipSupporters: action.vipSupporters,
        no_participants: action.vipSupporters.no_participants
      }


    default:
      return state;
  }
}

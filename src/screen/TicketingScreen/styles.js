import { StyleSheet, Dimensions } from 'react-native';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import { Fonts } from '../../common/fonts';
export const styles = StyleSheet.create({
    scrlView_container: {
        flexGrow: 1,
        backgroundColor: '#101113',
        paddingBottom: 50
    },
    main_container: {
        flex: 1,
        backgroundColor: '#101113'
    }
})
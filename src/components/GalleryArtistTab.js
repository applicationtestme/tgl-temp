import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Fonts } from '../common/fonts';
import { Images } from '../common/Images';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export class GalleryArtistTab extends Component {

    constructor(props) {
        super(props)
        this.state = {

        }

    }

    render() {
        return (
           
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <View>
                    <TouchableOpacity style={styles.image_view_style}>
                        <Image style={styles.image_container} source={{ uri: 'https://schnncdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Taylor_Swift_003_20200226074119_500x500.jpg' }}></Image>
                    </TouchableOpacity>
                </View>

                <View>
                    <View style={styles.image_resize_container}>
                        <TouchableOpacity style={styles.image_small_size_style}>
                            <Image style={styles.image_container} source={{ uri: 'https://schnncdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Taylor_Swift_003_20200226074119_500x500.jpg' }}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.image_small_size_style}>
                            <Image style={styles.image_container} source={{ uri: 'https://schnncdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Taylor_Swift_003_20200226074119_500x500.jpg' }}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.image_small_size_style}>
                            <Image style={styles.image_container} source={{ uri: 'https://schnncdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Taylor_Swift_003_20200226074119_500x500.jpg' }}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.image_small_size_style}>
                            <Image style={styles.image_container} source={{ uri: 'https://schnncdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Taylor_Swift_003_20200226074119_500x500.jpg' }}></Image>
                        </TouchableOpacity>
                        
                    </View> 
                    <View style={styles.image_resize_container}>
                        <TouchableOpacity style={styles.image_mid_size_style}>
                            <Image style={styles.image_container} source={{ uri: 'https://schnncdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Taylor_Swift_003_20200226074119_500x500.jpg' }}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.image_mid_size_style}>
                            <Image style={styles.image_container} source={{ uri: 'https://schnncdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Taylor_Swift_003_20200226074119_500x500.jpg' }}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.image_mid_size_style}>
                            <Image style={styles.image_container} source={{ uri: 'https://schnncdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Taylor_Swift_003_20200226074119_500x500.jpg' }}></Image>
                        </TouchableOpacity>
                    </View>
                </View>
               
            </ScrollView>
            
        )
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1
    },
    image_view_style: {
        height:245,
        width:150,
        marginRight:10,
       marginTop:10
    },
    image_small_size_style:{
        height:100,
        width:85,
        marginRight:10,
       marginTop:10
    },
    image_mid_size_style:{
        height:123,
        width:120,
        marginRight:10,
       marginTop:10
    },
    image_container: {
        height:undefined,
        width:undefined,
        flex:1,
        borderRadius:3
    },
    image_resize_container:{
        marginTop:5,
        width:'100%',
        flexDirection:'row'
    }
})
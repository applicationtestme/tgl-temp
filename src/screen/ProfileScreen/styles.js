import { StyleSheet, Dimensions } from 'react-native';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import { Fonts } from '../../common/fonts';
export const styles = StyleSheet.create({
    scrlView_container: {
        flexGrow: 1,  
        
    },
    main_container: {
        flex: 1,
        
    },
    body_contianer:{
        backgroundColor:'#101113',
        paddingBottom:20
    },
    header_container_style: {
        backgroundColor: 'transparent',
        position: 'absolute',
        backgroundColor: 'transparent',
        zIndex: 100,
        top: 0,
        left: 0,
        right: 0,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0,
        marginTop: 10
    },
    header_user_img_style: {
        width: 24,
        height: 24,
        borderRadius: 24 / 2,
        backgroundColor: '#ddd',
        marginLeft: 16,
        marginVertical: 12
    },
    img_bg_style: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    artist_img_viewer:{
        paddingHorizontal:20,
        marginLeft:5,
        marginTop:68,
        width:'100%',
        backgroundColor:'#fff',
        height:435,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'transparent',
        
    },
    artist_following_details_container:{
        flexDirection:"row",
        backgroundColor:'#000000',
        marginHorizontal:16,
        borderRadius:10,
    },
    gigs_enjoy_count_num_view:{
        flex:1
    },
    box_title_view:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    following_details_title_text:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
        color: '#dddddd',
        marginTop:25
    },
    box_following_count_view:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    following_details_count_text:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 24,
        color: '#dddddd',
        marginTop:5,
        marginBottom:30
    },
    location_n_details_view:{
        marginTop:24,
        marginHorizontal:16,
        justifyContent:'flex-start',
        alignItems:'flex-start',
        alignSelf:'flex-start',
        width:'100%',
        
    },
    location_n_social_icon_view:{
        alignItems:'center',
        justifyContent:'flex-start',
        flexDirection:'row'
    },
    location_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 16,
        color: '#dddddd',
    },
    social_btn_view:{
        justifyContent:'center',
        alignItems:"center",
        backgroundColor:'#252628',
        borderRadius:5,
        marginRight:8
    },
    social_icon_style:{
        marginVertical:12,
        marginHorizontal:13
    },
    follow_pop_up_btn:{
        borderRadius:10,
        backgroundColor:'#fff',
        justifyContent:'center',
        alignItems:'center',
        width:'40%',
        marginTop:16,
        flexDirection:'row'
    },
    Artist_name_text:{
        color: '#fff',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 24,
    },
    follow_btn_text:{
        color: '#1b1c20',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        marginVertical:10,
    },
    medal_img_style:{
        resizeMode:'contain',
        height:'20%',
        width:'20%',
        marginRight:20
    },
    about_you_view:{
        marginHorizontal:17,
        marginTop:16,
        flex:1,
        marginBottom:41
    },
    about_you_text_style:{
        color: '#ddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
    },
    there_here_for_musical_text_style:{
        marginTop:8,
        marginBottom:24,
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
        color: 'rgba(221, 221, 221, 0.5)',
    },
    graph_view_container:{
        borderRadius:10,
        backgroundColor:'#242527',
        width:'100%',
        paddingVertical:16,
        paddingLeft:24,
        paddingRight:19
    },
    Profile_Remaining_text_style:{
        color:'#525355',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 16,
    },
    pertage_text_style:{
        color:'#ddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
    },
    graph_img_view:{
        justifyContent:'center',
        alignItems:'center',
        height:100
    },
    graph_img_style:{
        width:'100%',
    },
    nextup_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
        color: 'rgba(221, 221, 221, 0.5)',
    },
    Setup_Instagram_text_style:{
        color:'#dddddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 16,
    },
    ultra_budge_view:{
        borderTopWidth:1,
        borderColor:'#2d2e2f',
        marginHorizontal:16,
        
    },
    ultra_badge_n_data_view:{
        marginTop:24,
        flexDirection:'row'
    },
    user_badge_view:{
        flex:1,
        borderWidth:1,
        backgroundColor:'#343536',
        borderRadius:10,
        justifyContent:'center',
        alignItems:'center'
    },
    budge_data_view:{
        flex:1.5,
        marginLeft:17
    },
    bedge_title_text_style:{
        color:'#dddddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 16,
    },
    bedge_description_text_style:{
        marginTop:8,
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 16,
        color: 'rgba(221, 221, 221, 0.5)',
    },
    badge_icon_style:{
        resizeMode:'contain',
        width:66,
        height:100,
        marginHorizontal:24,
        marginBottom:20
    },
    now_on_gigs_view:{
        backgroundColor:'#343536',
        borderRadius:10,
        flexDirection:'row',
        marginHorizontal:16,
        marginVertical:24,
        width: '91%'
    },
    dollor_pocket_img_view:{
        justifyContent:'center',
        alignItems:'center',
        // backgroundColor:'red',
        marginTop:24,
        marginLeft:25,
        marginBottom:26,
        marginRight:28,
        flex:1
    },
    dollor_beg_img_style:{
        width:54,
        height:72
    },
    now_on_gigs_data_view:{
        marginVertical:24,
        flex:1
    },
    NEW_text_style:{
        color:'#dddddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        // marginVertical:3,
        // marginHorizontal:25
    },
    line_new_view:{
        backgroundColor:'#888889',
        elevation:5,
        justifyContent:'center',
        alignItems:'center',
        height:12,
        width:79,
        top:23,
        left:11,
        position:'relative',
        transform:[{rotate:'40deg'}],
        // flex:1 
    },
    Badges_earned_view:{
        marginTop:24,
        marginLeft:16,
        borderTopLeftRadius:15,
        borderBottomLeftRadius:15,
        backgroundColor:'#27282a',
        width:'100%',
        
    },
    Badges_Earned_text_style:{
        color: '#fff',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 16,
        marginTop:20,
        marginLeft:20
    },
    budge_icon_container:{
        flexDirection:'row',
        // marginLeft:20
    },
    unlimited_fan_badge_view:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    badge_img_style:{
        marginTop:5,
        marginBottom:21
    },
    Chris_Photos_view:{
        marginTop:24,
        marginLeft:16
    },
    Chris_Photos_text_style:{
        color: '#fff',
        fontFamily: Fonts.PlayfairDisplay_Bold,
        fontSize: 18,
        marginBottom:8
    },
    Chris_Photos_inside_view:{
        flexDirection:'row'
    },
    upload_pic_btn_view:{
        borderWidth:1,
        borderStyle:"dashed",
        borderColor:'#777',
        borderRadius:10,
        backgroundColor:'#2b2b2b',
        justifyContent:'center',
        alignItems:'center',
        marginRight:8
    },
    camera_btn_round_view:{
        width:40,
        height:40,
        borderRadius:40/2,
        borderWidth:1,
        borderColor:'rgba(221, 221, 221, 0.5)',
        justifyContent:'center',
        alignItems:'center'
    },
    upload_pic_inside_btn_view:{
        justifyContent:'center',
        alignItems:'center',
        marginHorizontal:51,
        marginVertical:60
    },
    upload_pic_text_style:{
        marginTop:16,
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
        color: 'rgba(221, 221, 221, 0.5)',
    },
    saved_event_view:{
        marginTop:24,
        marginLeft:16,
    },
    artist_following_view:{
        marginTop:24,
        marginLeft:16,
    },
    explore_all_artist_btn_view:{
        backgroundColor:'#c4c4c4',
        borderRadius:6,
        marginTop:15
    },
    explore_all_artist_text_style:{
        color: '#141414',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        marginHorizontal:16,
        marginVertical:12
    },
    explore_the_genre_view:{
        marginTop:24,
        
    },
    explore_genre_text_style:{
        marginHorizontal:40,
        marginVertical:49,
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
        color: 'rgba(221, 221, 221, 0.5)',
    },
    fan_like_you_view:{
        marginTop:24,
        marginLeft:16,
    },
    featureFanView:{
        marginRight:65
    },
    featureFanUserImg:{
        width:96,
        height:96,
        borderRadius:96/2
    },
    feature_Fan_name_text_style:{
        fontFamily: 'PlayfairDisplaySC-Bold',
        fontSize: 12,
        color: '#fff',
        marginTop:3
    },
    feature_Fan_address_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 11,
        color: '#fff',
        marginTop:5
    },

    /// edit page
    body_contianer_edit_page:{
        flex:1,
        backgroundColor:'#101113',
        paddingBottom:20
    },
    location_input_view:{
        flexDirection:'row',
        marginLeft:16,
        marginTop:24,
        justifyContent:'flex-start',
        alignItems:'center',
    },
    location_text_input_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 16,
        color: '#dddddd',
    },
    twiter_input_view:{
        flexDirection:'row',
        marginLeft:16,
        justifyContent:'flex-start',
        alignItems:'center',
    },
    social_btn_view_edit_page:{
        justifyContent:'center',
        alignItems:"center",
        backgroundColor:'#252628',
        borderRadius:5,
        height:32,
        width:32
    },
    about_you_edit_page:{
        marginHorizontal:16,
        marginTop:24
    },
    about_you_edit_page_title:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    about_you_edit_page_text_style:{
        color: '#fff',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 16,
    },
    remaning_text_count_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
        color: 'rgba(221, 221, 221, 0.5)',
    },
    paregraph_view:{
        width:'100%',
        borderWidth:1,
        borderColor:'#fff',
        borderRadius:10,
        justifyContent:'flex-start',
        marginTop:8
    },
    save_change_btn_view:{
        backgroundColor:'#d8d8d8',
        borderRadius:10,
        justifyContent:'center',
        alignItems:'center',
        width:192,
        height:40,
        marginTop:16,
        marginBottom:26
    },
    Saves_Changes_text_style:{
        color: '#000',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 16,
    },
    //// tooltip
    tooltip_main_view:{
        borderRadius:20,
        // backgroundColor:'rgba(43, 43, 43, 0.9)',
        width:'100%'
    },
    tooltip_title_view:{
        marginTop:10,
        marginHorizontal:16,
        flexDirection:'row'
    },
    badge_icon_view:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    toottip_title_view:{
        marginTop:10,
        flex:2,
        marginLeft:16,
        paddingRight:20
    },
    title_text_style:{
        color: '#fff',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
    },
    Easy_Badge_text_style:{
        color:'rgba(221, 221, 221, 0.5)',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        marginTop:4
    },
    close_btn_view_tool_tip:{
        flex:1,
        marginLeft:25,
    },
    status_text_style:{
        color:'rgba(221, 221, 221, 0.5)',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        marginTop:15,
        marginLeft:18
    },
    details_text_style:{
        color:'rgba(221, 221, 221, 0.5)',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
    },
    nexttip_decription_text_style:{
        color:'#fff',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
    },
    tooltip_white_btn_style:{
        backgroundColor:'#c4c4c4',
        borderRadius:7,
        justifyContent:'center',
        alignItems:'center',
        marginLeft:16,
        marginBottom:24,
        width:'55%'
    },
    tooltip_white_btn_text_style:{
        color:'#141414',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        marginVertical:10
    }
})
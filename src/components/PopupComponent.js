import React, { Component } from 'react';
import { Text, View, Modal, StyleSheet, ScrollView, TouchableOpacity, TouchableHighlight } from 'react-native';
import { Fonts } from '../common/fonts';
import { Popover, PopoverController } from 'react-native-modal-popover';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export class PopupComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
        };
    }

    render() {
        return (
            <PopoverController>
                {({ openPopover, closePopover, popoverVisible, setPopoverAnchor, popoverAnchorRect }) => (
                    <React.Fragment>
                        {
                            this.props.roomSelection &&
                            <TouchableOpacity onPress={openPopover} ref={setPopoverAnchor} style={styles.TouchableOpacityStyle} >
                                <Text style={styles.dropDownTextStyle}>{this.props.SelectedValue}</Text>
                                <Icon name="caret-down-sharp" onPress={openPopover} ref={setPopoverAnchor} size={15} color="#fff" />
                            </TouchableOpacity>
                        }
                        {
                            this.props.threeDotsMenu &&
                            <TouchableOpacity onPress={openPopover} ref={setPopoverAnchor}>
                                <MaterialCommunityIcons name="dots-vertical" color="#ddd" size={25} />
                            </TouchableOpacity>
                        }
                        <Popover
                            contentStyle={styles.content}
                            arrowStyle={styles.arrow}
                            backgroundStyle={styles.background}
                            visible={popoverVisible}
                            onClose={closePopover}
                            fromRect={popoverAnchorRect}
                            supportedOrientations={['portrait', 'landscape']}
                        >
                            {
                                this.props.menuList.map((item, index) => {
                                    return (
                                        <View>
                                            <TouchableOpacity
                                                style={
                                                    [
                                                        this.props.SelectedValue == item.name
                                                            ?
                                                            styles.popupTextView_selected
                                                            :
                                                            styles.popupTextView
                                                        ,
                                                        index === 0 ?
                                                            {
                                                                borderTopLeftRadius: 8,
                                                                borderTopRightRadius: 8
                                                            }
                                                            :
                                                            index === this.props.menuList.length - 1
                                                                ?
                                                                {
                                                                    borderBottomLeftRadius: 8,
                                                                    borderBottomRightRadius: 8
                                                                }
                                                                :
                                                                null
                                                    ]
                                                }
                                                onPress={() => { this.props.handelValue(item); closePopover() }}
                                            >
                                                <Text style={styles.popupTextStyle}>{item.name}</Text>
                                                {
                                                    item.hasNotification === 1 &&
                                                    <View style={styles.popupTextNotification} />
                                                }
                                                {
                                                    item.isAdmin === 1 &&
                                                    <Text style={styles.popupTextStylesub}>Admin</Text>
                                                }
                                            </TouchableOpacity>
                                            {this.props.menuList.length - 1 > index && <View style={styles.popupSeprator} />}
                                        </View>
                                    )
                                })
                            }
                        </Popover>
                    </React.Fragment>
                )}
            </PopoverController>
        )
    }
}
const styles = StyleSheet.create({
    content: {
        padding: 0,
        backgroundColor: '#252628',
        borderRadius: 8,
        opacity: 1
    },
    arrow: {
        borderTopColor: 'transparent',
    },
    background: {
    },
    popupSeprator: {
        borderBottomWidth: 0.2,
        borderColor: '#dddddd'
    },
    popupTextView: {
        flexDirection: 'row',
        paddingVertical: 12,
        borderWidth: 1,
        borderColor: 'transparent',
        alignItems: 'center',
        paddingHorizontal: 16
    },
    popupTextView_selected: {
        flexDirection: 'row',
        paddingVertical: 12,
        borderWidth: 1,
        borderColor: 'transparent',
        alignItems: 'center',
        backgroundColor: '#494a4c',
        paddingHorizontal: 16
    },
    popupTextStyle: {
        flex: 1,
        marginHorizontal: 5,
        color: "#fff",
        fontFamily: Fonts.OpenSans_regular
    },
    popupTextStylesub: {
        color: "#fff",
        fontSize: 12,
        fontFamily: Fonts.OpenSans_regular
    },
    popupTextNotification: {
        height: 5,
        width: 5,
        marginRight: 10,
        backgroundColor: '#fff',
        borderRadius: 5,
    },
    TouchableOpacityStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    dropDownTextStyle: {
        color: '#fff',
        marginRight: 16
    }
});

import React, { Component } from 'react';
import { InteractionManager, Dimensions, Text, ScrollView, View, Image, TouchableOpacity, KeyboardAvoidingView, TextInput } from 'react-native';
import { styles } from './styles';
import { Header, UserRow, UserProfile_vip_notVip_Withcancel, FullButton, GettingReady } from '../../components'
import { Images } from '../../common/Images'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ImagePicker from 'react-native-image-picker';


const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export default class RoomInfoScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            supporterList: [
                { name: '', hasUpdate: false }, { name: '', hasUpdate: false }, { name: '', hasUpdate: true },
                { name: '', hasUpdate: false }, { name: '', hasUpdate: true }, { name: '', hasUpdate: true },
                { name: '', hasUpdate: false }, { name: '', hasUpdate: true }, { name: '', hasUpdate: false },
            ],
            groupName: 'Beyonce Club',
            groupNameMaxLength: 24,
            invitedUsersList: [],
            maxInviteLimit: 25,
            searchValue: '',
            participants: [
                {
                    userName: 'Anuja',
                    badges: 19,
                    address: 'Tennessey, USA',
                    isVip: 1,
                    isAdmin: 1
                },
                {
                    userName: 'Bruce',
                    badges: 21,
                    address: 'Kolkata, India',
                    isVip: 0,
                    isAdmin: 0
                },
                {
                    userName: 'Anuja',
                    badges: 2,
                    address: 'Tennessey, USA',
                    isVip: 1,
                    isAdmin: 0
                },
                {
                    userName: 'Anuja',
                    badges: 31,
                    address: 'Tennessey, USA',
                    isVip: 0,
                    isAdmin: 0
                }
            ],
            searchResult: [],
            isReady: false,
            filePath: null
        }
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            this.setState({
                isReady: true
            })
        });
    }

    changeGroupName = (groupName) => {
        if (groupName.length < 25) {
            this.setState({ groupName })
        }
    }

    searchUser = (searchValue) => {
        let participants = this.state.participants;
        let searchResult = []
        for (let i = 0; i < participants.length; i++) {
            if (participants[i].userName.indexOf(searchValue) !== -1) {
                searchResult.push(participants[i])
            }
        }
        this.setState({ searchResult, searchValue })
    }

    sendInvite = (user) => {
        let invitedUsersList = this.state.invitedUsersList;
        invitedUsersList.push(user)
        this.setState({ invitedUsersList })
    }

    removeUserFromInvitedList = (index) => {
        let invitedUsersList = this.state.invitedUsersList;
        invitedUsersList.splice(index, 1)
        this.setState({ invitedUsersList })
    }

    saveCahnges = () => {
        alert('Save - Coming soon')
    }

    deleteRoom = () => {
        alert('Delete - Coming soon')
    }

    addParticipant = () => {
        alert('Add participants - Coming soon')
    }

    goBack = () => {
        this.props.navigation.goBack()
    }

    removeUser = (index) => {
        let participants = this.state.participants;
        participants.splice(index, 1)
        this.setState({ participants })
    }
    chooseFile = () => {
        var options = {
            title: 'Select Image',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, response => {

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                let source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    filePath: source
                }, () => {
                });
            }
        });
    };

    render() {
        const { participants, groupName, groupNameMaxLength, invitedUsersList, maxInviteLimit, searchValue, searchResult } = this.state
        if (!this.state.isReady) {
            return (
                <GettingReady />
            )
        } else {
            return (
                <View style={styles.main_container}>
                    <Header />
                    <ScrollView contentContainerStyle={styles.scrlView_container}>
                        <View style={styles.screenHeadingView}>
                            <Text style={styles.headingTextStyle}>Room Info</Text>
                            <TouchableOpacity style={styles.closeIconStyle} onPress={this.goBack}>
                                <MaterialCommunityIcons name="close" color="#ddd" size={20} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.addNamePhotoView}>
                            <TouchableOpacity style={styles.cameraIconViewStyle} onPress={() => this.chooseFile()}>
                                {
                                    this.state.filePath == null ?
                                        <Ionicons name="camera-outline" size={30} color={'#6d6e6f'} />
                                        :
                                        <Image source={this.state.filePath} style={styles.roomImage} />
                                }
                                {/* <Text>Image</Text> */}
                            </TouchableOpacity>
                            <View style={styles.textBoxView}>
                                <TextInput
                                    placeholder={'Group Name'}
                                    placeholderTextColor="#ddd"
                                    style={styles.textInputStyle}
                                    value={groupName}
                                    onChangeText={(groupName) => this.changeGroupName(groupName)}
                                />
                                <Text style={styles.groupNameLengthStyle}>{groupName.length} / {groupNameMaxLength}</Text>
                            </View>
                        </View>
                        <FullButton buttonText={'Save Changes'} onPress={this.saveCahnges} buttonStyle={styles.saveButtonStyle} />
                        <Text style={styles.groupDetailsStyle}>Group Created by You</Text>
                        <Text style={styles.groupDetailsStyle}>Created on: 23 July 2020</Text>
                        <View style={styles.invitePartiipantsCountChip}>
                            <Text style={styles.invitePartiipantsCountTextChip}>PARTICIPANTS: {participants.length} OF {maxInviteLimit}</Text>
                        </View>
                        <View style={styles.participantListHeader}>
                            <View style={styles.searchBox}>
                                <Ionicons name="search-outline" size={30} color={'#6d6e6f'} style={styles.searchIconStyle} />
                                <TextInput
                                    placeholder={'Search'}
                                    placeholderTextColor="#ddd"
                                    style={styles.searchTextBoxStyle}
                                    value={searchValue}
                                    onChangeText={(searchValue) => this.searchUser(searchValue)}
                                />
                            </View>
                            <View style={styles.added_participantsVeiw}>
                                {
                                    invitedUsersList.map((addedUser, index) => {
                                        return (
                                            <View style={styles.addedUserItemStyle}>
                                                <UserProfile_vip_notVip_Withcancel isVip={addedUser.isVip} showCancelButton={true} onRemove={() => this.removeUserFromInvitedList(index)} />
                                                <Text style={styles.addedUserTextStyle}>{addedUser.userName}</Text>
                                            </View>
                                        )
                                    })
                                }
                            </View>
                        </View>
                        <TouchableOpacity onPress={this.addParticipant} style={styles.addParticipantsView}>
                            <MaterialCommunityIcons name="plus" size={30} color={'#ddd'} />
                            <Text style={styles.addParticipantsViewTextStyle}>Add Participants</Text>
                        </TouchableOpacity>
                        <View style={styles.participantListContainer}>
                            {
                                searchValue === '' && searchResult.length === 0 && participants.length !== 0 && participants.map((user, index) => {
                                    return (
                                        <UserRow roomInfo={true} user={user} onRemove={() => this.removeUser(index)} />
                                    )
                                })
                            }
                            {
                                searchResult.length !== 0 ? searchResult.map((user, index) => {
                                    return (
                                        <UserRow roomInfo={true} user={user} onRemove={() => this.removeUser(index)} />
                                    )
                                })
                                    :
                                    searchValue !== '' &&
                                    <Text style={styles.globalPartiipantsCountTextChip}>No user found</Text>
                            }
                        </View>
                    </ScrollView>
                    <FullButton buttonText={'Delete Room'} onPress={this.deleteRoom} />
                </View>
            )
        }
    }
}
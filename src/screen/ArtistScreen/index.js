import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, ImageBackground, KeyboardAvoidingView, TextInput } from 'react-native';
import { styles } from './styles';
import { Header, UserChatActionButtons, Similar_artist, Artist_TabEvents,SimilarEvent } from '../../components'
import { Images } from '../../common/Images'
import { ChatRoom } from '../index'
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/dist/Ionicons';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height


export default class ArtistScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            login: false,
            bought_ticket: false,
            timer: 120,
            timer_counting: false,
            without_ticket_live_view: false,
            artistData: [
                {
                    'artistName': 'Taylor Swift',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://schnncdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Taylor_Swift_003_20200226074119_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Martin Garrix',
                    'eventName': 'Summer Tour',
                    'artistProfileImage': 'https://sklktcdnems01.cdnsrv.jio.com/c.saavncdn.com/artists/Martin_Garrix_004_20200303120820_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'David Guetta',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://sklktcdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/David_Guetta_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Selena Gomez',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://sklktecdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Selena_Gomez_002_20200226073835_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Demi Lovato',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://schnncdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Demi_Lovato_002_20200312120805_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Slash',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://snoidcdnems01.cdnsrv.jio.com/c.saavncdn.com/607/You-re-A-Lie-English-2012-500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
            ]
        }
    }

    render() {
        return (
            <KeyboardAvoidingView behaviour="height" style={styles.main_container}>
                <ScrollView contentContainerStyle={styles.scrlView_container}>
                    <View style={styles.header_container_style}>
                        <TouchableOpacity style={styles.header_user_img_style}></TouchableOpacity>
                    </View>
                    <ImageBackground source={Images.mainBackground} style={styles.img_bg_style}>
                        <Text style={styles.singer_name_text_style}>Beyonce</Text>
                        <View style={styles.genres_text_view_style}>
                            <Text style={styles.genres_text_style}>Genres:  </Text>
                            <Text style={styles.genres_type_text_style}>Rock  •  </Text>
                            <Text style={styles.genres_type_text_style}>Pop  •   </Text>
                            <Text style={styles.genres_type_text_style}>Punk Rock</Text>
                        </View>
                        <View style={styles.following_N_rating_container}>
                            <View style={styles.follower_view}>
                                <Image source={Images.followers_icon} style={styles.follower_icon_style}></Image>
                                <Text style={styles.followers_text_style}>30K Followers  </Text>
                                <Text style={[styles.followers_text_style, { fontSize: 25, marginVertical: 0 }]}>•</Text>
                            </View>
                            <View style={styles.follower_view}>
                                <Image source={Images.ratings_icon} style={styles.follower_icon_style}></Image>
                                <Text style={styles.followers_text_style}>4.7 Rating</Text>
                            </View>
                        </View>
                        <TouchableOpacity onPress={() => { this.openSheet() }} style={styles.follow_artist_btn_style}>
                            <Text style={styles.follow_artist_text_style}>Follow the Artist</Text>
                        </TouchableOpacity>
                        <View style={styles.upcoming_event_for_view_container}>
                            <View style={styles.event_for_you_title_view}>
                                <View style={{ flex: 1 }}>
                                    <Text style={styles.upcoming_event_text_title}>Upcoming Events</Text>
                                </View>
                                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                    <TouchableOpacity>
                                        <Text style={styles.see_all_text_title}>See All</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </ImageBackground>
                    <View style={styles.event_for_view_container}>

                        <ImageBackground source={Images.lady_gaga_concer} style={styles.lady_gaga_concert_img_bg_style}>
                            <LinearGradient colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0,0.35)', '#000']} style={styles.linearGradient2}>
                                <View style={{ marginLeft: 25, }}>

                                    <View style={styles.artist_name_n_concert_name_container}>
                                        <View>
                                            <Image style={styles.lady_gaga_user_pic_style} source={Images.lady_gaga}></Image>
                                        </View>
                                        <View style={{ alignItems: 'flex-start', marginTop: 10 }}>
                                            <Text style={styles.artist_name_title_text_event_for_you_style}>Lady Gaga's</Text>
                                            <Text style={styles.concert_title_text_style}>Enigma</Text>
                                            <Text style={styles.concert_timing_text_style}>Aug 25 4:30 PM (PST)</Text>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ flex: 0 }}>
                                                    <TouchableOpacity style={styles.buy_tickets_concert_artist_btn_style}>
                                                        <Text style={styles.buy_ticket_concert_artist_text_style}>Buy Tickets</Text>
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={{ marginLeft: '40%' }}>
                                                    <TouchableOpacity style={{}}>
                                                        <Image style={{}} source={Images.right_arrow_padding}></Image>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </LinearGradient>
                        </ImageBackground>
                        <ImageBackground source={Images.bruno_concert} style={[styles.lady_gaga_concert_img_bg_style, {}]}>
                            <LinearGradient colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0,0.91)', '#000']} style={styles.linearGradient2}>
                                <View style={{ marginLeft: 25, }}>
                                    <View style={styles.artist_name_n_concert_name_container}>
                                        <View>
                                            <Image style={styles.lady_gaga_user_pic_style} source={Images.bruno}></Image>
                                        </View>
                                        <View style={{ alignItems: 'flex-start', marginTop: 10 }}>
                                            <Text style={styles.artist_name_title_text_event_for_you_style}>Bruno Mars’</Text>
                                            <Text style={styles.concert_title_text_style}>Finesse World Tour…</Text>
                                            <Text style={styles.concert_timing_text_style}>Aug 25 4:30 PM (PST)</Text>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ flex: 0 }}>
                                                    <TouchableOpacity style={styles.buy_tickets_concert_artist_btn_style}>
                                                        <Text style={styles.buy_ticket_concert_artist_text_style}>Buy Tickets</Text>
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={{ marginLeft: '40%', justifyContent: 'center' }}>
                                                    <TouchableOpacity style={{}}>
                                                        <Image style={{ height: 24, width: 24 }} source={Images.right_arrow_padding}></Image>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </LinearGradient>
                        </ImageBackground>
                        <Artist_TabEvents />
                        <Similar_artist />
                        <View style={styles.event_for_you_title_view}>
                            <View style={{ flex: 1 }}>
                                <Text style={styles.events_for_you_text_title}>Similar Events</Text>
                            </View>
                            <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                <TouchableOpacity>
                                    <Text style={styles.see_all_text_title}>See All</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.similar_artist_suggestion_view}>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                <SimilarEvent
                                artistData = {this.state.artistData}
                                />
                                {/* {
                                    this.state.artistData.map((item, index) => {
                                        return (
                                            <TouchableOpacity style={styles.similar_artist_box_view__style}>
                                                <ImageBackground source={{ uri: item.artistProfileImage }} style={styles.similar_artist_img__style}>
                                                    <LinearGradient colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0,0.91)', '#000']} style={styles.linearGradient2}>
                                                        <View style={styles.inside_contant}>
                                                            <Text style={styles.similar_artist_name_text__style}>{item.artistNa }</Text>
                                                            <Text style={styles.similar_artist_event_name_text__style}>{item.eventName}</Text>
                                                            <Text style={styles.similar_artist_event_time_name_text__style}>{item.eventTime}</Text>
                                                            <TouchableOpacity style={styles.similar_artist_event_buy_ticket_btn__style}>
                                                                <Text style={styles.similar_artist_event_buy_ticket_text_btn__style}>Buy Tickets</Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    </LinearGradient>
                                                </ImageBackground>
                                            </TouchableOpacity>
                                        )
                                    })
                                } */}
                                {/* <TouchableOpacity style={styles.similar_artist_box_view__style}>
                                    <ImageBackground source={{ uri: 'https://sklktcdnems01.cdnsrv.jio.com/c.saavncdn.com/artists/Martin_Garrix_004_20200303120820_500x500.jpg' }} style={styles.similar_artist_img__style}>
                                        <LinearGradient colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0,0.91)', '#000']} style={styles.linearGradient2}>
                                            <View style={styles.inside_contant}>
                                                <Text style={styles.similar_artist_name_text__style}>Martin Garrix</Text>
                                                <Text style={styles.similar_artist_event_name_text__style}>Summer Tour</Text>
                                                <Text style={styles.similar_artist_event_time_name_text__style}>Aug 25 4:30 PM (PST)</Text>
                                                <TouchableOpacity style={styles.similar_artist_event_buy_ticket_btn__style}>
                                                    <Text style={styles.similar_artist_event_buy_ticket_text_btn__style}>Buy Tickets</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </LinearGradient>
                                    </ImageBackground>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.similar_artist_box_view__style}>
                                    <ImageBackground source={{ uri: 'https://sklktcdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/David_Guetta_500x500.jpg' }} style={styles.similar_artist_img__style}>
                                        <LinearGradient colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0,0.91)', '#000']} style={styles.linearGradient2}>
                                            <View style={styles.inside_contant}>
                                                <Text style={styles.similar_artist_name_text__style}>David Guetta</Text>
                                                <Text style={styles.similar_artist_event_name_text__style}>Parachutes</Text>
                                                <Text style={styles.similar_artist_event_time_name_text__style}>Aug 25 4:30 PM (PST)</Text>
                                                <TouchableOpacity style={styles.similar_artist_event_buy_ticket_btn__style}>
                                                    <Text style={styles.similar_artist_event_buy_ticket_text_btn__style}>Buy Tickets</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </LinearGradient>
                                    </ImageBackground>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.similar_artist_box_view__style}>
                                    <ImageBackground source={{ uri: 'https://sklktecdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Selena_Gomez_002_20200226073835_500x500.jpg' }} style={styles.similar_artist_img__style}>
                                        <LinearGradient colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0,0.91)', '#000']} style={styles.linearGradient2}>
                                            <View style={styles.inside_contant}>
                                                <Text style={styles.similar_artist_name_text__style}>Selena Gomez</Text>
                                                <Text style={styles.similar_artist_event_name_text__style}>Parachutes</Text>
                                                <Text style={styles.similar_artist_event_time_name_text__style}>Aug 25 4:30 PM (PST)</Text>
                                                <TouchableOpacity style={styles.similar_artist_event_buy_ticket_btn__style}>
                                                    <Text style={styles.similar_artist_event_buy_ticket_text_btn__style}>Buy Tickets</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </LinearGradient>
                                    </ImageBackground>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.similar_artist_box_view__style}>
                                    <ImageBackground source={{ uri: 'https://schnncdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Demi_Lovato_002_20200312120805_500x500.jpg' }} style={styles.similar_artist_img__style}>
                                        <LinearGradient colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0,0.91)', '#000']} style={styles.linearGradient2}>
                                            <View style={styles.inside_contant}>
                                                <Text style={styles.similar_artist_name_text__style}>Demi Lovato</Text>
                                                <Text style={styles.similar_artist_event_name_text__style}>Parachutes</Text>
                                                <Text style={styles.similar_artist_event_time_name_text__style}>Aug 25 4:30 PM (PST)</Text>
                                                <TouchableOpacity style={styles.similar_artist_event_buy_ticket_btn__style}>
                                                    <Text style={styles.similar_artist_event_buy_ticket_text_btn__style}>Buy Tickets</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </LinearGradient>
                                    </ImageBackground>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.similar_artist_box_view__style}>
                                    <ImageBackground source={{ uri: 'https://snoidcdnems01.cdnsrv.jio.com/c.saavncdn.com/607/You-re-A-Lie-English-2012-500x500.jpg' }} style={styles.similar_artist_img__style}>
                                        <LinearGradient colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0,0.91)', '#000']} style={styles.linearGradient2}>
                                            <View style={styles.inside_contant}>
                                                <Text style={styles.similar_artist_name_text__style}>Slash</Text>
                                                <Text style={styles.similar_artist_event_name_text__style}>Parachutes</Text>
                                                <Text style={styles.similar_artist_event_time_name_text__style}>Aug 25 4:30 PM (PST)</Text>
                                                <TouchableOpacity style={styles.similar_artist_event_buy_ticket_btn__style}>
                                                    <Text style={styles.similar_artist_event_buy_ticket_text_btn__style}>Buy Tickets</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </LinearGradient>
                                    </ImageBackground>
                                    <Text style={styles.similar_artist_name_text__style}></Text>
                                </TouchableOpacity> */}
                            </ScrollView>
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }
}


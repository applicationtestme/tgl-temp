/**
 * @format
 */
import React from 'react'; // Remember to import React
import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './src/store/sagas.js'
import rootReducer from './src/store/reducers'
import configureStore from './src/store/configureStore'
const store = configureStore({});

const RNRedux = () => (
    <Provider store={store}>
        <App />
    </Provider>
);

AppRegistry.registerComponent(appName, () => RNRedux);

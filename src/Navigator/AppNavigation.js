import React, { Component } from 'react';
import { ScrollView, View, Text, Image, TextInput, TouchableOpacity } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createSwitchNavigator, createAppContainer } from 'react-navigation'
import EventDetailScreen from '../screen/EventDetailScreen'
import AddRoomScreen from '../screen/AddRoomScreen'
import RoomInfoScreen from '../screen/RoomInfoScreen'
import ChatRoom from '../screen/ChatRoom'
import ParticipantsScreen from '../screen/ParticipantsScreen'
import ArtistScreen from '../screen/ArtistScreen'
import  TicketingScreen  from '../screen/TicketingScreen'
import  DemoScreen  from '../screen/DemoScreen'
import SelectTicketScreen from '../screen/SelectTicketScreen'
import TicketFlowScreen from '../screen/TicketFlowScreen'
import ChooseShippingAddress  from '../screen/ChooseShippingAddress'
import ProfileScreen from '../screen/ProfileScreen'
import HomeScreen from '../screen/HomeScreen'
import ArtistListingScreen from '../screen/ArtistListingScreen'
import FanPageScreen from '../screen/FanPageScreen'
import EventListingScreen from '../screen/EventListingScreen'

const Stack = createStackNavigator({
    DemoScreen: {
        screen: DemoScreen,
        navigationOptions: {
            headerShown: false
        }
    },
    EventListingScreen: {
        screen: EventListingScreen,
        navigationOptions: {
            headerShown: false
        }
    },
    TicketFlowScreen: {
        screen: TicketFlowScreen,
        navigationOptions: {
            headerShown: false
        }
    },
    ChooseShippingAddress: {
        screen: ChooseShippingAddress,
        navigationOptions: {
            headerShown: false
        }
    },
    SelectTicketScreen: {
        screen: SelectTicketScreen,
        navigationOptions: {
            headerShown: false
        }
    },

    EventDetailScreen: {
        screen: EventDetailScreen,
        navigationOptions: {
            headerShown: false
        }
    },

    TicketingScreen: {
        screen: TicketingScreen,
        navigationOptions: {
            headerShown: false
        }
    },
    ArtistScreen: {
        screen: ArtistScreen,
        navigationOptions: {
            headerShown: false
        }
    },



    AddRoomScreen: {
        screen: AddRoomScreen,
        navigationOptions: {
            headerShown: false
        }
    },
    RoomInfoScreen: {
        screen: RoomInfoScreen,
        navigationOptions: {
            headerShown: false
        }
    },
    ChatRoom: {
        screen: ChatRoom,
        navigationOptions: {
            headerShown: false
        }
    },
    ParticipantsScreen: {
        screen: ParticipantsScreen,
        navigationOptions: {
            headerShown: false
        }
    },
    ProfileScreen: {
        screen: ProfileScreen,
        navigationOptions: {
            headerShown: false
        }
    },
    HomeScreen: {
        screen: HomeScreen,
        navigationOptions: {
            headerShown: false
        }
    },
    ArtistListingScreen: {
        screen: ArtistListingScreen,
        navigationOptions: {
            headerShown: false
        }
    },
    FanPageScreen: {
        screen: FanPageScreen,
        navigationOptions: {
            headerShown: false
        }
    }
});

const SwitchNavigator = createSwitchNavigator(
    {
        Stack: Stack,
    },
    {
        initialRouteName: 'Stack'
    }
)

const AppContainer = createAppContainer(SwitchNavigator)
export default AppContainer
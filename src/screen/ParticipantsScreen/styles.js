import { StyleSheet, Dimensions } from 'react-native';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import { Fonts } from '../../common/fonts';
export const styles = StyleSheet.create({
    main_container:{
        backgroundColor:'#101113'
    },
    scrlView_container:{
        backgroundColor:'#101113',
        marginHorizontal:16,
        paddingBottom:15
    },
    participants_title_container:{
        flexDirection:'row'
    },
    blank_space_view_title:{
        flex:1,
    },
    title_name_n_chat_tag_view:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    close_btn_view:{
        flex:1,
        justifyContent:'center',
        alignItems:'flex-end'
    },
    participants_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        color: '#ffffff',
        marginTop:12,
        marginBottom:4
    },
    global_chat_title_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#ffffff',
        marginBottom:10
    },
    close_btn_round_view_style:{
        width:30,
        height:30,
        borderRadius:30/2,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#232426',
        marginRight:15
    },
    tab_btn_text_style:{
        marginVertical:10,
        fontSize:12,
        color:'#fff',
    },
    tab_btn_style:{
        borderColor:'#dddddd',
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        borderTopLeftRadius:10,
        borderTopRightRadius:10
    },
})
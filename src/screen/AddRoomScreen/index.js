import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, InteractionManager } from 'react-native';
import { styles } from './styles';
import { Header, UserRow, UserProfile_vip_notVip_Withcancel, FullButton, GettingReady } from '../../components'
import { Images } from '../../common/Images'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { TextInput } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export default class AddRoomScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            supporterList: [
                { name: '', hasUpdate: false }, { name: '', hasUpdate: false }, { name: '', hasUpdate: true },
                { name: '', hasUpdate: false }, { name: '', hasUpdate: true }, { name: '', hasUpdate: true },
                { name: '', hasUpdate: false }, { name: '', hasUpdate: true }, { name: '', hasUpdate: false },
            ],
            groupName: '',
            groupNameMaxLength: 24,
            invitedUsersList: [],
            maxInviteLimit: 25,
            searchValue: '',
            usersList: [
                {
                    userName: 'Anuja',
                    badges: 19,
                    address: 'Tennessey, USA',
                    isVip: 1
                },
                {
                    userName: 'Bruce',
                    badges: 21,
                    address: 'Kolkata, India',
                    isVip: 0
                },
                {
                    userName: 'Anuja',
                    badges: 2,
                    address: 'Tennessey, USA',
                    isVip: 1
                },
                {
                    userName: 'Anuja',
                    badges: 31,
                    address: 'Tennessey, USA',
                    isVip: 0
                }
            ],
            searchResult: [],
            isReady: false,
            filePath: ''
        }
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            this.setState({
                isReady: true
            })
        });
    }

    changeGroupName = (groupName) => {
        if (groupName.length < 25) {
            this.setState({ groupName })
        }
    }

    searchUser = (searchValue) => {
        let usersList = this.state.usersList;
        let searchResult = []
        for (let i = 0; i < usersList.length; i++) {
            if (usersList[i].userName.indexOf(searchValue) !== -1) {
                searchResult.push(usersList[i])
            }
        }

        this.setState({ searchResult, searchValue })
    }

    sendInvite = (user) => {
        let invitedUsersList = this.state.invitedUsersList;
        invitedUsersList.push(user)
        this.setState({ invitedUsersList })
    }

    removeUserFromInvitedList = (index) => {
        let invitedUsersList = this.state.invitedUsersList;
        invitedUsersList.splice(index, 1)
        this.setState({ invitedUsersList })
    }

    creatingRoom = () => {
        alert('Coming soon')
    }

    goBack = () => {
        this.props.navigation.goBack()
    }

    chooseFile = () => {
        var options = {
            title: 'Select Image',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                let source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({ filePath: source });
            }
        });
    }

    render() {
        const { usersList, groupName, groupNameMaxLength, invitedUsersList, maxInviteLimit, searchValue, searchResult } = this.state
        if (!this.state.isReady) {
            return (
                <GettingReady />
            )
        } else {
            return (
                <View style={styles.main_container}>
                    <Header />
                    <ScrollView contentContainerStyle={styles.scrlView_container}>
                        <View style={styles.screenHeadingView}>
                            <Text style={styles.headingTextStyle}>Create Room</Text>
                            <TouchableOpacity style={styles.closeIconStyle} onPress={this.goBack}>
                                <MaterialCommunityIcons name="close" color="#ddd" size={20} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.addNamePhotoView}>
                            <TouchableOpacity style={styles.cameraIconViewStyle} onPress={() => this.chooseFile()}>
                                {
                                    this.state.filePath == '' ?
                                        <Ionicons name="camera-outline" size={30} color={'#6d6e6f'} />
                                        :
                                        <Image source={this.state.filePath} style={styles.roomImage} />
                                }
                            </TouchableOpacity>
                            <View style={styles.textBoxView}>
                                <TextInput
                                    placeholder={'Group Name'}
                                    placeholderTextColor="#ddd"
                                    style={styles.textInputStyle}
                                    value={groupName}
                                    onChangeText={(groupName) => this.changeGroupName(groupName)}
                                />
                                <Text style={styles.groupNameLengthStyle}>{groupName.length} / {groupNameMaxLength}</Text>
                            </View>
                        </View>
                        <View style={styles.invitePartiipantsCountChip}>
                            <Text style={styles.invitePartiipantsCountTextChip}>INVITE PARTICIPANTS: {invitedUsersList.length} OF {maxInviteLimit}</Text>
                        </View>
                        <View style={styles.participantListHeader}>
                            <View style={styles.searchBox}>
                                <Ionicons name="search-outline" size={30} color={'#6d6e6f'} style={styles.searchIconStyle} />
                                <TextInput
                                    placeholder={'Search'}
                                    placeholderTextColor="#ddd"
                                    style={styles.searchTextBoxStyle}
                                    value={searchValue}
                                    onChangeText={(searchValue) => this.searchUser(searchValue)}
                                />
                            </View>
                            {
                                invitedUsersList.length !== 0 &&
                                <View style={styles.added_participantsVeiw}>
                                    {
                                        invitedUsersList.map((addedUser, index) => {
                                            return (
                                                <View style={styles.addedUserItemStyle}>
                                                    <UserProfile_vip_notVip_Withcancel isVip={addedUser.isVip} showCancelButton={true} onRemove={() => this.removeUserFromInvitedList(index)} />
                                                    <Text style={styles.addedUserTextStyle}>{addedUser.userName}</Text>
                                                </View>
                                            )
                                        })
                                    }
                                </View>
                            }
                        </View>
                        <View style={styles.participantListContainer}>
                            <Text style={styles.globalPartiipantsCountTextChip}>GLOBAL PARTICIPANTS ({usersList.length})</Text>
                            {
                                searchValue === '' && searchResult.length === 0 && usersList.length !== 0 && usersList.map((user, index) => {
                                    return (
                                        <UserRow creatingRoom={true} user={user} onInvite={() => this.sendInvite(user)} />
                                    )
                                })
                            }
                            {
                                searchResult.length !== 0 ? searchResult.map((user, index) => {
                                    return (
                                        <UserRow creatingRoom={true} user={user} onInvite={() => this.sendInvite(user)} />
                                    )
                                })
                                    :
                                    searchValue !== '' &&
                                    <Text style={styles.globalPartiipantsCountTextChip}>No user found</Text>
                            }
                        </View>
                    </ScrollView>
                    <FullButton buttonText={'Create Room'} onPress={this.creatingRoom} />
                </View>
            );
        }
    }
}
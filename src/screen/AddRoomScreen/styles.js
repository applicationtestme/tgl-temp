import { StyleSheet, Dimensions } from 'react-native';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import { Fonts } from '../../common/fonts';
const padding_horizontal = 16
export const styles = StyleSheet.create({
    scrollContainer: {
        height: height - 75
    },
    scrlView_container: {
        flexGrow: 1
    },
    main_container: {
        flex: 1,
        backgroundColor: '#090a0c'
    },
    screenHeadingView: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 20
    },
    headingTextStyle: {
        color: '#fff',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        flex: 1,
        alignSelf :'center',
        textAlign: 'center',
        marginLeft: 30
    },
    closeIconStyle: {
        height: 30,
        width: 30,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#212123',
        marginRight: 16
    },
    addNamePhotoView: {
        flexDirection: 'row',
        paddingHorizontal: 16
    },
    cameraIconViewStyle: {
        backgroundColor: '#3d3e3f',
        height: 48,
        width: 48,
        borderRadius: 48,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textBoxView: {
        marginLeft: 10,
        borderRadius: 12,
        height: 48,
        borderWidth: 1,
        borderColor: '#979797',
        paddingHorizontal: 10,
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center'
    },
    textInputStyle: {
        color: '#ddd',
        flex: 1
    },
    groupNameLengthStyle: {
        color: '#fff',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
    },
    invitePartiipantsCountChip: {
        backgroundColor: '#262729',
        paddingHorizontal: 16,
        paddingVertical: 10,
        marginTop: 20
    },
    invitePartiipantsCountTextChip: {
        color: '#fff',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
    },
    participantListHeader :{
        paddingVertical: 12,
        paddingHorizontal: 16
    },
    added_participantsVeiw: {
        flexDirection: 'row',
        marginTop: 10
    },
    addedUserItemStyle: {
        marginRight: 16,
        alignItems: 'center'
    },
    addedUserTextStyle: {
        color: '#fff',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        marginTop: 8
    },
    searchBox: {
        flexDirection: 'row',
        backgroundColor: '#262729',
        borderRadius: 35,
        paddingVertical: 4,
        paddingHorizontal: 12,
        alignItems: 'center',
    },
    searchIconStyle: {
        
    },
    searchTextBoxStyle: {
        color: '#ddd',
        height: 40,
        width: '90%',
        borderRadius: 35,
    },
    participantListContainer: {
        backgroundColor: '#0c0d0e',
        paddingHorizontal: 16
    },
    globalPartiipantsCountTextChip: {
        marginVertical: 10,
        color: '#fff',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
    },
    roomImage:{
        height: 48,
        width: 48,
        borderRadius: 48/2
    },
})
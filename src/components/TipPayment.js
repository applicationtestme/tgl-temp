import React, { Component } from 'react';
import { Dimensions, Text, TextInput, View, Image, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import { Fonts } from '../common/fonts';
import { Images } from '../common/Images';
import Icon from 'react-native-vector-icons/dist/Ionicons';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

const tipAmount = [
    { amount: 1 },
    { amount: 2 },
    { amount: 4 },
    { amount: 5 },
    { amount: 10 },
    { amount: 15 }
]
const bankNameAndCVV = [
    { bankName: 'Bank Of America', CVVNum: '1020', isPaypal: false },
    { bankName: 'Bank Of America', CVVNum: '1234', isPaypal: false },
    { bankName: 'Bank Of America', CVVNum: '5678', isPaypal: false },
    { bankName: 'Pay Using PayPal', CVVNum: '5678', isPaypal: true },
]
export class TipPayment extends Component {

    constructor(props) {
        super(props)
        this.state = {
            tipAmount: [
                { amount: 1 },
                { amount: 2 },
                { amount: 4 },
                { amount: 5 },
                { amount: 10 },
                { amount: 15 }
            ],
            SelectedAmount: null,
            tipMessage: ''
        }
    }

    SelectedAmount = (val) => {
        this.setState({ SelectedAmount: val }, () => {
        })
    }
    isSelectedbtn = (index) => {
        this.setState({ selectedIndex: index });
    }

    render() {
        return (

            <View style={{}}>
                <View style={styles.participants_title_container}>
                    <View style={styles.blank_space_view_title}>
                        <Icon name='arrow-back' size={24} color='#dddddd'></Icon>
                    </View>
                    <View style={styles.title_name_n_chat_tag_view}>
                        <Text style={styles.participants_text_style}>Tip & Support the Artist</Text>
                        <Text style={styles.global_chat_title_text_style}>100% Tips go to the Artists directly</Text>
                    </View>
                    <View style={styles.close_btn_view}>
                        <TouchableOpacity onPress={this.props.closeTips} style={styles.close_btn_round_view_style}>
                            <Icon name='close' size={24} color='#dddddd'></Icon>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.body_view_tips_popup_view}>
                    {
                        this.props.step == 1 ?
                            <View style={styles.tips_money_btn_view}>
                                {
                                    tipAmount.map((item, index) => {
                                        return (
                                            <TouchableOpacity
                                                style={[styles.tips_btn_style, { backgroundColor: this.state.SelectedAmount == item.amount ? '#fff' : 'rgba(255, 255, 255, 0.3)' }]}
                                                onPress={() => this.SelectedAmount(item.amount)}>
                                                <Text style={[styles.doller_text_style, { color: this.state.SelectedAmount == item.amount ? '#1b1c20' : '#fff' }]}>$ {item.amount}</Text>
                                            </TouchableOpacity>
                                        )
                                    })
                                }
                                <TouchableOpacity style={styles.custom_tips_btn_style}>
                                    <View style={[styles.doller_sign_custom_style, { backgroundColor: this.state.SelectedAmount == -1 ? "#fff" : "#bebebf" }]}>
                                        <Text style={styles.doller_sign_text_style}>$</Text>
                                    </View>
                                    <View style={styles.textinput_custom_style}>
                                        <TextInput style={styles.text_input_style_custom} placeholder="Custom Tip  "
                                            onChangeText={(val) => this.SelectedAmount(val)}
                                            placeholderTextColor={'#1b1c20'}></TextInput>
                                    </View>
                                </TouchableOpacity>
                                <View style={styles.send_msg_container}>
                                    <Text style={styles.send_msg_text_style}>Send a Message</Text>
                                    <View style={styles.textarea_style}>
                                        <View style={styles.text_msg_input_view}>
                                           
                                            <TextInput
                                                numberOfLines={10}
                                                placeholder={'Your Message here...'}
                                                placeholderTextColor={'#fff'}
                                                onChangeText={(val) => this.setState({ tipMessage: val })}
                                                style={{ color: '#fff', textAlignVertical: "top" , }}

                                            >
                                                
                                            </TextInput>
                                        </View>
                                        <View style={styles.lenght_text_view}>
                                            <Text style={styles.lenght_text_style}>{this.state.tipMessage.length}/50</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            /* ////////////////   saved card  //////////////// */
                            :
                            this.props.step == 2 ?
                                <View>
                                    <Text style={styles.select_amount_text_style}>Saved Cards</Text>
                                    <ScrollView style={{ height: 200 }}>
                                        {
                                            bankNameAndCVV.map((item, index) => {
                                                return (
                                                    item.isPaypal === false &&
                                                    <View style={styles.saved_card_container}>
                                                        <View style={styles.round_selected_btn}>
                                                            <TouchableOpacity onPress={() => this.isSelectedbtn(index)} style={styles.round_radio_style}>
                                                                <View style={[styles.selected_btn_style, { backgroundColor: this.state.selectedIndex === index ? '#ddd' : null }]}></View>
                                                            </TouchableOpacity>
                                                        </View>
                                                        <View style={styles.bank_name_num_container}>
                                                            <Text style={styles.bank_name_text_style_cvv_container}>{item.bankName}</Text>
                                                            <View style={{ flexDirection: 'row' }}>
                                                                <Text style={styles.account_num_text_style_star}>**** </Text>
                                                                <Text style={styles.account_num_text_style_star}>{item.CVVNum}</Text>
                                                            </View>
                                                        </View>
                                                        <View style={styles.cvv_btn_view}>
                                                            <TouchableOpacity style={styles.cvv_btn_style}>
                                                                <Text style={styles.CVV_text_style}>CVV</Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    </View>
                                                )
                                            })}
                                    </ScrollView>
                                    <View style={styles.use_diff_card_view}>
                                        <Text style={styles.select_amount_text_style}>Use a Different Card</Text>
                                    </View>
                                    <View style={styles.border_view_style}></View>
                                    {bankNameAndCVV.map((item, index) => {
                                        return (
                                            item.isPaypal === true &&
                                            <View style={styles.saved_card_container}>
                                                <View style={styles.round_selected_btn}>
                                                    <TouchableOpacity onPress={() => this.isSelectedbtn(index)} style={styles.round_radio_style}>
                                                        <View style={[styles.selected_btn_style, { backgroundColor: this.state.selectedIndex === index ? '#ddd' : null }]}></View>
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={styles.pay_using_paypal_container}>
                                                    <Text style={styles.pay_using_pay_pal_text_style}>Pay Using PayPal</Text>
                                                </View>
                                            </View>
                                        )
                                    })
                                    }


                                </View>
                                : null
                    }
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginVertical: 19 }}>
                        <TouchableOpacity style={styles.payment_btn}
                            onPress={() => this.props.stepHandel(this.state.SelectedAmount, this.state.tipMessage)}
                        >
                            {this.props.step == 1 ?
                                <Text style={styles.payment_btn_text}>Proceed to Payment ($ {this.state.SelectedAmount})</Text>
                                :
                                <Text style={styles.payment_btn_text}>Pay ($ {this.state.SelectedAmount})</Text>
                            }
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    participants_title_container: {
        flexDirection: 'row',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        borderBottomWidth: 1
    },
    blank_space_view_title: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title_name_n_chat_tag_view: {
        flex: 3,
        justifyContent: 'center',
        alignItems: 'center'
    },
    close_btn_view: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    participants_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        color: '#ffffff',
        marginTop: 20,
        marginBottom: 4
    },
    global_chat_title_text_style: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#ffffff',
        marginBottom: 11
    },
    close_btn_round_view_style: {
        width: 30,
        height: 30,
        borderRadius: 30 / 2,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor:'#232426',
        marginRight: 15
    },
    select_amount_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        color: '#ffffff',
        marginBottom: 11.5
    },
    body_view_tips_popup_view: {
        marginHorizontal: 16,
        marginTop: 26,
    },
    tips_money_btn_view: {
        flexDirection: 'row',
        flexWrap: 'wrap',

        alignItems: 'center'
    },
    tips_btn_style: {
        backgroundColor: 'rgba(255, 255, 255, 0.3)',
        borderRadius: 12,
        marginTop: 9,
        justifyContent: 'center',
        alignItems: 'center',
        width: width / 5 - 12,
        marginRight: 12

    },
    doller_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 16,
        color: '#ffffff',
        marginVertical: 9,
    },
    custom_tips_btn_style: {
        flexDirection: 'row',
        borderRadius: 10,
        height: 40,
        backgroundColor: 'rgba(255, 255, 255, 0.3)',
        width: width / 2 - 30,
        marginTop: 8
    },
    doller_sign_custom_style: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#dddddd',
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10
    },
    doller_sign_text_style: {
        color: '#1b1c20',
        fontSize: 18,
        fontFamily: Fonts.OpenSans_semibold,
    },
    textinput_custom_style: {
        flex: 4,
        backgroundColor: 'rgba(255, 255, 255, 0.3)',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10
    },
    text_input_style_custom: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
    },
    send_msg_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        color: '#ffffff',
    },
    send_msg_container: {
        marginTop: 24,
        width: '100%'
    },
    textarea_style: {
        marginTop: 9,
        borderRadius: 10,
        width: '100%',
        backgroundColor: '#3d3d3e94'
    },
    text_msg_input_view: {
        marginHorizontal: 16,
        marginTop: 16,
        backgroundColor:'transparent'
    },
    lenght_text_view: {
        justifyContent: 'center',
        alignItems: 'flex-end',
        marginBottom: 8,
        marginRight: 16
    },
    lenght_text_style: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
        color: '#fff'
    },
    payment_btn: {
        backgroundColor: '#fff',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    payment_btn_text: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        color: '#000',
        marginHorizontal: 24,
        marginVertical: 15
    },
    saved_card_container: {
        flexDirection: 'row',
        borderRadius: 10,
        backgroundColor: '#333333',
        marginVertical: 4
    },
    round_selected_btn: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    bank_name_num_container: {
        flex: 2,
    },
    cvv_btn_view: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    account_num_text_style_star: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
        color: '#ddd',
        marginBottom: 9
    },
    bank_name_text_style_cvv_container: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        color: '#ddd',
        marginTop: 8,
        marginBottom: 5
    },
    cvv_btn_style: {
        borderRadius: 10,
        borderColor: '#fff',
        borderWidth: 1,
        marginVertical: 7
    },
    CVV_text_style: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 14,
        color: '#ddd',
        marginVertical: 11,
        marginHorizontal: 30
    },
    use_diff_card_view: {
        alignItems: 'flex-end',
        marginTop: 12,
    },
    border_view_style: {
        borderColor: '#101113',
        borderWidth: 1,
        marginLeft: -150,
        width: '150%',
        marginBottom: 15
    },
    pay_using_paypal_container: {
        flex: 4
    },
    pay_using_pay_pal_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 16,
        color: '#ddd',
        marginVertical: 13
    },
    round_radio_style: {
        width: 24,
        height: 24,
        borderRadius: 24 / 2,
        borderWidth: 1,
        borderColor: '#ddd',
        justifyContent: 'center',
        alignItems: 'center'
    },
    selected_btn_style: {
        width: 14,
        height: 14,
        borderRadius: 24 / 2,
        justifyContent: 'center',
        alignItems: 'center'
    }
})


import { StyleSheet, Dimensions } from 'react-native';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import { Fonts } from '../../common/fonts';
const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.7);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 3 / 4);
export const styles = StyleSheet.create({
    scrlView_container: {
        flexGrow: 1,
        paddingBottom:100
    },
    main_container: {
        flex: 1,
        backgroundColor: '#101012'
    },
    ticket_text_n_close_btn_view:{
        flexDirection:'row',
        marginTop:21,
        marginBottom:20
    },
    blank_view:{
        flex:1,
    },
    ticket_title_view:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    close_btn_view:{
        flex:1,
        justifyContent:'center',
        alignItems:'flex-end'
    },
    tickets_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        color: '#ffffff',
    },
    card_main_view_container:{
        flex:1,
        backgroundColor: 'transparent',
    },
    compnay_title_view_container:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
    },
    TGL_Presents_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        color: '#474749',
    },
    line_view_style:{
        borderBottomWidth:1,
        borderColor:'#474749',
        flex:1
    },
    tour_name_title_style:{
        fontFamily: Fonts.PlayfairDisplay_Regular,
        fontSize: 16,
        color: '#dddddd',
        marginTop:5
    },
    _regular_n_price_container:{
        flexDirection:'row',
        marginTop:27,
    },
    regular_n_live_concert_text_style_view:{
        flex:1,
        alignItems:'flex-start'
    },
    event_ticket_type_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 30,
        color: '#dddddd',
    },
    live_concert_pass_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 14,
        color: '#dddddd',
    },
    live_concert_view:{
        marginVertical:3,
        justifyContent:'center',
        alignItems:'center'
    },
    price_view:{
        flex:1,
        alignItems:'flex-end',
        // marginTop:10
    },
    doller_sign_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 24,
        color: '#7c7d7d',
        marginTop:10
    },
    amount_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 56,
        color: '#ddd',
        marginTop:-20
    },
    per_ticket_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#7c7d7d',
        marginTop:-10,
        marginRight:6
    },
    time_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        color: '#ddd',
        marginBottom:10
    },
    select_quality_container:{
        justifyContent:'center',
        alignItems:'center',
        borderRadius:10,
        // flex:1
    },
    select_Quantity_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        color: '#ddd',
        marginTop:15
    },
    select_btn_container:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        marginHorizontal:86,
        marginTop:14,
        backgroundColor:'#212223',
        borderRadius:18
    },
    close_btn_round_view_style:{
        width:36,
        height:36,
        borderRadius:36/2,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#333435',
        // marginRight:15
    },
    selected_amount_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 26,
        color: '#ddd',
        marginHorizontal:30
    },
    head_view_container:{
        // margin:15,
        marginBottom:0,
        padding:15,
        // flex:1,
        backgroundColor: '#1c1c1f',
        borderRadius:10
    },
    body_view_container:{
        justifyContent:'space-evenly',
        alignItems:'center',
        backgroundColor: '#1c1c1f',
        borderRadius:10,
        // margin:15,
        marginTop:0,
        padding:5,
    },
    few_left_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        color: '#ddd',
        marginTop:16,
        marginBottom:15
    },
    sold_out_view:{
        // backgroundColor: 'red',
        marginVertical:12
    },
    total_ticket_n_amount_view:{
        flexDirection:"row",
        justifyContent:'center',
        alignItems:'center',
        marginRight:20
    },
    total_price_view:{
        justifyContent:'center',
        alignItems:"flex-end",
        borderColor:'#fff',
        flex:1,
        marginRight:30
    },
    total_amount_view:{
        justifyContent:'center',
        alignItems:"flex-start",
        borderColor:'#fff',
        flex:1,
        marginLeft:30
    },
    total_Tickets_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#5d5e5f',
        marginTop:28
    },
    total_amout_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 36,
        color: '#fff',
    },
    doller_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 26,
        color: '#7e7f80',
        marginTop:8
    },
    checkout_view:{
        justifyContent:'center',
        alignItems:'center',
        marginTop:34
    },
    checkout_btn_style:{
        backgroundColor:'#fff',
        borderRadius:10
    },
    checkout_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 16,
        color: '#1b1c20',
        marginHorizontal:47,
        marginVertical:17
    },
    FAQ_view:{
        width:'90%',
        alignItems:'flex-start',
        justifyContent:'flex-start',
        marginTop:35
    },
    FAQ_title_view:{
        alignItems:'flex-start',
        justifyContent:'flex-start'
    },
    FAQ_title_text:{
        fontFamily: Fonts.PlayfairDisplay_Regular,
        fontSize: 20,
        color: '#dddddd',
        alignSelf:'flex-start'
    },
    FAQ_question_view:{
        marginTop:21
    },
    que_1_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 14,
        color: '#969797',
    },
    ans_1_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 14,
        color: '#636465',
        marginTop:5
    }
})
import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, TouchableNativeFeedback } from 'react-native';
import { styles } from '../screen/EventDetailScreen/styles';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
export class RippleView extends Component {
    render() {
        return (
            <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('#ddd', true)} onPress={this.props.onPress}>
                <View style={this.props.style}>
                    {this.props.children}
                </View>
            </TouchableNativeFeedback>
        )
    }
}


import React, { Component } from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { PopupComponent } from '../components'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { Fonts } from '../common/fonts'
export class ChatRoomSelectionView extends Component {

    constructor(props) {
        super(props)
        this.state = {
            selectedRoom: 'Global Chat',
            roomList: this.props.roomList,
            threeDotsOption_admin: [
                { name: 'Room Info' },
                { name: 'Delete Room' }
            ],
            threeDotsOption: [
                { name: 'Leave Room' },
            ],
        }
    }

    handelValue = (item) => {
        this.setState({ selectedRoom: item.name })
        console.log("Global : "+JSON.stringify(item))
        console.log("Global : "+JSON.stringify(this.state.roomList))
        global.selectedRoom = item.name
        global.selectedRoomDetail = [{
            "name":item.name,
            "isAdmin":item.isAdmin,
            "roomId":item.roomId
        }]
        this.props.changeRoom(item.name)
        global.isAdmin = item.isAdmin
        console.log("Global : "+JSON.stringify(global.selectedRoomDetail))
    }

    selectThreeDotOption = (item) => {
        if (item.name === 'Room Info') {
            global.myNavigation.navigate('RoomInfoScreen')
        }
    }

    render() {
        const { isAdmin, threeDotsOption_admin, threeDotsOption } = this.state
        return (
            <View style={styles.chatDetailsContainerLeftView}>
                <View style={{ flex: 1 }}>
                    <PopupComponent
                        handelValue={(item) => this.handelValue(item)}
                        menuList={this.state.roomList}
                        SelectedValue={global.selectedRoom}
                        roomSelection={true}
                    />
                </View>
                {
                    global.selectedRoom === 'Global Chat' ?
                        <View style={styles.chatRoomDetailRightIcons}>
                            <TouchableOpacity style={styles.addIconStyle} onPress={this.props.addRoom}>
                                <Entypo name="plus" color="#ddd" size={20} />
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.RIconStyle}>
                                <Text style={styles.RIconTextStyle}>R</Text>
                            </TouchableOpacity>
                        </View>
                        :
                        <PopupComponent
                            handelValue={(item) => this.selectThreeDotOption(item)}
                            menuList={global.isAdmin === 1 ? threeDotsOption_admin : threeDotsOption}
                            threeDotsMenu={true}
                        />
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    chatDetailsContainerLeftView: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    chatRoomDetailRightIcons: {
        flexDirection: 'row'
    },
    addIconStyle: {
        height: 24,
        width: 24,
        borderRadius: 24,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#212123'
    },
    RIconStyle: {
        height: 24,
        width: 24,
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#191919',
        marginLeft: 20
    },
    RIconTextStyle: {
        color: '#ddd',
        fontSize: 15,
        fontFamily: Fonts.OpenSans_semibold
    }
})


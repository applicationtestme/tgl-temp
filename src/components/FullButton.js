import React, { Component } from 'react';
import { SafeAreaView, StyleSheet, Text, ScrollView, View, Image, TouchableOpacity, ImageBackground } from 'react-native';
import { Fonts } from '../common/fonts';
export class FullButton extends Component {
    render() {
        return (
            <TouchableOpacity style={[styles.buttonStyle, this.props.buttonStyle]} onPress={this.props.onPress}>
                <Text style={styles.buttonTextStyle}>{this.props.buttonText}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    buttonStyle: {
        flexDirection: 'row',
        backgroundColor: '#afafaf',
        alignItems: 'center',
        paddingVertical: 12,
        width: '80%',
        justifyContent: 'center',
        borderRadius: 10,
        alignSelf: 'center',
        marginVertical: 10
    },
    buttonTextStyle: {
        color: '#ddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 16
    }
})


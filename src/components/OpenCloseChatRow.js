import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Fonts } from '../common/fonts';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
export class OpenCloseChatRow extends Component {
    doActionForChat = () => {

    }

    render() {
        const { oepnClose } = this.props
        return (
            oepnClose === 'close' ?
                <TouchableOpacity onPress={this.props.onCloseChat} style={styles.closeChatView}>
                    <MaterialIcons name="close" size={18} color={'#ddd'} />
                    <Text style={styles.closeChatTextStyle}>Close Chat</Text>
                </TouchableOpacity>
                :

                <TouchableOpacity onPress={this.props.onCloseChat} style={styles.closeChatView}>
                    <Ionicons name="chatbox" size={18} color="#dddddd" />
                    <Text style={styles.closeChatTextStyle}>Show Chat</Text>
                </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    closeChatView: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 15,
        backgroundColor: '#0c0d0f',
    },
    closeChatTextStyle: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
        color: '#ddd',
        marginLeft: 8
    }
})


import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, StyleSheet, ImageBackground } from 'react-native';
import { Fonts } from '../common/fonts';
import { Images } from '../common/Images';
import LinearGradient from 'react-native-linear-gradient';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
export class GenreOptionBtn extends Component {

    render() {
        return (
            this.props.genreOption.map((item, index) => {
                return (
                    <View style={{ flexDirection: 'row', }}>
                        <TouchableOpacity style={[styles.genre_option_btn_style,{backgroundColor: item.isSelectoption === 1 ? '#fff' : '#252628'}]}>
                            <Text style={[styles.genre_option_btn_text_style,{color:item.isSelectoption === 1 ? '#000' : '#fff'}]}>{item.optionName}</Text>
                        </TouchableOpacity>
                        
                    </View>
                )
            })
        )
    }
}

const styles = StyleSheet.create({
    genre_option_btn_style: {
        borderRadius: 16,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight:16,
        width:103
    },
    genre_option_btn_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 18,
        color: '#1b1c20',
        // marginHorizontal: 43,
        marginVertical: 6
    }
})



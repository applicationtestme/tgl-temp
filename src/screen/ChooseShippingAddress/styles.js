import { StyleSheet, Dimensions } from 'react-native';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import { Fonts } from '../../common/fonts';
export const styles = StyleSheet.create({
    ScrollView: {
        // flex: 1,
        width: '100%',
        backgroundColor: '#1c1c1f'
    },
    main_container: {
        // flex: 1,
        marginHorizontal:16,
        marginVertical:16,
        backgroundColor: '#1c1c1f'
    },
    title_section_view: {

    },
    title_n_close_view: {
        flexDirection: 'row',
        justifyContent:'space-between'
    },
    title_view: {

    },
    title_text_title_view: {
        fontSize: 18,
        color: '#ddd',
        fontFamily: Fonts.OpenSans_semibold,
    },
    close_btn_view:{
        justifyContent:'center',
        alignItems:'center',
    },
    decription_text_view:{
        marginTop:16,
        borderBottomWidth:1,
        borderColor:'#343536'
    },
    decription_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#6f6f70',
        marginBottom:20
    },
    card_view_container:{
        backgroundColor:'#262729',
        borderRadius:10,
        marginVertical:17,
        width:'90%'
    },
    inside_card_view_container:{
        marginHorizontal:24,
        marginVertical:16
    },
    name_n_radio_btn:{
        flexDirection:'row',
        justifyContent:'space-between'
    },
    name_n_details_view:{

    },
    address_type_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        color: '#ddd',
    },
    user_card_name_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
        color: '#ddd',
        marginTop:8
    },
    round_radio_style: {
        width: 24,
        height: 24,
        borderRadius: 24 / 2,
        borderWidth: 1,
        borderColor: '#ddd',
        justifyContent: 'center',
        alignItems: 'center'
    },
    selected_btn_style: {
        width: 14,
        height: 14,
        borderRadius: 24 / 2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#ddd'
    },
    selectedCardStyle: {
        fontSize:14,
        color:'#ddd',
        marginVertical:5
    },
    radio_btn_view:{
        
    },
    address_view:{
        marginTop:11
    },
    addresss_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
        color: '#67686a',
    },
    country_view:{
        marginTop:30
    },
    country_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
        color: '#67686a',
    },
    edit_n_delete_view:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop:11
    },
    edit_text:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        color: '#67686a',
    },
    addNewCardButtonView: {
        borderColor: 'rgba(221, 221, 221, 0.5)',
        borderStyle: 'dashed',
        borderWidth: 2,
        borderRadius: 10,
        paddingVertical: 20,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20
    },
    addNewCardPlusButtonView: {
        height: 24,
        width: 24,
        borderRadius: 24,
        backgroundColor: '#2e2f31',
        justifyContent: 'center',
        alignItems: 'center'
    },
    addNewCardButtonText: {
        color: '#ddd',
        fontSize: 12,
        fontFamily: Fonts.OpenSans_regular,
        marginTop: 10
    },
    delivery_btn_style:{
        borderRadius:10,
        backgroundColor:'#fff',
        justifyContent:'center',
        alignItems:'center',
        marginHorizontal:53,
        marginTop:48
    },
    btn_text_style:{
        marginVertical:17,
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        color: '#1b1c20',
    },
    Disclaimer_text_view:{
        marginTop:40,
        justifyContent:'flex-start'
    },
    Disclaimer_title_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        color: '#777778',
    },
    Disclaimer_details_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        color: '#777778',
        marginTop:10
    },
    right_sign_view:{
        width:24,
        height:24,
        borderRadius:24/2,
        backgroundColor:'#fff',
        justifyContent:'center',
        alignItems:'center'
    }
})

import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Fonts } from '../common/fonts';
import { Images } from '../common/Images';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
export class ChatRow extends Component {
    doActionForChat = () => {

    }

    render() {
        const { item } = this.props
        return (
            item.sendTip === true ?
                <View style={styles.messageRowTip}>
                    <View style={styles.tipAmountView}>
                        <TouchableOpacity style={styles.tipAmountButton}>
                            <Text style={styles.tipAmountText}>$ {item.tipAmount}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.tipmessageDetailView}>
                        <View style={styles.tipUserNameNtime}>
                            <View>
                                <Text style={styles.commenterNameStyle}>{item.tipName} </Text>
                            </View>
                            <View>
                                <Text style={styles.commentTextStyle}>Just Now</Text>
                            </View>
                        </View>
                        <View style={[styles.tipUserNameNtime, { marginTop: 0 }]}>
                            <View>
                                <Text style={styles.tipMsgNameStyle}>{item.tipMessage}</Text>
                            </View>
                        </View>
                    </View>
                </View>
                :
                item.isUserCommnet === false ?
                    <View style={styles.messageRow}>
                        <Image source={Images.dpIcon} style={styles.dpStyle} />
                        <View style={styles.messageDetailView}>
                            <Text style={styles.commenterNameStyle}>{item.commenter}</Text>
                            <Text style={styles.commentTextStyle}>{item.comment}</Text>
                            <Text style={styles.commentTimeStyle}>{item.commentTime}</Text>
                        </View>
                    </View>
                    :
                    <View style={styles.messageRowLR}>
                        <Image source={Images.dpIcon} style={styles.dpStyle} />
                        <View style={styles.messageDetailViewLR}>
                            <Text style={styles.commenterNameStyle}>{item.commenter}</Text>
                            <Text style={styles.commentTextStyle}>{item.comment}</Text>
                            <Text style={styles.commentTimeStyle}>{item.commentTime}</Text>
                        </View>
                    </View>

        )
    }
}

const styles = StyleSheet.create({
    messageRow: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        paddingVertical: 10,
        paddingHorizontal: 16
    },
    messageRowLR: {
        flexDirection: 'row-reverse',
        alignItems: 'flex-start',
        paddingVertical: 10,
        paddingHorizontal: 16,
        marginRight: 20
    },
    messageRowTip: {
        flexDirection: 'row',
        backgroundColor: '#dddddd',
        borderRadius: 10,
        marginHorizontal: 20,
        marginVertical: 5
    },
    dpStyle: {
        height: 26,
        width: 26,
        borderRadius: 26
    },
    tipAmountView: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    messageDetailView: {
        marginHorizontal: 12,
    },
    messageDetailViewLR: {
        marginHorizontal: 12,
        alignItems: 'flex-end'
    },
    commenterNameStyle: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        color: '#b2b2b2'
    },
    commentTextStyle: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
        color: '#b2b2b2',
        marginTop: 2
    },
    commentTimeStyle: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#ddd',
        marginTop: 2
    },
    tipAmountButton: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 12,
        backgroundColor: '#b1b1b1',
        marginVertical: 8,
        marginLeft: 16
    },
    tipAmountText: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        color: '#fff',
        marginVertical: 11,
        marginHorizontal: 14
    },
    tipmessageDetailView: {
        marginLeft: 10
    },
    tipUserNameNtime: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '69%',
        marginTop: 11
    },
    tipMsgNameStyle: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
        color: '#1b1c20',
    }
})


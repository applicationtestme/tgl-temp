import React, { Component } from 'react';
import { InteractionManager, Dimensions, Text, ScrollView, View, Image, TouchableOpacity } from 'react-native';
import { styles } from './styles';
import {
    Header,
    UserProfilePicture,
    PendingRequestRow,
    ChatRow,
    UserChatActionButtons,
    AddCommentBox,
    OpenCloseChatRow,
    PopupComponent,
    GettingReady,
    ChatRoomSelectionView,
    TipPayment
} from '../../components'
import { Images } from '../../common/Images'
import RBSheet from "react-native-raw-bottom-sheet";
import { BlurView, VibrancyView } from "@react-native-community/blur";
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

const tipAmount = [
    { amount: 1 },
    { amount: 2 },
    { amount: 4 },
    { amount: 5 },
    { amount: 10 },
    { amount: 15 }
]

export default class ChatRoom extends Component {

    constructor(props) {
        super(props)
        this.state = {
            selectedRoom: 'Global Chat',
            userName: "fam3456",
            isComment: false,
            sendTip: false,
            tipSteps: 1,
            userComment: "",
            tipAmount: [
                { amount: 1 },
                { amount: 2 },
                { amount: 4 },
                { amount: 5 },
                { amount: 10 },
                { amount: 15 }
            ],
            supporterList: [
                { name: '', hasUpdate: false }, { name: '', hasUpdate: false }, { name: '', hasUpdate: true },
                { name: '', hasUpdate: false }, { name: '', hasUpdate: true }, { name: '', hasUpdate: true },
                { name: '', hasUpdate: false }, { name: '', hasUpdate: true }, { name: '', hasUpdate: false },
            ],
            pendingRequests: [
                {
                    groupName: 'Beyonce Club',
                    invitedBy: 'Ashutosh',
                },
                {
                    groupName: 'Beyoncehh group',
                    invitedBy: 'Dhaval',
                },
            ],
            chatData: [
                {
                    commenter: 'Keith',
                    comment: 'She is a great example of humanity, perfect in every way. ❤️🌹❤️ Beyonce.. Thank you.',
                    commentTime: '25/4/20 3:08',
                    isUserCommnet: false,
                    sendTip: false,
                },
                {
                    commenter: 'Georgey',
                    comment: "I just missed her @beyoncé 🌹🌹🌹 , came here to watched one of my favourite song ever!Omg, watched this again, I'm still in tears, watched how everyone were happy to see",
                    commentTime: '2 min ago',
                    isUserCommnet: false,
                    sendTip: false,
                },
                {
                    commenter: 'Keith',
                    comment: 'She is a great example of humanity, perfect in every way. ❤️🌹❤️ Beyonce.. Thank you.',
                    commentTime: '25/4/20 3:08',
                    isUserCommnet: false,
                    sendTip: false,
                },
                {
                    commenter: 'Georgey',
                    comment: "I just missed her @beyoncé 🌹🌹🌹 , came here to watched one of my favourite song ever!Omg, watched this again, I'm still in tears, watched how everyone were happy to see",
                    commentTime: '2 min ago',
                    isUserCommnet: false,
                    sendTip: false,
                }
            ],
            selectedRoom: 'Global Chat',
            roomList: [
                { 'name': 'Global Chat', 'isAdmin': 0, 'hasNotification': 0 },
                { 'name': 'FireFiles', 'isAdmin': 1, 'hasNotification': 0 },
                { 'name': 'Beyonce Firefiles', 'isAdmin': 0, 'hasNotification': 1 },
                { 'name': 'Sparkles', 'isAdmin': 0, 'hasNotification': 0 }
            ],
            isReady: false
        }
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            this.setState({
                isReady: true
            })
        });
    }

    handelValue = (value) => {
        this.setState({ selectedRoom: value })
    }

    onCloseChat = () => {
        this.props.navigation.goBack()
    }

    addRoom = () => {
        this.props.navigation.navigate('AddRoomScreen')
    }

    seeAllParticipants = () => {
        this.props.navigation.navigate('ParticipantsScreen')
    }

    aproveRequest = (index) => {

    }

    rejectRequest = (index) => {
        let pendingRequests = this.state.pendingRequests;
        pendingRequests.splice(index, 1)
        this.setState({ pendingRequests })
    }

    changeRoom = (selectedRoom) => {
        this.setState({ selectedRoom })
    }
    closeTips = () => {
        this.RBSheet_Tips.close();
    }

    AddComment = () => {

        this.setState({ isComment: true }, () => {
            if (this.state.isComment == true) {
                this.state.chatData.push(
                    {
                        commenter: this.state.userName,
                        comment: this.state.userComment,
                        commentTime: '25/4/20 3:08',
                        isComment: true,
                    }
                )
            }
            this.setState({ userComment: '' })
        })
    }
    HandelSendTip = () => {
        this.setState({
            tipSteps: 1
        }, () => {
            this.RBSheet_Tips.open()
        })
    }
    SelectedAmount = (val) => {
        this.setState({ SelectedAmount: val })
    }
    stepHandel = (amount, message) => {
        if (this.state.tipSteps == 1) {
            this.setState({ tipSteps: 2 })
        } else if (this.state.tipSteps == 2) {
            this.closeTips()
            this.AddTip(amount, message)
        }
    }
    AddTip = (amount, message) => {
        this.setState({ sendTip: true }, () => {
            if (this.state.sendTip == true) {
                this.state.chatData.push(
                    {
                        tipName: this.state.userName,
                        tipAmount: amount,
                        tipMessage: message,
                        commentTime: '25/4/20 3:08',
                        sendTip: true
                    }
                )
                this.setState({ tipAmount: 0 })
            }
        })
        this.setState({ isComment: false })
    }

    render() {
        const { supporterList, pendingRequests, chatData, selectedRoom, tipsAmount, roomList } = this.state
        if (!this.state.isReady) {
            return (
                <GettingReady />
            )
        } else {
            return (
                <View style={styles.main_container}>
                    <Header />
                    <ScrollView contentContainerStyle={styles.scrlView_container}>
                        <View style={styles.chatDetailsContainer}>
                            <ChatRoomSelectionView
                                roomList={roomList}
                                changeRoom={this.changeRoom} addRoom={this.addRoom} />
                            <TouchableOpacity onPress={this.seeAllParticipants} style={styles.participarts_view_container}>
                                <View style={styles.participarts_profile_pic}>
                                    <TouchableOpacity style={styles.user_budge_pic_small_style}></TouchableOpacity>
                                    <TouchableOpacity style={[styles.user_budge_pic_small_style, { backgroundColor: '#696969', right: 5 }]}></TouchableOpacity>
                                    <TouchableOpacity style={[styles.user_budge_pic_small_style, { backgroundColor: '#9d9d9d', right: 10 }]}></TouchableOpacity>
                                </View>
                                <Text style={styles.participants_count_text_style}>42 Participants</Text>
                            </TouchableOpacity>
                        </View>
                        {
                            global.selectedRoom === 'Global Chat' &&
                            <View style={styles.vipSupporterMainView}>
                                <View style={styles.supporterListHeader}>
                                    <Image source={Images.vipSuporterIcon} style={styles.supporterIconStyle} />
                                    <Text style={styles.supporterListHeaderTitle}>VIP Supporters:</Text>
                                </View>
                                <View style={styles.supporterListContainer}>
                                    <Image source={Images.leftSliderIcon} />
                                    <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
                                        {
                                            supporterList.map((item, index) => {
                                                return (
                                                    <View>
                                                        <UserProfilePicture key={'user' + index} />
                                                        {
                                                            item.hasUpdate &&
                                                            <Image source={Images.vipMarkIcon} style={styles.vipMarkStyle} />
                                                        }
                                                    </View>
                                                )
                                            })
                                        }
                                    </ScrollView>
                                    <Image source={Images.rightSliderIcon} />
                                </View>
                            </View>
                        }
                        {
                            pendingRequests.length !== 0 &&
                            pendingRequests.map((item, index) => {
                                return (
                                    <PendingRequestRow
                                        item={item} key={'chat' + index}
                                        aproveRequest={() => this.aproveRequest(index)}
                                        rejectRequest={() => this.rejectRequest(index)}
                                    />
                                )
                            })
                        }
                        <View style={styles.messagesMainContainer}>
                            {
                                chatData.map((item, index) => {
                                    return (
                                        <ChatRow item={item} key={'chat' + index} />
                                    )
                                })
                            }
                        </View>
                    </ScrollView>
                    <UserChatActionButtons
                        SendTip={() => this.HandelSendTip()}
                    />
                    <AddCommentBox
                        addComment={() => this.AddComment()}
                        commentText={(val) => this.setState({ userComment: val })}
                    />
                    <OpenCloseChatRow oepnClose={'close'} onCloseChat={this.onCloseChat} />
                    <RBSheet
                        ref={ref => {
                            this.RBSheet_Tips = ref;
                        }}
                        closeOnDragDown={false}
                        closeOnPressMask={false}
                        height={height / 1}
                        openDuration={300}
                        animationType={'slide'}
                        keyboardAvoidingViewEnabled={true}
                        customStyles={{
                            container: {
                                backgroundColor: '#69696a52',
                                borderTopLeftRadius: 10,
                                borderTopRightRadius: 10,
                                marginTop: 50,
                            },
                            wrapper: {
                                backgroundColor: "transparent"
                            },
                            draggableIcon: {
                                backgroundColor: "transparent"
                            }
                        }}
                    >
                        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                            <View>
                                <BlurView
                                    style={styles.absolute}
                                    blurType="light"
                                    blurAmount={10}
                                    reducedTransparencyFallbackColor="white"
                                />
                                <TipPayment
                                    closeTips={() => this.closeTips()}
                                    step={this.state.tipSteps}
                                    SelectedAmount={(val) => { this.SelectedAmount(val) }}
                                    stepHandel={(val, message) => this.stepHandel(val, message)}
                                    tipMessage={(val) => this.setState({ tipMessage: val })}
                                />
                            </View>
                        </ScrollView>
                    </RBSheet>
                </View>

            );
        }
    }
}


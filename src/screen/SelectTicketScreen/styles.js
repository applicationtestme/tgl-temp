import { StyleSheet, Dimensions } from 'react-native';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import { Fonts } from '../../common/fonts';
const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.7);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 3 / 4);
export const styles = StyleSheet.create({
    scrlView_container: {
        flexGrow: 1,
        paddingBottom:69
    },
    main_container: {
        flex: 1,
        backgroundColor: '#101012'
    },
    ticket_text_n_close_btn_view:{
        flexDirection:'row',
        marginTop:21,
        marginBottom:20
    },
    blank_view:{
        flex:1,
    },
    ticket_title_view:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    close_btn_view:{
        flex:1,
        justifyContent:'center',
        alignItems:'flex-end'
    },
    tickets_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        color: '#ffffff',
    },

    
    close_btn_round_view_style:{
        width:36,
        height:36,
        borderRadius:36/2,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#333435',
        // marginRight:15
    },
    selected_amount_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 26,
        color: '#ddd',
        marginHorizontal:30
    },
    main_container_view:{
        flex:1,
        backgroundColor:'#101012',
        paddingHorizontal:13,
        marginTop:28
    },
    title_view:{
        flexDirection:'row'
    },
    select_tickets_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        color: '#ffffff',
        marginLeft:10,
        marginBottom:18
    },
    folding_ticket_one_view:{
        backgroundColor:'#252628',
        // height:100,
        width:'100%',
        borderLeftWidth:1,
        borderRightWidth:1,
        borderBottomWidth:1,
        borderColor:'#4a4a4b',
        borderStyle: 'dotted',
        borderRadius:4,    
        flexDirection:'row'
    },
    Left_side_view_one_side:{
        marginVertical:16,
    },
    vip_n_ticket_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 16,
        color: '#ffffff',
        marginLeft:5
    },
    live_concert_pass_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#848585',
    },
    event_ticket_type_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 30,
        color: '#848585',
    },
    Right_side_view_one_side:{
        marginVertical:16,
        flex:1
    },
    dlr_btn_round_style:{
        justifyContent:'center',
        alignItems:'center',
        width:19,
        height:19,
        borderRadius:19/2,
        backgroundColor:'#101012'
    },
    tickets_count_text:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 16,
        color: '#848585',
    },
    tickets_No_text:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#848585',
    },
    folding_ticket_second_view:{
        backgroundColor:'#252628',
        width:'100%',
        borderLeftWidth:1,
        borderRightWidth:1,
        borderBottomWidth:1,
        borderColor:'#4a4a4b',
        borderStyle: 'dotted',
        borderRadius:4,    
        flexDirection:'row'
    },
    Left_side_view_second_side:{
        flex:1,
        justifyContent:'center',
        alignItems:'flex-start'
    },
    Right_side_view_second_side:{
        flex:1,
        justifyContent:'center',
        alignItems:'flex-start'
    },
    select_tshirt_text:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#848585',
        marginTop:44
    },
    size_btn_view:{
        marginBottom:35,
        marginTop:13,
        flexDirection:'row'
    },
    select_size_btn:{
        width:22,
        height:20,
        borderRadius:20/2,
        borderWidth:1,
        borderColor: '#848585',
        justifyContent:'center',
        alignItems:'center',
        marginRight:8
    },
    select_size_btn_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#848585',
    },

    /////
    Regular_ticket_one_view:{
        backgroundColor:'#252628',
        width:'100%',
        borderLeftWidth:1,
        borderRightWidth:1,
        borderBottomWidth:1,
        borderColor:'#4a4a4b',
        borderStyle: 'dotted',
        borderRadius:4,    
        flexDirection:'row',
        marginTop:11
    },
    regular_left_side_view:{
        flex:2
    },
    inside_left_side_view:{
        marginVertical:14,
        marginLeft:18,
    },
    title_left_side:{
        marginTop:8
    },
    Regular_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 16,
        color: '#ddd',
    },
    regular_point_view_left_side:{
        marginTop:8,
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center'
    },
    regular_right_side_view:{
        flex:1
    },
    inside_right_side_view:{
        marginVertical:14,
        marginRight:25,
        flex:1
    },
    regular_right_top_view:{
        flex:1,
        justifyContent:'flex-end',
        alignItems:'flex-end'
    },
    regular_right_bottom_view:{
        flex:1,
        justifyContent:'flex-end',
        alignItems:'flex-end',
    },
    checkout_view:{
        justifyContent:'center',
        alignItems:'center',
        marginTop:34
    },
    checkout_btn_style:{
        backgroundColor:'#fff',
        borderRadius:10
    },
    checkout_text_style:{
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 16,
        color: '#1b1c20',
        marginHorizontal:47,
        marginVertical:17
    },
    tshirt_img:{
        height:90,
        width:90,
        resizeMode:'contain',
        marginLeft:16
    }
})
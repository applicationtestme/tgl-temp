import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, ImageBackground, KeyboardAvoidingView, TextInput, } from 'react-native';
import { styles } from './styles';
import { Header, UserChatActionButtons, AddCommentBox, OpenCloseChatRow, ChatRoomSelectionView } from '../../components'
import LinearGradient from 'react-native-linear-gradient';
import { Images } from '../../common/Images'
import RBSheet from "react-native-raw-bottom-sheet";
import { MerchandiseArtistTab } from "../../components/MerchandiseArtistTab";
import Icon from 'react-native-vector-icons/dist/Ionicons';
import ImagePicker from 'react-native-image-picker';
import { connect } from 'react-redux';
import { GetEventDetails, EventDetailsFollow, EventDetailsBuyTicket, EventDetailsSendComment } from '../../store/EventDetatilStore/actions'
import { retry } from 'redux-saga/effects';



console.disableYellowBox = true;
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

const EventURL = "https://tgl-api.herokuapp.com/api/event/";
const ChatURL = "https://tgl-api.herokuapp.com/api/chat/";
const UserURL = "https://tgl-api.herokuapp.com/api/user/";
const ArtistURL = "https://tgl-api.herokuapp.com/api/artist/";


export class EventDetailScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            login: false,
            isLoading: true,
            bought_ticket: false,
            timer: 120,
            timer_counting: false,
            without_ticket_live_view: false,
            past_event_tab: true,
            video_tab: false,
            gellery_tab: false,
            buy_album_tab: true,
            merchandise: false,
            SelectedValue: 'Global Chat',
            roomList: [
                { 'name': 'Global Chat', 'isAdmin': 0, 'hasNotification': 0, 'roomId': 10 },
                { 'name': 'FireFiles', 'isAdmin': 1, 'hasNotification': 0, 'roomId': 11 },
                { 'name': 'Beyonce Firefiles', 'isAdmin': 0, 'hasNotification': 1, 'roomId': 12 },
                { 'name': 'Sparkles', 'isAdmin': 0, 'hasNotification': 0, 'roomId': 13 }
            ],
            logged_in_user_id: 123,
            commentTextValue: '',
            followArtist: false,
            buyTicket: false,
            IsTicket: 0,
            // ScreenList: [
            //     { 'name': 'EventDetail', 'isAdmin': 0, 'hasNotification': 0 },
            //     { 'name': 'TicketingScreen', 'isAdmin': 0, 'hasNotification': 0 },
            //     { 'name': 'ArtistScreen', 'isAdmin': 0, 'hasNotification': 0 },
            //     { 'name': 'ParticipantsScreen', 'isAdmin': 0, 'hasNotification': 0 }
            // ],
        }
        global.myNavigation = this.props.navigation
        global.selectedRoom = 'Global Chat'
        global.selectedRoomDetail = [
            {
                roomId: '10',
                roomName: 'Global Chat',
                roomName: 'Global Chat',
            }
        ]
        global.isAdmin = false
        this.commentTextValueRef = React.createRef();
    }

    componentDidMount = () => {
        // this.tempAction()
        this.props.GetEventDetails("title", "name")
        // this.props.GetArtistPastEventList()
        setTimeout(() => {
            this.timer_count()
        }, 10000)
    }

    handelValue = (value) => {
        this.setState({ SelectedValue: value })
        global.selectedRoom = value
    }


    timer_count = () => {
        if (this.state.timer_counting == false) {
            this.setState({ timer_counting: true }, () => {
                this.interval = setInterval(
                    () => this.setState((prevState) => ({ timer: prevState.timer - 1 })),
                    1000
                );
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.eventType === 'EVENTDETAIL_ON_LOAD_SUCCESS') {
            console.log(" WILL Recive Props Envent Detail Response : ", nextProps.userData)
            console.log(" WILL Recive Props Envent Detail Response : ", nextProps.userData.eventData[0].IsTicket)
            this.setState({ IsTicket: nextProps.userData.eventData[0].IsTicket })
        }
        if (nextProps.eventType === 'EVENTDETAIL_FOLLOW_ARTIST_SUCCESS') {
            console.log(" WILL Recive Props Follow Artist Response : ", nextProps.followArtist)
            this.setState({ followArtist: nextProps.followArtist.status })
        }
        if (nextProps.eventType === 'EVENTDETAIL_BUY_TICKET_SUCCESS') {
            console.log(" WILL Recive Props Buy Ticket Response : ", nextProps.buyTicket)
            console.log(" WILL Recive Props Get Ticket Response : ", nextProps.getTicket)
            this.setState({ IsTicket: nextProps.getTicket.IsTicket })
        }
        if (nextProps.eventType === 'EVENTDETAIL_SEND_COMMENT_SUCCESS') {
            console.log(" WILL Recive Props Buy Ticket Response : ", nextProps.sendComment)
        }
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }
    check_login = () => {
        if (this.state.login === true) {
            this.props.EventDetailsBuyTicket()
            // alert("you have successfully bought ticket");
            // this.BuyTicket();
            // this.setState({ bought_ticket: true });
            // this.RBSheet.close();
            // this.timer_count()
        } else {
            alert("Please Login");
        }
    }
    handelBuyTicket = () => {
        this.setState({ bought_ticket: true });
        this.RBSheet.close();
        this.timer_count()
    }
    buying_ticket = () => {
        alert("you have successfully bought ticket");
        this.setState({ bought_ticket: true });
        this.RBSheet.close();
    }
    loginInfo = () => {
        alert("you have successfully logged in");
        this.setState({ login: true });
        this.RBSheet.close();
    }

    openSheet = () => {
        this.RBSheet.open();
    }

    closeSheet = () => {
        this.RBSheet.close();
    }

    openChat = () => {
        this.props.navigation.navigate('ChatRoom')
    }

    closeChat = () => {
        this.RBSheet_chat.close();
    }
    past_event_tab_view = () => {
        this.setState({ past_event_tab: true, video_tab: false, gellery_tab: false });
    }
    video_tab_view = () => {
        this.setState({ video_tab: true, past_event_tab: false, gellery_tab: false });
    }
    gellery_tab_view = () => {
        this.setState({ gellery_tab: true, past_event_tab: false, video_tab: false });
    }
    buy_album_tab_view = () => {
        this.setState({ buy_album_tab: true, merchandise: false });
    }
    Merchandise_tab_view = () => {
        this.setState({ merchandise: true, buy_album_tab: false });
    }
    changeRoom = () => {

    }
    addRoom = () => {

    }
    chooseFile = () => {
        var options = {
            title: 'Select Image',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                let source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    filePath: source
                }, () => {
                });
            }
        });
    };



    GetEventDetails = () => {
        const url = EventURL + 'GetEventDetails';
        return fetch(url, {
            method: 'POST',
            headers: {
                "Content-Type": 'application/json'
            }
        })
            .then(res =>
                res.json()
            ).then(json => {
                console.log("GetEventDetails" + JSON.stringify(json));
                this.setState({ getEventDetail: json })
            })
    };
    BuyTicket = () => {
        const url = EventURL + 'BuyTicket';
        return fetch(url, {
            method: 'POST',
            headers: {
                "Content-Type": 'application/json'
            }
        })
            .then(res =>
                res.json()
            ).then(json => {
                console.log("Buy Ticket : " + JSON.stringify(json.success_code));
                console.log("Buy Ticket : " + JSON.stringify(json));

                if (json.status == true) {
                    this.GetTicket()
                }
                // this.setState({ buyTicket: json })
            })
    };
    FollowArtist = () => {
        this.props.EventDetailsFollow()
    };

    tempAction = async () => {
        await this.GetEventDetails()
        // await this.BuyTicket()
        // await this.GetTicket()
        // await this.SendComment()
        await this.CommentList()
        await this.GetEventSchedule()
        // await this.FollowArtist()
        await this.GetArtistPastEventList()
    }
    addComment = () => {
        this.commentTextValueRef.current.clear();
        this.props.EventDetailsSendComment();
    }


    render() {
        return (
            <KeyboardAvoidingView behaviour="height" style={styles.main_container}>
                <Header />
                {
                    this.props.isLoading == false &&
                    this.props.userData !== undefined && this.props.userData !== null &&
                    <ScrollView contentContainerStyle={styles.scrlView_container}>
                        <ImageBackground source={{ uri: this.props.userData.eventData[0].mainImage }} style={styles.img_bg_style}>
                            {/* <ImageBackground source={Images.mainBackground} style={styles.img_bg_style}> */}

                            <View style={{ height: height / 2 - 100, alignItems: 'center', justifyContent: 'flex-end' }}>
                                {this.state.timer_counting === true ?
                                    <View>
                                        {
                                            this.state.timer > 0 &&
                                            <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center' }}>
                                                <Text style={styles.top_event_Begins_in_text_style}>Event Begins in</Text>
                                                <Text style={styles.timing_count_text_style}>{this.state.timer}</Text>
                                                <Text style={styles.seconds_text_style}>Seconds</Text>
                                            </View>
                                        }
                                        {
                                            this.state.bought_ticket &&
                                            <View style={styles.enter_ticket_start_watching_container}>
                                                <View style={styles.enter_ticket_code_view}>
                                                    <TextInput style={styles.enter_ticket_code_textinput} placeholder={'Enter Ticket Code'} placeholderTextColor="#dddddd"></TextInput>
                                                </View>
                                                <TouchableOpacity style={styles.start_watching_btn_style}>
                                                    <Text style={styles.start_watching_text_style}>Start Watching</Text>
                                                </TouchableOpacity>
                                            </View>
                                        }
                                    </View> : null
                                }
                            </View>
                            <Text style={styles.timing_text_style}>Aug 25   4:30 PM (PST)</Text>
                            <Text numberOfLines={3} style={styles.singer_name_text_style}>{this.props.userData.eventData[0].eventName}</Text>

                            <View style={{ flexDirection: 'row' }}>
                                {this.props.userData.eventData[0].eventType.map((item, index) => {
                                    return (
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={styles.category_text_style}> {this.props.userData.eventData[0].eventType[index]} </Text>
                                            {
                                                this.props.userData.eventData[0].eventType.length - 1 !== index &&
                                                <Text style={styles.category_text_style}> • </Text>
                                            }
                                        </View>
                                    )
                                })
                                }
                            </View>
                            {this.state.timer === 0 ?
                                <View style={styles.live_now_btn_container}>
                                    <TouchableOpacity style={styles.live_btn_style}>
                                        <Text style={styles.LIVE_NOW_text_style}>LIVE NOW</Text>
                                    </TouchableOpacity>
                                    <Text style={styles.how_much_watching_count_text_style}>1.9K Watching</Text>
                                </View> : null
                            }
                            {this.state.bought_ticket === false ?
                                <View>
                                    <Text style={styles.event_Begins_in_text_style}>Event Begins in</Text>
                                    <Text style={styles.remaining_time_text_style}>2 Days 4 Hours</Text>
                                </View> :
                                <View >
                                    <Text style={styles.bought_ticket_count_text_style}>You have 4 tickets of this show</Text>
                                </View>
                            }
                            {
                                this.state.IsTicket == 0 &&
                                <TouchableOpacity onPress={() => { this.openSheet() }} style={styles.buy_tickets_btn_style}>
                                    <Text style={styles.buy_ticket_text_style}>Buy Tickets</Text>
                                </TouchableOpacity>
                            }
                            <Text style={styles.price_info_text_style}>Starting from ${this.props.userData.eventData[0].ticketPrice}   •   Only {this.props.userData.eventData[0].remainingTicket} Left</Text>
                            <View style={styles.wishlist_share_invite_container}>
                                <TouchableOpacity style={styles.dotIconView}
                                    onPress={() => this.tempAction()}
                                >
                                    <View style={styles.dotIconStyle}></View>
                                    <Text style={styles.dotTextStyle}>Watchlist</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.dotIconView}>
                                    <View style={styles.dotIconStyle}></View>
                                    <Text style={styles.dotTextStyle}>Share</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.dotIconView}>
                                    <View style={styles.dotIconStyle}></View>
                                    <Text style={styles.dotTextStyle}>Invite</Text>
                                </TouchableOpacity>
                            </View>
                        </ImageBackground>

                        <View style={styles.global_chat_container}>

                            <View style={{ marginTop: 16, marginHorizontal: 16 }}>
                                <ChatRoomSelectionView
                                    changeRoom={this.changeRoom}
                                    addRoom={this.addRoom}
                                    roomList={this.props.userData.eventData[0].listOfGroup}
                                    roomList={this.state.roomList}
                                />
                            </View>

                            <View style={styles.participarts_view_container}>
                                <View style={styles.participarts_profile_pic}>
                                    <TouchableOpacity style={styles.user_budge_pic_small_style}></TouchableOpacity>
                                    <TouchableOpacity style={[styles.user_budge_pic_small_style, { backgroundColor: '#696969', right: 5 }]}></TouchableOpacity>
                                    <TouchableOpacity style={[styles.user_budge_pic_small_style, { backgroundColor: '#9d9d9d', right: 10 }]}></TouchableOpacity>
                                </View>
                                <Text style={styles.participants_count_text_style}>42 Participants</Text>
                            </View>
                        </View>
                        {this.props.commentList !== undefined && this.props.commentList !== null &&
                            <View style={styles.letest_comment_view_style}>
                                <Text style={styles.latest_comment_style}>Latest Comment</Text>
                                <View style={styles.profile_comment_time_view_container}>
                                    <TouchableOpacity onPress={() => this.chooseFile()}>
                                        {/* <Image source={this.state.filePath} style={styles.dpStyle} /> */}
                                        <Image source={this.props.commentList.comments[0].commentUserProfilePic} style={styles.dpStyle} />
                                    </TouchableOpacity>
                                    <Text style={styles.user_comment_text_style}>{this.props.commentList.comments[0].latestComment}</Text>
                                </View>
                                <Text style={styles.comment_time_text_style}>{this.props.commentList.comments[0].latestTime} min ago</Text>
                            </View>}
                        <UserChatActionButtons />
                        <AddCommentBox
                            bought_ticket={this.state.bought_ticket}
                            commentText={(value) => { console.log("value : " + value); this.setState({ commentTextValue: value }) }}
                            commentTextRef={this.commentTextValueRef}
                            addComment={() => { this.addComment() }}
                        />
                        <OpenCloseChatRow oepnClose={'Open'} onCloseChat={() => this.openChat()} />
                        <View style={styles.enent_indicator_container}>
                            <Text style={styles.event_schedule_text_style}>Event Schedule</Text>
                            <View style={styles.indicator_view_container}>
                                {/* {this.props.getEventSchedule.map((itm, index) => {
                                    return ( */}

                                <View style={styles.dot_view_container}>
                                    <View style={styles.dot_view}></View>
                                    <View>
                                        <Text style={styles.single_ladies_text_style}>Single Ladies</Text>
                                        <Text style={styles.beyonce_text_style}>Beyonce</Text>
                                    </View>
                                </View>
                                {/* )
                                })
                                } */}
                                <View style={styles.dot_view_container}>
                                    <View style={styles.dot_view}></View>
                                    <View>
                                        <Text style={styles.single_ladies_text_style}>Single Ladies</Text>
                                        <Text style={styles.beyonce_text_style}>Beyonce</Text>
                                    </View>
                                </View>
                                <View style={styles.dot_view_container}>
                                    <View style={styles.dot_view}></View>
                                    <View>
                                        <Text style={styles.single_ladies_text_style}>Single Ladies</Text>
                                        <Text style={styles.beyonce_text_style}>Beyonce</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <ImageBackground source={{ uri: this.props.userData.eventData[0].subImage }} style={styles.white_img_bg_style}>
                            <LinearGradient colors={['rgba(251, 251, 251, 0)', '#fbfbfb', '#fff']} style={styles.linearGradient}>
                                <Text style={styles.img_title_beyonce_text_style}>Beyonce</Text>
                                <View style={styles.follower_n_rating_container}>
                                    <Text style={styles.white_img_follow_count_text_style}>{this.props.userData.eventData[0].artistFollower} Followers • </Text>
                                    <Text style={styles.white_img_follow_count_text_style}>{this.props.userData.eventData[0].artistRating} Rating</Text>
                                </View>
                                {this.state.followArtist == false &&
                                    <TouchableOpacity style={styles.white_follow_artist_btn_style} onPress={() => {
                                        this.FollowArtist()
                                    }}>
                                        <Text style={styles.follow_the_Artist_text_style}>Follow the Artist</Text>
                                    </TouchableOpacity>
                                }
                                <View style={styles.tab_view_container}>
                                    <TouchableOpacity onPress={() => this.past_event_tab_view()} style={[styles.tab_btn_style, { borderBottomWidth: this.state.past_event_tab === true && this.state.video_tab === false && this.state.gellery_tab === false ? 3 : null }]}>
                                        <Text style={styles.tab_btn_text_style}>Past Events</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.video_tab_view()} style={[styles.tab_btn_style, { borderBottomWidth: this.state.video_tab === true && this.state.past_event_tab === false && this.state.gellery_tab === false ? 3 : null }]}>
                                        <Text style={styles.tab_btn_text_style}>Video</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.gellery_tab_view()} style={[styles.tab_btn_style, { borderBottomWidth: this.state.gellery_tab === true && this.state.past_event_tab === false && this.state.video_tab === false ? 3 : null }]}>
                                        <Text style={styles.tab_btn_text_style}>Gallery</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.tab_containt_view}>
                                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                        {this.props.getArtistPastEventList.map((itm, idx) => {
                                            return (

                                                <View>
                                                    {/* {itm.EventPastId} */}
                                                    <TouchableOpacity style={styles.video_view_style}>
                                                        <ImageBackground style={{}} imageStyle={{ height: 202, width: 114, borderRadius: 10 }} source={itm.Url}>
                                                            <Image style={{}} source={Images.play_btn}></Image>
                                                        </ImageBackground>
                                                    </TouchableOpacity>
                                                    <Text style={styles.past_event_video_title_text}>{itm.Name}</Text>
                                                </View>
                                            )
                                        })
                                        }
                                    </ScrollView>
                                </View>
                                <View style={styles.tab_view_container}>
                                    <TouchableOpacity onPress={() => this.buy_album_tab_view()} style={[styles.tab_btn_style, { borderBottomWidth: this.state.buy_album_tab === true && this.state.merchandise === false ? 3 : null }]}>
                                        <Text style={styles.tab_btn_text_style}>Buy Albums</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.Merchandise_tab_view()} style={[styles.tab_btn_style, { borderBottomWidth: this.state.merchandise === true && this.state.buy_album_tab === false ? 3 : null }]}>
                                        <Text style={styles.tab_btn_text_style}>Merchandise</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.tab_btn_style}>
                                    </TouchableOpacity>
                                </View>
                                {this.state.buy_album_tab == true ?
                                    <View style={styles.tab_containt_view}>
                                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                            {this.props.getUserByAlbum.map((itm, idx) => {
                                                return (
                                                    <View style={styles.album_main_view_container}>
                                                        <TouchableOpacity >
                                                            <ImageBackground source={{ uri: itm.Image }} imageStyle={styles.album_main_view_Image_backgroud} style={styles.album_view_style}>
                                                                <Text style={styles.album_name_text_style_tab_view}>{itm.eventName}</Text>
                                                            </ImageBackground>
                                                        </TouchableOpacity>
                                                        {/* evetBuyId */}
                                                        <TouchableOpacity style={styles.buy_btn_album_view_style}>
                                                            <Text style={styles.BUY_text_style}>BUY</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                )
                                            })
                                            }
                                        </ScrollView>
                                    </View>
                                    :
                                    <MerchandiseArtistTab /> 
                                    }
                            </LinearGradient>
                        </ImageBackground>
                        <View style={styles.event_for_view_container}>
                            <View style={styles.event_for_you_title_view}>
                                <View style={{ flex: 1 }}>
                                    <Text style={styles.events_for_you_text_title}>Events for you</Text>
                                </View>
                                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                    <TouchableOpacity>
                                        <Text style={styles.see_all_text_title}>See All</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {
                                this.props.EventsForYou !== undefined && this.props.EventsForYou !== null &&
                                this.props.EventsForYou.eventForYou.map((item, index) => {
                                    return (


                                        <ImageBackground source={{ uri: item.artistEventPoster }} style={styles.lady_gaga_concert_img_bg_style}>
                                            <LinearGradient colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0,0.35)', '#000']} style={styles.linearGradient2}>
                                                <View style={{ marginLeft: 25, }}>

                                                    <View style={styles.artist_name_n_concert_name_container}>
                                                        <View>
                                                            <Image style={styles.lady_gaga_user_pic_style} source={{ uri: item.artistProfile }}></Image>
                                                            {/* <Text style={{color:'#fff'}}>{item.artistProfile}</Text> */}
                                                        </View>
                                                        <View style={{ alignItems: 'flex-start', marginTop: 10 }}>
                                                            <Text style={styles.artist_name_title_text_event_for_you_style}>{item.artistName}</Text>
                                                            <Text style={styles.concert_title_text_style}>{item.eventName}</Text>
                                                            {/* <Text style={styles.concert_timing_text_style}>Aug 25 4:30 PM (PST)</Text>EventDateandTime */}
                                                            <Text style={styles.concert_timing_text_style}>{item.EventDateandTime}</Text>
                                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '68%', }}>
                                                                <View>
                                                                    <TouchableOpacity style={styles.buy_tickets_concert_artist_btn_style}>
                                                                        <Text style={styles.buy_ticket_concert_artist_text_style}>Buy Tickets</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                                <View >
                                                                    <TouchableOpacity style={{}}>
                                                                        <Image style={{}} source={Images.right_arrow_padding}></Image>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>
                                                        </View>
                                                    </View>
                                                </View>
                                            </LinearGradient>
                                        </ImageBackground>
                                    )
                                })
                            }
                            <View style={styles.event_for_you_title_view}>
                                <View style={{ flex: 1 }}>
                                    <Text style={styles.events_for_you_text_title}>Similar Artists</Text>
                                </View>
                                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                    <TouchableOpacity>
                                        <Text style={[styles.see_all_text_title, { marginRight: 16 }]}>See All</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.similar_artist_suggestion_view}>
                                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                    {
                                        this.props.getSimilerArtistList.map((itm, idx) => {
                                            return (

                                                <TouchableOpacity style={styles.similar_artist_box_view__style}>
                                                    <Image source={{ uri: itm.artistProfileImage }} style={styles.similar_artist_img__style}>
                                                    </Image>
                                                    <Text style={styles.similar_artist_name_text__style}>{itm.artistName}</Text>
                                                </TouchableOpacity>
                                            )
                                        })
                                    }
                                </ScrollView>
                            </View>
                        </View>
                        <RBSheet
                            ref={ref => {
                                this.RBSheet = ref;
                            }}
                            closeOnDragDown={true}
                            closeOnPressMask={true}
                            height={291}
                            openDuration={250}
                            customStyles={{
                                container: {
                                    backgroundColor: '#e7e7e7',
                                    opacity: 0.97,
                                    borderTopLeftRadius: 10,
                                    borderTopRightRadius: 10,
                                },
                                wrapper: {
                                    backgroundColor: "transparent"
                                },
                                draggableIcon: {
                                    backgroundColor: "transparent"
                                }
                            }}
                        >
                            {/* <View style={styles.modal_view_container}> */}
                            <View style={styles.close_view_container}>
                                <TouchableOpacity onPress={() => this.closeSheet()} >
                                    <Icon name='close' size={30} color='#1b1c20'></Icon>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.btn_n_details_container}>
                                <TouchableOpacity style={styles.modal_buy_btn_style} onPress={() => this.check_login()}>
                                    <Text style={styles.buy_ticket_text_style}>Buy Tickets</Text>
                                </TouchableOpacity>
                                <Text style={styles.lefting_ticket_text_style}>Starting from $10     Only 10 Left</Text>
                                <Text style={styles.get_ticket_to_unlock_text_style}>Get the tickets to Unlock the chat.</Text>
                                {this.state.login === false ?
                                    <View style={styles.login_line_modal_style}>
                                        <Text style={styles.already_have_login_text_style}>Already have the tickets?</Text>
                                        <TouchableOpacity onPress={() => this.loginInfo()}>
                                            <Text style={styles.login_text_modal_style}> Login</Text>
                                        </TouchableOpacity>
                                    </View> :
                                    null
                                }
                            </View>
                            {/* </View> */}
                        </RBSheet>
                    </ScrollView>
                }
            </KeyboardAvoidingView>
        );
    }
}
function mapStateToProps(state) {
    return {
        eventType: state.EventDetatilStore.type,
        userData: state.EventDetatilStore.userData,
        commentList: state.EventDetatilStore.commentList,
        EventsForYou: state.EventDetatilStore.EventsForYou,
        getUserByAlbum: state.EventDetatilStore.getUserByAlbum,
        getArtistPastEventList: state.EventDetatilStore.getArtistPastEventList,
        getSimilerArtistList: state.EventDetatilStore.getSimilerArtistList,
        getEventSchedule: state.EventDetatilStore.getEventSchedule,
        followArtist: state.EventDetatilStore.followArtist,
        buyTicket: state.EventDetatilStore.buyTicket,
        getTicket: state.EventDetatilStore.getTicket,
        sendComment: state.EventDetatilStore.sendComment,
        isLoading: state.EventDetatilStore.isLoading,
    };
}
function matchDispatchToProps(dispatch) {
    //return bindActionCreators({ loadTask }, dispatch)
    return {
        // addNewTask: (newTask) => dispatch(addNewTask(newTask)),
        loadTask: (newTask) => dispatch(loadTask(newTask)),
        GetEventDetails: () => dispatch(GetEventDetails()),
        EventDetailsFollow: () => dispatch(EventDetailsFollow()),
        EventDetailsBuyTicket: () => dispatch(EventDetailsBuyTicket()),
        EventDetailsSendComment: () => dispatch(EventDetailsSendComment())
    }
}
export default connect(mapStateToProps, matchDispatchToProps)(EventDetailScreen);
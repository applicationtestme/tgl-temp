import { StyleSheet, Dimensions } from 'react-native';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import { Fonts } from '../../common/fonts';
export const styles = StyleSheet.create({
    scrlView_container: {
        flexGrow: 1
    },
    main_container: {
        flex: 1,
        backgroundColor: '#101012'
    },
    img_bg_style: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    header_container_style: {
        backgroundColor: 'transparent',
        position: 'absolute',
        backgroundColor: 'transparent',
        zIndex: 100,
        top: 0,
        left: 0,
        right: 0,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0,
        marginTop: 10
    },
    header_user_img_style: {
        width: 24,
        height: 24,
        borderRadius: 24 / 2,
        backgroundColor: '#ddd',
        marginLeft: 16,
        marginVertical: 12
    },
    singer_name_text_style: {
        fontFamily: 'PlayfairDisplaySC-Bold',
        fontSize: 24,
        color: '#dddddd',
        marginTop: 8,
        textAlign: 'center',
        width: '70%',
        marginTop: 274
    },
    genres_text_view_style: {
        flexDirection: 'row',
        marginTop: 12
    },
    genres_text_style: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#fff',
    },
    genres_type_text_style: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#ddd',
    },
    following_N_rating_container: {
        flexDirection: 'row',
        marginTop: 17
    },
    follower_view: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    follower_icon_style: {
        width: 24,
        height: 24
    },
    followers_text_style: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#ddd',
        marginLeft: 4
    },
    follow_artist_btn_style: {
        backgroundColor: '#d8d8d8',
        borderRadius: 10,
        marginTop: 8

    },
    follow_artist_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        color: '#1b1c20',
        textAlign: 'center',
        paddingVertical: 11,
        paddingHorizontal: 41
    },
    event_for_view_container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 16
    },
    event_for_you_title_view: {
        flexDirection: 'row'
    },
    upcoming_event_text_title: {
        fontFamily: Fonts.PlayfairDisplay_Bold,
        color: '#dddddd',
        fontSize: 18,
        marginTop: 24,
    },
    see_all_text_title: {
        color: '#dddddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 18,
        marginTop: 31,
    },
    event_for_view_container:{
          backgroundColor:'#101113',
          justifyContent:'center',
          alignItems:'center',
          marginHorizontal:16
      },
      event_for_you_title_view:{
          flexDirection:'row'
      },
      events_for_you_text_title:{
          fontFamily:Fonts.PlayfairDisplay_Bold,
          color:'#dddddd',
          fontSize:18,
          marginTop:24,
    },
    see_all_text_title:{
        color:'#dddddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        marginTop:31,
    },
    lady_gaga_concert_img_bg_style:{
        backgroundColor: 'transparent',
        flex: 1,
        width:'100%',
        height:185,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:16,
        alignSelf:'center',
    },
    artist_name_title_text_event_for_you_style:{
        color: '#dddddd',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        marginTop:-15
    },
    linearGradient2:{
        opacity:1,
        flex: 1,
        width:'100%',
        justifyContent:'center',
        alignItems:'center'
    },
    artist_name_n_concert_name_container:{
        flexDirection:'row',
        alignItems:'flex-start',
        marginTop:75
    },
    lady_gaga_user_pic_style:{
        marginBottom:24,
    },
    concert_title_text_style:{
        fontFamily:Fonts.PlayfairDisplay_Bold,
          color:'#dddddd',
          fontSize:16,
    },
    concert_timing_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        color:'#dddddd',
        fontSize:8,
        marginTop:3,
        marginBottom:8
    },
    buy_tickets_concert_artist_btn_style:{
        backgroundColor:'#d8d8d8',
        borderRadius:5,
    },
    buy_ticket_concert_artist_text_style:{
        color:'#1b1c20',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        marginVertical:8,
        marginHorizontal:30
    },
    similar_artist_suggestion_view:{
        flexDirection:'row',
        marginVertical:20,
        alignSelf:'flex-start'
    },
    similar_artist_img__style:{
        width:118,
        height:170,
        borderRadius:10
    },
    similar_artist_name_text__style:{
        color:'#dddddd',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
    },
    similar_artist_box_view__style:{
        justifyContent:'center',
        alignItems:'center',
        marginRight:10,
        
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
      },
      upcoming_event_for_view_container:{
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 16,
        marginBottom:16
      },
      event_for_you_title_view: {
        flexDirection: 'row'
    },
    events_for_you_text_title: {
        fontFamily: Fonts.PlayfairDisplay_Bold,
        color: '#dddddd',
        fontSize: 18,
        marginTop: 24,
    },
    see_all_text_title: {
        color: '#dddddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        marginTop: 31,
    },
    similar_artist_event_name_text__style:{
        color:'#dddddd',
        fontFamily: Fonts.PlayfairDisplay_Black,
        fontSize: 16,
        marginTop:4
    },
    similar_artist_event_time_name_text__style:{
        color:'#dddddd',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        marginTop:4
    },
    similar_artist_event_buy_ticket_btn__style:{
        borderWidth:1,
        borderColor:'grey',
        borderRadius:10,
        marginTop:11
    },
    similar_artist_event_buy_ticket_text_btn__style:{
        color:'#dddddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        marginVertical:10,
        marginHorizontal:28
    },
    inside_contant:{
        marginTop:50,
        justifyContent:'center',
        alignItems:'center'
    }
})
import { StyleSheet, Dimensions } from 'react-native';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import { Fonts } from '../../common/fonts';
export const styles = StyleSheet.create({
    scrlView_container: {
        flexGrow: 1,
        backgroundColor: '#101113',
        paddingBottom: 50
    },
    main_container: {
        flex: 1,
        backgroundColor: '#101113'
    },
    img_bg_style: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    timing_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        color: '#dddddd',
        alignSelf: 'center',
    },
    singer_name_text_style: {
        fontFamily: 'PlayfairDisplaySC-Bold',
        fontSize: 18,
        color: '#dddddd',
        marginTop: 8,
        textAlign: 'center',
        width: '70%'
    },
    category_text_style: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#dddddd',
        marginTop: 4,
        textAlign: 'center'
    },
    event_Begins_in_text_style: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#dddddd',
        marginTop: 16,
        textAlign: 'center'
    },
    remaining_time_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        color: '#dddddd',
        marginTop: 4,
        textAlign: 'center'
    },
    buy_tickets_btn_style: {
        backgroundColor: '#d8d8d8',
        borderRadius: 10,
        marginTop: 8

    },
    buy_ticket_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        color: '#1b1c20',
        textAlign: 'center',
        paddingVertical: 11,
        paddingHorizontal: 41
    },
    price_info_text_style: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#dddddd',
        marginTop: 8,
        textAlign: 'center'
    },
    wishlist_share_invite_container: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 20
    },
    globle_chat_container: {
        backgroundColor: '#101113'
    },
    global__title_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 16,
        color: '#dddddd',
        marginTop: 16,
        marginLeft: 15
    },
    participarts_view_container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 9,
        marginLeft: 15,
        marginBottom: 18
    },
    participarts_profile_pic: {
        flexDirection: 'row'
    },
    participants_count_text_style: {
        textDecorationLine: 'underline',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        color: '#dddddd',
        right: 3
    },
    user_budge_pic_small_style: {
        width: 10,
        height: 10,
        borderRadius: 10 / 2,
        backgroundColor: '#363636'
    },
    dotIconStyle: {
        height: 50,
        width: 50,
        resizeMode: 'contain'
    },
    letest_comment_view_style: {
        backgroundColor: '#101113',
        borderBottomWidth: 1,
        borderBottomColor: '#27282a',
        paddingHorizontal: 16
    },
    latest_comment_style: {
        color: '#dddddd',
        fontSize: 10,
        fontFamily: Fonts.OpenSans_regular,
    },
    profile_comment_time_view_container: {
        flexDirection: 'row',
        marginTop: 6,
    },
    dpStyle: {
        height: 26,
        width: 26,
        borderRadius: 26
    },
    user_comment_text_style: {
        color: '#b2b2b2',
        fontSize: 12,
        fontFamily: Fonts.OpenSans_regular,
        marginLeft: 12,
    },
    comment_time_text_style: {
        marginLeft: 38,
        color: '#b2b2b2',
        fontSize: 12,
        fontFamily: Fonts.OpenSans_regular,
        marginBottom: 9
    },

    /// dev addition
    top_event_Begins_in_text_style: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#dddddd',
        textAlign: 'center',
        marginTop: 83
    },
    timing_count_text_style: {
        color: '#9e9d9e',
        fontSize: 59,
        fontFamily: Fonts.OpenSans_semibold,
        textAlign: 'center'
    },
    seconds_text_style: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#dddddd',
        textAlign: 'center',
    },
    enter_ticket_start_watching_container: {
        flexDirection: 'row',
        marginTop: 21,
        marginBottom: 16
    },
    enter_ticket_code_view: {
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        height: 40,
        borderTopLeftRadius: 4,
        borderBottomLeftRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    },
    enter_ticket_code_textinput: {
        color: '#dddddd',
        marginHorizontal: 12,
        fontSize: 10
    },
    start_watching_btn_style: {
        backgroundColor: '#d8d8d8',
        borderTopLeftRadius: 4,
        borderBottomLeftRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        height: 40
    },
    start_watching_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        color: '#000000',
        textAlign: 'center',
        marginHorizontal: 18
    },
    bought_ticket_count_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        color: '#dddddd',
        marginTop: 16,
        textAlign: 'center'
    },
    plus_icon_container: {
        flex: 1,
        alignItems: "flex-end",
    },
    plus_icon_img_style: {
        width: 30,
        height: 30,
        resizeMode: 'contain',
        marginTop: 16,
        marginRight: 16
    },
    close_view_container: {
        alignSelf: 'flex-end',
        marginTop: -10,
        marginRight: 16
    },
    btn_n_details_container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 48
    },
    modal_buy_btn_style: {
        backgroundColor: '#ffffff',
        borderRadius: 10,
        // elevation:5,
        shadowColor: "#fff",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,

        elevation: 8,
    },
    lefting_ticket_text_style: {
        color: '#1b1c20',
        fontSize: 12,
        fontFamily: Fonts.OpenSans_regular,
        marginTop: 8
    },
    get_ticket_to_unlock_text_style: {
        color: '#1b1c20',
        fontSize: 14,
        fontFamily: Fonts.OpenSans_regular,
        marginTop: 32
    },
    login_line_modal_style: {
        flexDirection: 'row'
    },
    already_have_login_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        color: '#101113',
        marginTop: 8,
        marginBottom: 62
    },
    login_text_modal_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        color: '#101113',
        marginTop: 8,
        textDecorationLine: "underline"
    },
    global_chat_container: {
        backgroundColor: '#101113'
    },
    global_chat_title_n_plus_icon_container: {
        flexDirection: 'row'
    },
    global_title_container: {
        flex: 1,
        alignItems: "flex-start"
    },
    plus_icon_container: {
        flex: 1,
        alignItems: "flex-end",
    },
    plus_icon_img_style: {
        width: 30,
        height: 30,
        resizeMode: 'contain',
        marginTop: 16,
        marginRight: 16
    },
    top_event_Begins_in_text_style: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#dddddd',
        textAlign: 'center',
        marginTop: 83
    },
    timing_count_text_style: {
        color: '#9e9d9e',
        fontSize: 59,
        fontFamily: Fonts.OpenSans_semibold,
        textAlign: 'center'
    },
    seconds_text_style: {
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        color: '#dddddd',
        textAlign: 'center',
    },
    enter_ticket_start_watching_container: {
        flexDirection: 'row',
        marginTop: 21,
        marginBottom: 16
    },
    enter_ticket_code_view: {
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        height: 40,
        borderTopLeftRadius: 4,
        borderBottomLeftRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    },
    enter_ticket_code_textinput: {
        color: '#dddddd',
        marginHorizontal: 12,
        fontSize: 10
    },
    start_watching_btn_style: {
        backgroundColor: '#d8d8d8',
        borderTopLeftRadius: 4,
        borderBottomLeftRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        height: 40
    },
    start_watching_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        color: '#000000',
        textAlign: 'center',
        marginHorizontal: 18
    },
    dotIconView: {
        height: 50,
        width: 50,
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        marginHorizontal: 5
    },
    dotIconStyle: {
        height: 10,
        width: 10,
        marginLeft: 5,
        backgroundColor: '#fff',
        borderRadius: 5,
        borderWidth: 0.5,
        borderColor: '#fff',
        marginVertical: 6,
    },
    dotTextStyle: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        color: '#ffffff',
        marginLeft: 4,
        textAlign: 'center'
    },
    live_now_btn_container: {
        flexDirection: 'row',
        marginTop: 8
    },
    live_btn_style: {
        backgroundColor: '#d8d8d8',
        borderRadius: 12,

    },
    LIVE_NOW_text_style: {
        fontFamily: Fonts.OpenSans_Bold,
        fontSize: 12,
        marginHorizontal: 13,
        marginVertical: 4
    },
    how_much_watching_count_text_style: {
        color: '#dddddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        alignSelf: 'center',
        marginLeft: 8
    },
    enent_indicator_container: {
        backgroundColor: '#101113',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:24
    },
    event_schedule_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        color: '#dddddd',
        marginTop: 24
    },
    indicator_view_container: {
        marginVertical: 28,
        flexDirection: 'row'
    },
    single_ladies_text_style: {
        color: '#dddddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
    },
    beyonce_text_style: {
        color: '#dddddd',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        marginTop: 5,
    },
    dot_view_container: {
        flex: 1, 
        alignItems: 'center', 
        height: 1, 
        backgroundColor: '#2a2b2d'
    },
    dot_view: {
        width: 10, 
        height: 10, 
        borderRadius: 10 / 2, 
        backgroundColor: '#2a2b2d', 
        bottom: 5
    },
    white_img_bg_style: {
        backgroundColor: 'transparent',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    img_title_beyonce_text_style:{
        color: '#1b1c20',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 16,
        marginTop:283
    },
    follower_n_rating_container:{
        flexDirection:'row',
        marginVertical:8
    },
    white_img_follow_count_text_style:{
        color: '#1b1c20',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
    },
    white_follow_artist_btn_style:{
        backgroundColor:'#1b1c20',
        borderRadius:10
    },
    follow_the_Artist_text_style:{
        color: '#ffffff',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 14,
        marginVertical:11,
        marginHorizontal:24
    },
    linearGradient: {
        opacity:0.98,
        flex: 1,
        width:'100%',
        justifyContent:'center',
        alignItems:"center"
      },
      tab_view_container:{
          flexDirection:'row',
          justifyContent:"center",
          alignItems:"center",
          marginHorizontal:16
      },
      tab_btn_text_style:{
        color: '#1b1c20',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        marginVertical:10,
      },
      tab_btn_style:{
          borderColor:'#101113',
          flex:1,
          justifyContent:'center',
          alignItems:'center',
          marginHorizontal:3
      },
      tab_containt_view:{
          flexDirection:'row',
          marginLeft:16
      },
      video_view_style:{
          width:202,
          height:114,
          backgroundColor:'#1b1c20',
          borderRadius:10,
          marginVertical:8,
          marginRight:8,
          justifyContent:'center',
          alignItems:'center'
      },
      past_event_video_title_text:{
          color:'#1b1c20', 
          fontFamily: Fonts.OpenSans_semibold,
          fontSize: 12,
      },
      album_view_style:{
          width:118,
          height:118,
        //   backgroundColor:'#1b1c20',
          borderRadius:6,
          marginVertical:8,
          marginRight:8,
          alignItems:'flex-start',
          justifyContent:'flex-end'
      },
      album_name_text_style_tab_view:{
        color: '#dddddd',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 8,
        marginBottom:9,
        marginLeft:10
      },
        buy_btn_album_view_style:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'rgba(0, 0, 0, 0.4)',
        width:64,
        height:24,
        borderRadius:10
      },
      BUY_text_style:{
            color:'#ffffff',
            fontFamily: Fonts.OpenSans_semibold,
            fontSize: 10,
      },
      album_main_view_container:{
          justifyContent:'center',
          alignItems:'center',
          marginBottom:24
      },
      album_main_view_Image_backgroud:{
          borderRadius:5
      },
      event_for_view_container:{
          backgroundColor:'#101113',
          justifyContent:'center',
          alignItems:'center',
          marginHorizontal:16
      },
      event_for_you_title_view:{
          flexDirection:'row'
      },
      events_for_you_text_title:{
          fontFamily:Fonts.PlayfairDisplay_Bold,
          color:'#dddddd',
          fontSize:18,
          marginTop:24,
    },
    see_all_text_title:{
        color:'#dddddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        marginTop:31,
    },
    lady_gaga_concert_img_bg_style:{
        backgroundColor: 'transparent',
        flex: 1,
        width:'100%',
        height:185,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:16,
        alignSelf:'center',
    },
    artist_name_title_text_event_for_you_style:{
        color: '#dddddd',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        marginTop:-15
    },
    linearGradient2:{
        opacity:1,
        flex: 1,
        width:'100%',
    },
    artist_name_n_concert_name_container:{
        flexDirection:'row',
        // marginLeft:25,
        // backgroundColor:'red',
        // justifyContent:'center',
        alignItems:'flex-start',
        marginTop:75
    },
    lady_gaga_user_pic_style:{
        marginBottom:24,
        marginHorizontal:10,
        height:80,
        width:80,
    },
    concert_title_text_style:{
        fontFamily:Fonts.PlayfairDisplay_Bold,
          color:'#dddddd',
          fontSize:16,
    },
    concert_timing_text_style:{
        fontFamily: Fonts.OpenSans_regular,
        color:'#dddddd',
        fontSize:8,
        marginTop:3,
        marginBottom:8
    },
    buy_tickets_concert_artist_btn_style:{
        backgroundColor:'#d8d8d8',
        borderRadius:5,
    },
    buy_ticket_concert_artist_text_style:{
        color:'#1b1c20',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        marginVertical:8,
        marginHorizontal:30
    },
    similar_artist_suggestion_view:{
        flexDirection:'row',
        marginVertical:20,
        alignSelf:'flex-start'
    },
    similar_artist_img__style:{
        width:150,
        height:150,
        borderRadius:150/2,
    },
    similar_artist_name_text__style:{
        color:'#dddddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        marginTop:10
    },
    similar_artist_box_view__style:{
        justifyContent:'center',
        alignItems:'center',
        marginRight:10
    },
})
import { StyleSheet, Dimensions } from 'react-native';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import { Fonts } from '../../../../common/fonts';
export const styles = StyleSheet.create({
    main_container: {
        margin: 16,
    },
    titleView: {
        flexDirection: 'row'
    },
    title: {
        fontSize: 12,
        color: '#ddd',
        fontFamily: Fonts.OpenSans_semibold,
        marginHorizontal: 5
    },
    formMainContent: {
        backgroundColor: '#1b1c1e',
        paddingHorizontal: 16,
        marginTop: 20,
        paddingVertical: 30,
        borderRadius: 10
    },
    formTitle: {
        fontSize: 16,
        color: '#ddd',
        fontFamily: Fonts.OpenSans_semibold,
        marginBottom: 15
    },
    formLabelStyle: {
        fontSize: 10,
        color: '#ddd',
        fontFamily: Fonts.OpenSans_semibold,
        marginTop: 13,
        marginLeft: 5
    },

    /* Second Screen */
    addressDisplayView: {
        backgroundColor: '#1b1c1e',
        paddingHorizontal: 14,
        marginTop: 20,
        paddingVertical: 16,
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    addressDetails: {
        flex: 1
    },
    addressTitle: {
        fontSize: 14,
        color: '#ddd',
        fontFamily: Fonts.OpenSans_semibold,
    },
    addressValueStyle: {
        fontSize: 12,
        color: '#636465',
        fontFamily: Fonts.OpenSans_regular,
        marginTop: 10
    },
    changeButton: {
        paddingHorizontal: 12,
        borderRadius: 12,
        paddingVertical: 10,
        justifyContent: 'center',
        height: 24
    },
    changeButtonText: {
        fontSize: 12,
        color: '#ddd',
        fontFamily: Fonts.OpenSans_semibold,
    },
    paymentContainer: {
        marginVertical: 10,
        backgroundColor: '#1b1c1e',
        borderRadius: 10,
    },
    paymentHeader: {
        paddingVertical: 20,
        paddingHorizontal: 14,
        borderBottomWidth: 1,
        borderBottomColor: '#979797'
    },
    paymentMethodsView: {
        flexDirection: 'row',
        paddingHorizontal: 16,
        paddingBottom: 24,
        flexWrap: 'wrap',
        paddingVertical: 12
    },
    paymentItem: {
        backgroundColor: '#dddddd',
        paddingVertical: 10,
        paddingHorizontal: 16,
        borderRadius: 10,
        marginRight: 8,
        flexDirection: 'row',
        marginTop: 12
    },
    paymentMethodName: {
        fontSize: 14,
        color: '#1b1c20',
        fontFamily: Fonts.OpenSans_semibold,
        marginHorizontal: 10
    },
    circleStyle: {
        height: 20,
        width: 20,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    paypalMainContainer: {
        paddingVertical: 20,
        width: '100%'
    },
    paypalButton: {
        width: '100%',
        height:'40%',
        backgroundColor: '#ffc200',
        paddingVertical: 12,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 32,
        marginTop: 16
    },
    paypalLogoStyle: {
        height: 20,
        width: 80
    },
    cardDesignView:{
        width:'100%',
        marginTop:32
    },
    cardSavedCardTxt:{
        color: '#ddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize:14,
    },
    cardLogoStyle: {
        height: 50,
        width: 80
    },
    paymentCalculationView: {
        marginTop: 10
    },
    calculationRow: {
        flexDirection: 'row',
        marginTop: 10,
        alignItems: 'center'
    },
    calculationLeftText: {
        flex: 1,
        fontSize: 16,
        color: '#ddd',
        fontFamily: Fonts.OpenSans_regular,
    },
    calculationRightText: {
        fontSize: 16,
        color: '#ddd',
        fontFamily: Fonts.OpenSans_regular,
    },
    finalTotalStyle: {
        marginTop: 12,
        borderTopColor: '#979797',
        borderTopWidth: 1,
        paddingTop: 8
    },
    helpIconStyle: {
        marginHorizontal: 10,
        flex: 1
    },
    calculationRightTotalText: {
        fontSize: 24,
        color: '#ddd',
        fontFamily: Fonts.OpenSans_semibold,
    },

    /* Screen THREE */

    addCardView: {
        marginTop: 20,
        width: '100%'
    },
    addCardHeader: {
        paddingVertical: 10,
        flexDirection: 'row'
    },
    cardHeaderText: {
        fontSize: 14,
        color: '#ddd',
        fontFamily: Fonts.OpenSans_semibold,
        flex: 1
    },
    crossButtonStyle: {

    },
    cardView: {
        backgroundColor: '#333333',
        borderRadius: 10,
        paddingHorizontal: 16,
        paddingVertical: 12
    },
    cardNameStyle: {
        fontSize: 14,
        color: '#ddd',
        fontFamily: Fonts.OpenSans_semibold,
    },
    cardLabelStyle: {
        fontSize: 10,
        color: '#ddd',
        fontFamily: Fonts.OpenSans_regular,
        marginTop: 16
    },
    cardNumberInputsview: {
        flexDirection: 'row'
    },
    textBoxStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: '#ddd',
        marginRight: 16,
        color: '#ddd',
        height: 40
    },
    expiryDateStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    dateSlashStyle: {
        fontSize: 23,
        color: '#ddd',
        fontFamily: Fonts.OpenSans_regular,
        marginRight: 10
    },
    cardValidationDetails: {
        flexDirection: 'row',
        marginVertical: 12
    },
    cardValidationItem: {
        flex: 2
    },
    saveCardView: {
        flexDirection: 'row',
        paddingHorizontal: 16,
        marginTop: 25,
        alignItems: 'center',
    },
    cardValidationLabelStyle: {
        fontSize: 12,
        color: '#ddd',
        fontFamily: Fonts.OpenSans_regular,
    },
    makePaymentButton: {
        backgroundColor: '#fff',
        borderRadius: 10,
        paddingVertical: 18,
        width: '50%',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 25
    },
    makePaymentButtonText: {
        color: '#1b1c20',
        fontSize: 14,
        fontFamily: Fonts.OpenSans_semibold,
    },
    round_selected_btn: {
        justifyContent: 'center',
        alignItems: 'center'
    },round_radio_style: {
        width: 24,
        height: 24,
        borderRadius: 24 / 2,
        borderWidth: 1,
        borderColor: '#ddd',
        justifyContent: 'center',
        alignItems: 'center'
    },
    selected_btn_style: {
        width: 14,
        height: 14,
        borderRadius: 24 / 2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#ddd'
    },
    selectedCardStyle: {
        fontSize:14,
        color:'#ddd',
        marginVertical:5
    },
    cardTextAplicantNameStyle:{
        fontFamily:Fonts.OpenSans_regular,
        fontSize:14,
        color:'#ddd',
        marginLeft:20
    },
})
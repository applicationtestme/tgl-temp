export function GetEventDetails() {
  return {
    type: "EVENTDETAIL_SAGA",
  }
}
export function EventDetailsFollow() {
  return {
    type: "EVENTDETAIL_FOLLOW_ARTIST_SAGA"
  }
}
export function EventDetailsBuyTicket() {
  return {
    type: "EVENTDETAIL_BUY_TICKET_SAGA"
  }
}
export function EventDetailsSendComment() {
  return {
    type: "EVENTDETAIL_SEND_COMMENT_SAGA"
  }
}

export function GetEventVipSupporters() {
  return {
    type: "GET_EVENT_VIPSUPPORTERS"
  }
}
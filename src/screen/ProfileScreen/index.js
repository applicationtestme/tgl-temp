import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, ImageBackground, KeyboardAvoidingView, TextInput } from 'react-native';
import { styles } from './styles';
import { SimilarEvent } from '../../components/SimilarEvent'
import { Genres_round_view } from '../../components/Genres_round_view'
import { Similar_artist } from '../../components/Similar_artist'
import LinearGradient from 'react-native-linear-gradient';
import { Images } from '../../common/Images'
import { Popover, PopoverController } from 'react-native-modal-popover';
import RBSheet from "react-native-raw-bottom-sheet";
import Entypo from 'react-native-vector-icons/dist/Entypo';
import Fontisto from 'react-native-vector-icons/dist/Fontisto';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export default class ProfileScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            empty_Tab_Selected_tab: true,
            mutualFriendTab_tab: false,
            eventsAttended_tab: false,
            artist_following_tab: false,
            genre_tab: false,
            openDropDown: false,
            follow: false,
            visible: false,
            editProfile: true,
            roomList: [
                { 'name': 'Unfollow', 'isAdmin': 0, 'hasNotification': 0 },
                { 'name': 'Block', 'isAdmin': 0, 'hasNotification': 0 },

            ],
            text: '',
            artistData: [
                {
                    'artistName': 'Taylor Swift',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://schnncdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Taylor_Swift_003_20200226074119_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Martin Garrix',
                    'eventName': 'Summer Tour',
                    'artistProfileImage': 'https://sklktcdnems01.cdnsrv.jio.com/c.saavncdn.com/artists/Martin_Garrix_004_20200303120820_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'David Guetta',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://sklktcdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/David_Guetta_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Selena Gomez',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://sklktecdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Selena_Gomez_002_20200226073835_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Demi Lovato',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://schnncdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Demi_Lovato_002_20200312120805_500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
                {
                    'artistName': 'Slash',
                    'eventName': 'Parachutes',
                    'artistProfileImage': 'https://snoidcdnems01.cdnsrv.jio.com/c.saavncdn.com/607/You-re-A-Lie-English-2012-500x500.jpg',
                    'eventTime': 'Aug 25 4:30 PM (PST)',
                    'isBuyTicket': 0
                },
            ],
            featureFanList: [
                { name: 'Alice Janes', address: 'Minnessota, US', img_url: 'https://sklktecdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Selena_Gomez_002_20200226073835_500x500.jpg' },
                { name: 'Paul Pogba', address: 'Nashville, US', img_url: 'https://sklktcdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/David_Guetta_500x500.jpg' },
                { name: 'Alice Janes', address: 'Minnessota, US', img_url: 'https://schnncdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Demi_Lovato_002_20200312120805_500x500.jpg' },
                { name: 'Alice Janes', address: 'Minnessota, US', img_url: 'https://sklktcdnems01.cdnsrv.jio.com/c.saavncdn.com/artists/Martin_Garrix_004_20200303120820_500x500.jpg' },
            ]
        };

    }
    toggleAnswer = (value) => {
        if (value === 'Mutual Friends') {
            this.setState({ eventsAttended_tab: false, mutualFriendTab_tab: true, empty_Tab_Selected_tab: false })
            this.setState({ mutualFriendTab_tab: true, event_attended: false, empty_Tab_Selected_tab: false })
        }
        if (value === 'Events Attended') {
            this.setState({ mutualFriendTab_tab: false, event_attended: true, empty_Tab_Selected_tab: false })
            this.setState({ eventsAttended_tab: true, mutualFriendTab_tab: false, empty_Tab_Selected_tab: false })
        }
    }
    followProcess = () => {
        this.setState({ editProfile: !this.state.editProfile })
    }
    dropdownProcess = () => {
        this.setState({ openDropDown: !this.state.openDropDown })
    }
    handelValue = (item) => {

    }
    render() {
        var data = [["Unfollow", "Block"]];
        return (
            <KeyboardAvoidingView behaviour="height" style={styles.main_container}>
                <ScrollView contentContainerStyle={styles.scrlView_container}>
                    <ImageBackground source={Images.fan_ImageBackground} style={styles.img_bg_style}>
                        <View style={styles.header_container_style}>
                            <TouchableOpacity style={styles.header_user_img_style}></TouchableOpacity>
                        </View>
                        <View style={styles.artist_img_viewer}>
                            <View style={{ flexDirection: 'row', flex: 1, }}>
                                <View style={{ flex: 1, borderRadius: 10, }}>
                                </View>
                                <View style={{ flex: 1, borderWidth: 3, borderColor: '#535353', backgroundColor: '#535353', borderRadius: 10, marginRight: 15 }}>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', flex: 1, }}>
                                <View style={{ flex: 1, borderWidth: 3, borderColor: '#535353', backgroundColor: '#535353', borderRadius: 10, }}>
                                </View>
                                <View style={{ flex: 1, borderRadius: 10, }}>
                                </View>
                            </View>
                            <ImageBackground source={Images.fan_img} imageStyle={{ borderRadius: 10, marginLeft: 0, marginTop: 8 }} style={{ position: 'absolute', height: '97%', width: '95%', justifyContent: 'flex-end' }}>
                                <View style={{ alignItems: 'flex-end', flex: 1, marginTop: 5 }}>
                                    <Image style={styles.medal_img_style} source={Images.Gold_Medal}></Image>
                                </View>
                                <View style={{ margin: 24, }}>
                                    <Text style={styles.Artist_name_text}>Chris Daniels</Text>
                                    <View style={[styles.follow_pop_up_btn, { backgroundColor: this.state.follow === true ? '#6e6e6e' : '#fff' }]}>
                                        <TouchableOpacity style={{ flex: 3, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.followProcess()}>
                                            <Text style={styles.follow_btn_text}>Edit Profile</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </ImageBackground>
                        </View>
                        <View style={styles.artist_following_details_container}>
                            <View style={styles.gigs_enjoy_count_num_view}>
                                <View style={styles.box_title_view}>
                                    <Text style={styles.following_details_title_text}>Gigs Enjoyed</Text>
                                </View>
                                <View style={styles.box_following_count_view}>
                                    <Text style={styles.following_details_count_text}>32</Text>
                                </View>
                            </View>
                            <View style={styles.gigs_enjoy_count_num_view}>
                                <View style={styles.box_title_view}>
                                    <Text style={styles.following_details_title_text}>Gigs Enjoyed</Text>
                                </View>
                                <View style={styles.box_following_count_view}>
                                    <Text style={styles.following_details_count_text}>32</Text>
                                </View>
                            </View>
                            <View style={styles.gigs_enjoy_count_num_view}>
                                <View style={styles.box_title_view}>
                                    <Text style={styles.following_details_title_text}>Gigs Enjoyed</Text>
                                </View>
                                <View style={styles.box_following_count_view}>
                                    <Text style={styles.following_details_count_text}>32</Text>
                                </View>
                            </View>
                        </View>
                    </ImageBackground>
                    {this.state.editProfile === true ?
                        <View style={styles.body_contianer}>
                            <View style={{}}>
                                <View style={styles.location_n_details_view}>
                                    <View style={styles.location_n_social_icon_view}>
                                        <Entypo name='location-pin' size={30} color='#c4c4c4'></Entypo >
                                        <Text style={styles.location_text_style}>Nashville, USA</Text>
                                        <TouchableOpacity style={[styles.social_btn_view, { marginLeft: 16 }]}>
                                            <Fontisto style={styles.social_icon_style} name='twitter' size={18} color='#c4c4c4'></Fontisto>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.social_btn_view}>
                                            <Fontisto name='facebook' style={[styles.social_icon_style, { marginHorizontal: 15 }]} size={18} color='#c4c4c4'></Fontisto>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.about_you_view}>
                                <Text style={styles.about_you_text_style}>About You</Text>
                                <Text style={styles.there_here_for_musical_text_style}>Hey there! Here for a musical gig!</Text>
                                <View style={styles.graph_view_container}>
                                    <View style={{}}>
                                        <Text style={styles.Profile_Remaining_text_style}>Profile Remaining <Text style={styles.pertage_text_style}>25%</Text></Text>
                                    </View>
                                    <View style={styles.graph_img_view}>
                                        <Image source={Images.graph_bg_img} resizeMode="center" style={styles.graph_img_style}></Image>
                                    </View>
                                    <View style={{ marginTop: 10, flex: 2 }}>
                                        <Text style={styles.nextup_text_style}>Nextup</Text>
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                            <Text style={styles.Setup_Instagram_text_style}>Setup Instagram Handle  </Text>
                                            <Fontisto name='arrow-right' size={10} color='#fff'></Fontisto>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.ultra_budge_view}>
                                <View style={styles.ultra_badge_n_data_view}>
                                    <View style={styles.user_badge_view}>
                                        <Image source={Images.ultra_user_medal} style={styles.badge_icon_style}></Image>
                                    </View>
                                    <View style={styles.budge_data_view}>
                                        <Text style={styles.bedge_title_text_style}>Almost an ultra user!</Text>
                                        <Text style={styles.bedge_description_text_style}>3 more events to attend & you’ll become an Ultra user</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.now_on_gigs_view}>
                                <View style={styles.dollor_pocket_img_view}>
                                    <Image source={Images.money_bag2} style={styles.dollor_beg_img_style}></Image>
                                </View>
                                <View style={styles.now_on_gigs_data_view}>
                                    <Text style={styles.bedge_title_text_style}>NEW on GIGS</Text>
                                    <Text style={styles.bedge_description_text_style}>You can now tip the artist, live!</Text>
                                </View>
                                <View style={styles.line_new_view}>
                                    <Text style={styles.NEW_text_style}>NEW</Text>
                                </View>
                            </View>
                            <View style={styles.Badges_earned_view}>
                                <Text style={styles.Badges_Earned_text_style}>Badges Earned</Text>
                                <View style={styles.budge_icon_container}>
                                <ScrollView horizontal={true} contentContainerStyle={{width:width}} showsHorizontalScrollIndicator={false}>
                                    <PopoverController  >
                                        {({ openPopover, closePopover, popoverVisible, setPopoverAnchor, popoverAnchorRect }) => (
                                            <React.Fragment>
                                                <TouchableOpacity style={styles.unlimited_fan_badge_view} ref={setPopoverAnchor} onPress={openPopover}>
                                                    <View style={styles.unlimited_fan_badge_view}>
                                                        <Image style={styles.badge_img_style} source={Images.unlimited_badge}></Image>
                                                    </View>
                                                </TouchableOpacity>
                                                <Popover
                                                    contentStyle={{backgroundColor:'#313131f5',borderRadius:10,marginHorizontal:20,}}
                                                    backgroundStyle={{backgroundColor:'transparent',}}
                                                    visible={popoverVisible}
                                                    arrowStyle={{borderTopColor:'traparent'}}
                                                    onClose={closePopover}
                                                    placement='auto'
                                                    fromRect={popoverAnchorRect}
                                                    supportedOrientations={['portrait', 'landscape']}
                                                >
                                                    <View style={styles.tooltip_main_view}>
                                                        <View style={styles.tooltip_title_view}>
                                                            <View style={styles.badge_icon_view}>
                                                                <Image style={{resizeMode:'contain',height:71,width:78}} source={Images.star_badge_icon}></Image>
                                                            </View>
                                                           <View style={styles.toottip_title_view}>
                                                                <Text style={styles.title_text_style}>Profile Completion Badge</Text>
                                                                <Text style={styles.Easy_Badge_text_style}>Easy Badge</Text>
                                                           </View>
                                                           <View style={styles.close_btn_view_tool_tip}>
                                                           <Fontisto name='close-a' size={18} color='#c4c4c4'></Fontisto>
                                                           </View>
                                                        </View>
                                                        <Text style={styles.status_text_style}>Status: To Acquire</Text>
                                                        <View style={{marginHorizontal:16,marginTop:32,}}>
                                                            <Text numberOfLines={5} style={styles.details_text_style}>You get this badge when you have completed your{"\n"} whole Profile.
                                                            Lorem Ipsum is simply dummy text of the {"\n"} printing and typesetting industry. 
                                                            Lorem Ipsum has been {"\n"} the industry's standard dummy text ever since.</Text>
                                                        </View>
                                                        <View style={{marginTop:24,width:'50%',marginLeft:16,marginBottom:24}}>
                                                            <Text style={styles.nexttip_decription_text_style}>Your Profile is 25% Remaining{"\n"} Nextup: Setup Instagram Link</Text>
                                                        </View>
                                                        <TouchableOpacity style={styles.tooltip_white_btn_style}>
                                                            <Text style={styles.tooltip_white_btn_text_style}>Setup Instagram link</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                </Popover>
                                            </React.Fragment>
                                        )}
                                    </PopoverController>

                                    <View style={{ borderWidth: 0.4, height: '90%', borderColor: '#fff', width: 0, borderStyle: 'dashed', borderRadius: 15 }}></View>
                                    <View style={styles.unlimited_fan_badge_view}>
                                        <Image style={styles.badge_img_style} source={Images.super_fan}></Image>

                                    </View>
                                    <View style={{ borderWidth: 0.4, height: '90%', borderColor: '#fff', width: 0, borderStyle: 'dashed', borderRadius: 15 }}></View>
                                    <View style={styles.unlimited_fan_badge_view}>
                                        <Image style={styles.badge_img_style} source={Images.regular_badge}></Image>
                                    </View>
                                    <View style={{ borderWidth: 0.4, height: '90%', borderColor: '#fff', width: 0, borderStyle: 'dashed', borderRadius: 15 }}></View>
                                    <View style={styles.unlimited_fan_badge_view}>
                                        <Image style={styles.badge_img_style} source={Images.regular_badge}></Image>
                                    </View>
                                    </ScrollView>
                                </View>
                            </View>
                            <View style={styles.Chris_Photos_view}>
                                <Text style={styles.Chris_Photos_text_style}>Chris’ Photos</Text>
                                <View style={styles.Chris_Photos_inside_view}>
                                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                        <TouchableOpacity style={styles.upload_pic_btn_view}>
                                            <View style={styles.upload_pic_inside_btn_view}>
                                                <View style={styles.camera_btn_round_view}>
                                                    <Fontisto name='camera' size={17} color='#575757'></Fontisto>
                                                </View>
                                                <Text style={styles.upload_pic_text_style}>Upload Pictures</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.upload_pic_btn_view}>
                                            <Image style={{ width: 197, height: 197, borderRadius: 10 }} source={Images.fan_img}></Image>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.upload_pic_btn_view}>
                                            <Image style={{ width: 197, height: 197, borderRadius: 10 }} source={Images.fan_img}></Image>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.upload_pic_btn_view}>
                                            <Image style={{ width: 197, height: 197, borderRadius: 10 }} source={Images.fan_img}></Image>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.upload_pic_btn_view}>
                                            <Image style={{ width: 197, height: 197, borderRadius: 10 }} source={Images.fan_img}></Image>
                                        </TouchableOpacity>
                                    </ScrollView>
                                </View>
                            </View>
                            <View style={styles.saved_event_view}>
                                <Text style={styles.Chris_Photos_text_style}>Saved Events</Text>
                                <View style={{ flexDirection: 'row', marginTop: 8 }}>
                                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                        <SimilarEvent artistData={this.state.artistData} />
                                    </ScrollView>
                                </View>
                            </View>
                            <View style={styles.artist_following_view}>
                                <Text style={styles.Chris_Photos_text_style}>Artists Following</Text>
                                <View style={{ flexDirection: 'row', marginTop: 8 }}>
                                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                        <TouchableOpacity style={styles.upload_pic_btn_view}>
                                            <View style={[styles.upload_pic_inside_btn_view, { width: 164, marginTop: 20, marginBottom: 6, marginHorizontal: 20 }]}>
                                                <View style={styles.camera_btn_round_view}>
                                                    <Fontisto name='plus-a' size={17} color='#575757'></Fontisto>
                                                </View>
                                                <Text style={[styles.upload_pic_text_style, { textAlign: 'center' }]}>Find & Follow More Artists on the Gigs</Text>
                                                <TouchableOpacity style={styles.explore_all_artist_btn_view}>
                                                    <Text style={styles.explore_all_artist_text_style}>Explore all Artists</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </TouchableOpacity>
                                        <Similar_artist titleVisible={false} />
                                    </ScrollView>
                                </View>
                                <View style={styles.explore_the_genre_view}>
                                    <Text style={styles.Chris_Photos_text_style}>Explore the Genres</Text>
                                    <View style={[styles.Chris_Photos_inside_view, {}]}>
                                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                            <TouchableOpacity style={styles.upload_pic_btn_view}>
                                                <Text style={styles.explore_genre_text_style}>Explore Genres</Text>
                                            </TouchableOpacity>
                                            <View style={{}}>
                                                <Genres_round_view titleVisible={false} />
                                            </View>
                                        </ScrollView>
                                    </View>
                                </View>
                                <View style={styles.fan_like_you_view}>
                                    <Text style={styles.Chris_Photos_text_style}>Fans like you</Text>
                                    <View style={{ flexDirection: 'row' }}>
                                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} >
                                            {this.state.featureFanList.map((item, index) => {
                                                return (
                                                    <View style={styles.featureFanView}>
                                                        <Image source={{ uri: item.img_url }} style={styles.featureFanUserImg}></Image>
                                                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                            <Text style={styles.feature_Fan_name_text_style}>{item.name}</Text>
                                                            <Text style={styles.feature_Fan_address_text_style}>{item.address}</Text>
                                                        </View>
                                                    </View>
                                                )
                                            })}
                                        </ScrollView>
                                    </View>
                                </View>
                            </View>
                        </View>
                        :
                        <View style={styles.body_contianer_edit_page}>
                            <View>
                                <View style={styles.location_input_view}>
                                    <Entypo name='location-pin' size={30} color='#c4c4c4'></Entypo>
                                    <View style={{ width: '30%', }}>
                                        <TextInput underlineColorAndroid='#2c2c2e' style={styles.location_text_input_style}>Nashville, USA</TextInput>
                                    </View>
                                </View>
                                <View style={styles.twiter_input_view}>
                                    <TouchableOpacity style={[styles.social_btn_view_edit_page]}>
                                        <Fontisto name='twitter' size={13} color='#c4c4c4'></Fontisto>
                                    </TouchableOpacity>
                                    <View style={{ width: '50%', marginLeft: 16 }}>
                                        <TextInput underlineColorAndroid='#2c2c2e' style={styles.location_text_input_style}>twitter.com/user.com</TextInput>
                                    </View>
                                </View>
                                <View style={styles.twiter_input_view}>
                                    <TouchableOpacity style={[styles.social_btn_view_edit_page]}>
                                        <Fontisto name='facebook' size={13} color='#c4c4c4'></Fontisto>
                                    </TouchableOpacity>
                                    <View style={{ width: '50%', marginLeft: 16 }}>
                                        <TextInput underlineColorAndroid='#2c2c2e' style={styles.location_text_input_style}>facebook.com/user.c...</TextInput>
                                    </View>
                                </View>
                                <View style={styles.twiter_input_view}>
                                    <TouchableOpacity style={[styles.social_btn_view_edit_page]}>
                                        <Fontisto name='instagram' size={13} color='#c4c4c4'></Fontisto>
                                    </TouchableOpacity>
                                    <View style={{ width: '50%', marginLeft: 16 }}>
                                        <TextInput underlineColorAndroid='#2c2c2e' style={styles.location_text_input_style} placeholderTextColor={'rgba(255, 255, 255, 0.5)'} placeholder={'Instagram User ID'}></TextInput>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.about_you_edit_page}>
                                <View style={styles.about_you_edit_page_title}>
                                    <Text style={styles.about_you_edit_page_text_style}>About you</Text>
                                    <Text style={styles.remaning_text_count_text_style}>58/200</Text>
                                </View>
                                <View style={styles.paregraph_view}>
                                    <TextInput numberOfLines={5} style={{ marginLeft: 16, color: '#fff' }} placeholder={'Hey there! Here for a musical gig!'} placeholderTextColor='rgba(221, 221, 221, 0.5)'></TextInput>
                                </View>
                                <TouchableOpacity style={styles.save_change_btn_view}>
                                    <Text style={styles.Saves_Changes_text_style}>Saves Changes</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    }
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}


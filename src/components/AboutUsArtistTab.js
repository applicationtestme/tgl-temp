import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, StyleSheet,ImageBackground } from 'react-native';
import { Fonts } from '../common/fonts';
import { Images } from '../common/Images';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import LinearGradient from 'react-native-linear-gradient';
export class AboutUsArtistTab extends Component {

    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render(){
        return(

            <View style={styles.main_container}>
                <View style={{width:'50%'}}>
                    <Text style={styles.title_text}>About</Text>
                    <View style={styles.Textview_container}>
                        <ScrollView>
                        <Text style={styles.about_text}>
                            Taylor Alison Swift is an American singer-songwriter. Her narrative songwriting, which often centers around her personal life, has received widespread media coverage. Born in West Reading, Pennsylvania, Swift relocated to Nashville, Tennessee in 2004 to pursue a career in country music. At age 14, she became the youngest artist signed by the Sony/ATV Music publishing house, and at age 15, she signed her first record deal. Her 2006 eponymous debut studio album was the longest-charting album of the 2000s on the Billboard 200. Its third single, "Our Song", made her the youngest person to single-handedly write and perform a number-one song on the Billboard Country Airplay chart. Swift's breakthrough second studio album, Fearless (2008), won four Grammy Awards, including Album of the Year, and was certified Diamond by the Recording Industry Association of America. It yielded the crossover hit singles "Love Story" and "You Belong with Me". 
                        </Text>
                        </ScrollView>
                    </View>
                </View>
                
                <View style={{width:'50%'}}>
                    <ImageBackground source={{ uri: 'https://schnncdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Taylor_Swift_003_20200226074119_500x500.jpg' }} style={styles.imag_container}>
                        <LinearGradient colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0,0.91)']} style={styles.linearGradient2}>
                        
                        </LinearGradient>
                    </ImageBackground>
                </View>
                
            </View>
        )
    }
}
const styles = StyleSheet.create({
    main_container:{
        flex:1,
        flexDirection:'row',
        marginTop:5
    },
    title_text: {
        color: '#fff',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 24,
        textAlign:'center'
    },
    linearGradient2:{
        opacity:1,
        flex: 1,
        width:'100%',
        height:10,
        justifyContent:'center',
        alignItems:'center'
    },
    about_text:{
        color: '#fff',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 12,
    },
    imag_container:{
        height:250,
        width:'100%'
    },
    Textview_container:{
        height:210,
        width:'100%'
    }
})
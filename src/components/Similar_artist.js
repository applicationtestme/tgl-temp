import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Fonts } from '../common/fonts';
import { Images } from '../common/Images';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
export class Similar_artist extends Component {

    render() {
        return (
            <View >
                <View style={styles.event_for_you_title_view}>
                    <View style={{ flex: 1 }}>
                        <Text style={styles.events_for_you_text_title}>Similar Artists</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-end' }}>
                        <TouchableOpacity>
                            <Text style={styles.see_all_text_title}>See All</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.similar_artist_suggestion_view}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        <TouchableOpacity style={styles.similar_artist_box_view__style}>
                            <Image source={{ uri: 'https://schnncdnems04.cdnsrv.jio.com/c.saavncdn.com/artists/Taylor_Swift_003_20200226074119_500x500.jpg' }} style={styles.similar_artist_img__style}>
                            </Image>
                            <Text style={styles.similar_artist_name_text__style}>Taylor Swift</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.similar_artist_box_view__style}>
                            <Image source={{ uri: 'https://sklktcdnems01.cdnsrv.jio.com/c.saavncdn.com/artists/Martin_Garrix_004_20200303120820_500x500.jpg' }} style={styles.similar_artist_img__style}>
                            </Image>
                            <Text style={styles.similar_artist_name_text__style}>Martin Garrix</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.similar_artist_box_view__style}>
                            <Image source={{ uri: 'https://sklktcdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/David_Guetta_500x500.jpg' }} style={styles.similar_artist_img__style}>
                            </Image>
                            <Text style={styles.similar_artist_name_text__style}>David Guetta</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.similar_artist_box_view__style}>
                            <Image source={{ uri: 'https://sklktecdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Selena_Gomez_002_20200226073835_500x500.jpg' }} style={styles.similar_artist_img__style}>
                            </Image>
                            <Text style={styles.similar_artist_name_text__style}>Selena Gomez</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.similar_artist_box_view__style}>
                            <Image source={{ uri: 'https://schnncdnems02.cdnsrv.jio.com/c.saavncdn.com/artists/Demi_Lovato_002_20200312120805_500x500.jpg' }} style={styles.similar_artist_img__style}>
                            </Image>
                            <Text style={styles.similar_artist_name_text__style}>Demi Lovato</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.similar_artist_box_view__style}>
                            <Image source={{ uri: 'https://snoidcdnems01.cdnsrv.jio.com/c.saavncdn.com/607/You-re-A-Lie-English-2012-500x500.jpg' }} style={styles.similar_artist_img__style}>
                            </Image>
                            <Text style={styles.similar_artist_name_text__style}>Slash</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    event_for_you_title_view: {
        flexDirection: 'row'
    },
    events_for_you_text_title: {
        fontFamily: Fonts.PlayfairDisplay_Bold,
        color: '#dddddd',
        fontSize: 18,
        marginTop: 24,
    },
    see_all_text_title: {
        color: '#dddddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        marginTop: 31,
    },
    similar_artist_suggestion_view: {
        flexDirection: 'row',
        marginVertical: 20,
        alignSelf: 'flex-start'
    },
    similar_artist_img__style: {
        width: 150,
        height: 150,
        borderRadius: 150 / 2,
    },
    similar_artist_name_text__style: {
        color: '#dddddd',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        marginTop: 10
    },
    similar_artist_box_view__style: {
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10
    },
})



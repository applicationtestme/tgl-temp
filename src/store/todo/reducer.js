let initialState = {
  isFetching: false,
  title: "Press Me",
}

export default function (state = initialState, action) {
  switch (action.type) {

    case "STORE_TOKEN":
      {
        return {
          ...state,
          type: action.type,
          login_token: action.login_token,
          my_id: action.user_id
        }
      }

    case 'LOAD_FAIL':
      console.log('consoling: ', action.userData)
      let title = state.title
      let updated = ''
      if (title === 'Press Me') {
        updated = 'Press'
      } else {
        updated = 'Press Me'
      }
      return {
        ...state,
        title: updated
      }

    default:
      return state;
  }
}

// const reducer=(state=initialState,action)=>{
//   switch(action.type){
//     case 'ADD_TASK_SUCCESSED':
//       return {
//         ...state,
//         listTodo:[...state.listTodo,action.newTask],
//         isFetching:false
//       }
//     case 'IS_FETCHING':
//       return {
//         ...state,
//         isFetching:true
//       }
//     case 'LOAD_SUCCESSED':
//       return{
//         ...state,
//         listTodo:action.listTodo.reverse(),
//         isFetching:false,
//       }
//     case 'LOAD_FAIL':
//       let title = JSON.stringify([...state])
//       alert(title)
//       let updated = ''
//       if(title === 'Press Me') {
//         updated = 'Press'
//       } else {
//         updated = 'Press'
//       }
//       return{
//         ...state,
//         title: updated
//       }

//     default:
//       return state;
//   }
// }
// export default reducer
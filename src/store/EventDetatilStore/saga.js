import { call, put, takeEvery, takeLatest, all } from 'redux-saga/effects'
import { delay } from 'redux-saga'


const EventURL = "https://tgl-api.herokuapp.com/api/event/";
const ChatURL = "https://tgl-api.herokuapp.com/api/chat/";
const UserURL = "https://tgl-api.herokuapp.com/api/user/";
const ArtistURL = "https://tgl-api.herokuapp.com/api/artist/";

export function* addNewTask(action) {
  try {
    yield put({ type: 'IS_FETCHING' })
    const response = yield call(axios.post, 'http://jsonplaceholder.typicode.com/todos/', {
      userId: 7,
      title: action.newTask,
      completed: false,
    })
    yield put({ type: 'ADD_TASK_SUCCESSED', newTask: action.newTask })
  } catch (err) {
    yield put({ type: 'LOAD_FAIL' })
  }
}

export function EventDataAPI() {
  const url = EventURL + 'GetEventDetails';
  return fetch(url, {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json'
    }
  })
    .then(res =>
      res.json()
    ).then(json => {
      // console.log("GEt DATA Evenet Data : " + JSON.stringify(json))
      return json
    })
}
export function CommentListAPI() {
  const url = ChatURL + 'CommentList';
  return fetch(url, {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json'
    }
  })
    .then(res =>
      res.json()
    ).then(json => {
      // console.log("Comment Detail : " + JSON.stringify(json))
      return json
    })
}
export function EventsForYouAPI() {
  const url = EventURL + 'EventsForYou';
  return fetch(url, {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json'
    }
  })
    .then(res =>
      res.json()
    ).then(json => {
      // console.log("Evenet For You : " + JSON.stringify(json))
      return json
    })
}
export function GetUserByAlbumAPI() {
  const url = EventURL + 'GetUserByAlbum';
  return fetch(url, {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json'
    }
  })
    .then(res =>
      res.json()
    ).then(json => {
      // console.log("GetUserByAlbumAPI: " + JSON.stringify(json))
      return json
    })
}
export function GetArtistPastEventListAPI() {
  const url = ArtistURL + 'GetArtistPastEventList';
  return fetch(url, {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json'
    }
  })
    .then(res =>
      res.json()
    ).then(json => {
      // console.log("GetArtistPastEventListAPI : " + JSON.stringify(json))
      return json
    })
}
export function GetSimilerArtistListAPI() {
  const url = ArtistURL + 'SimilarArtist';
  return fetch(url, {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json'
    }
  })
    .then(res =>
      res.json()
    ).then(json => {
      console.log("GetArtistPastEventListAPI : " + JSON.stringify(json))
      return json
    })
}

export function GetEventScheduleAPI() {
  const url = EventURL + 'GetEventSchedule';
  return fetch(url, {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json'
    }
  })
    .then(res =>
      res.json()
    ).then(json => {
      console.log("GetArtistPastEventListAPI : " + JSON.stringify(json))
      return json
    })
}

export function FollowArtistAPI() {
  const url = UserURL + 'FollowArtist';
  return fetch(url, {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json'
    }
  })
    .then(res =>
      res.json()
    ).then(json => {
      // console.log("Follow Artis : " + JSON.stringify(json));
      return json
    })
}
export function BuyTicketAPI() {
  const url = EventURL + 'BuyTicket';
  return fetch(url, {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json'
    }
  })
    .then(res =>
      res.json()
    ).then(json => {
      // console.log("Buy Ticket: " + JSON.stringify(json));
      return json
    })
}
export function GetTicketAPI() {
  const url = EventURL + 'GetTicket';
  return fetch(url, {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json'
    }
  })
    .then(res =>
      res.json()
    ).then(json => {
      // console.log("Get Ticket : " + JSON.stringify(json));
      return json
    })
}

export function SendCommentAPI() {
  const url = ChatURL + 'SendComment';
  return fetch(url, {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json'
    }
  })
    .then(res =>
      res.json()
    ).then(json => {
      // console.log("Get Ticket : " + JSON.stringify(json));
      return json
    })
}

export function* EventDetailOnLoad() {
  try {
    yield put({ type: 'EVENTDETAIL_ON_LOAD_BEGIN' })// send reducer type
    let userData = yield call(EventDataAPI);
    let commentList = yield call(CommentListAPI);
    let EventsForYou = yield call(EventsForYouAPI);
    let getUserByAlbum = yield call(GetUserByAlbumAPI);
    let getArtistPastEventList = yield call(GetArtistPastEventListAPI);
    let getSimilerArtistList = yield call(GetSimilerArtistListAPI);
    let getEventSchedule = yield call(GetEventScheduleAPI);
    yield put({ type: 'EVENTDETAIL_ON_LOAD_SUCCESS', userData, commentList, EventsForYou, getUserByAlbum, getArtistPastEventList, getSimilerArtistList,getEventSchedule })// send reducer type
  } catch (err) {
    yield put({ type: 'EVENTDETAIL_ON_LOAD_FAIL' })// send reducer type
  }
}
export function* EventDetailOnFollowArtist() {
  try {
    yield put({ type: 'EVENTDETAIL_FOLLOW_ARTIST_BEGIN' })// send reducer type
    let followArtist = yield call(FollowArtistAPI);
    yield put({ type: 'EVENTDETAIL_FOLLOW_ARTIST_SUCCESS', followArtist })// send reducer type
  } catch (err) {
    yield put({ type: 'EVENTDETAIL_FOLLOW_ARTIST_FAIL' })// send reducer type
  }
}
export function* EvenetDetailBuyTicket() {
  try {
    yield put({ type: 'EVENTDETAIL_BUY_TICKET_BEGIN' })// send reducer type
    let buyTicket = yield call(BuyTicketAPI);
    let getTicket = yield call(GetTicketAPI);
    yield put({ type: 'EVENTDETAIL_BUY_TICKET_SUCCESS', buyTicket,getTicket })// send reducer type
  } catch (err) {
    yield put({ type: 'EVENTDETAIL_BUY_TICKET_FAIL' })// send reducer type
  }
}
export function* EvenetDetailSendComment() {
  try {
    yield put({ type: 'EVENTDETAIL_SEND_COMMENT_BEGIN' })// send reducer type
    let sendComment = yield call(SendCommentAPI);
    yield put({ type: 'EVENTDETAIL_SEND_COMMENT_SUCCESS', sendComment })// send reducer type
  } catch (err) {
    yield put({ type: 'EVENTDETAIL_SEND_COMMENT_FAIL' })// send reducer type
  }
}
export default function* EventDetatilStoreWatchTodo() {
  yield all(
    [
      takeEvery('ADD_TASK', addNewTask),
      takeEvery('EVENTDETAIL_SAGA', EventDetailOnLoad),
      takeEvery('EVENTDETAIL_FOLLOW_ARTIST_SAGA', EventDetailOnFollowArtist),
      takeEvery('EVENTDETAIL_BUY_TICKET_SAGA', EvenetDetailBuyTicket),
      takeEvery('EVENTDETAIL_SEND_COMMENT_SAGA', EvenetDetailSendComment),
    ])
}

// const watchGetUserExpenses = takeEvery('ADD_TASK', loadTask)
import React, { Component } from 'react';
import { Dimensions, Text, TextInput, View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Fonts } from '../common/fonts';
import { Images } from '../common/Images';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
export class AddCommentBox extends Component {

    addComment = () => {

    }

    render() {
        return (
            <View style={styles.comment_input_view}>
                <TouchableOpacity style={styles.comment_user_icon_input_style}></TouchableOpacity>
                <View style={{ flex: 1 }}>
                <TextInput
                    style={styles.textinput_comment_style} 
                    placeholder={this.props.bought_ticket === true ? 'Comment as 5ive ' : 'Comment'} 
                    placeholderTextColor="#b2b2b2"
                    value = {this.props.value}
                    onChangeText = {this.props.commentText}
                    ref = {this.props.commentTextRef}
                ></TextInput>
                </View>
                <TouchableOpacity style={styles.POST_text_view_style} onPress={this.props.addComment}>
                    <Text style={styles.POST_text_style}>POST</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    comment_input_view: {
        backgroundColor: '#252628',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 16
    },
    comment_user_icon_input_style: {
        width: 24,
        height: 24,
        borderRadius: 24,
        backgroundColor: '#d8d8d8',
        marginVertical: 8
    },
    textinput_comment_style: {
        marginLeft: 8,
        color: '#b2b2b2'
    },
    POST_text_view_style: {
        textAlign: 'center',
        paddingLeft: 12,
        paddingVertical: 8
    },
    POST_text_style: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        color: '#dddddd',
    },
})


import React, { Component } from 'react';
import { SafeAreaView, StyleSheet, Text, ScrollView, View, Image, TouchableOpacity, ImageBackground } from 'react-native';
import { Images } from '../common/Images';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
export class Header extends Component {
    render() {
        return (
            <View style={styles.Header_view}>
                <MaterialCommunityIcons name="menu" size={30} color="#767676" />
                <Image source={Images.userIconBackground} />
                <TouchableOpacity style={styles.header_user_icon}>
                    <Image source={Images.userIconBackground} />
                    <FontAwesome5 name="user-alt" size={14} color="#d8d8d8" style={styles.userIcon} />
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    Header_view: {
        flexDirection: 'row',
        backgroundColor: '#1b1c20',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 16
    },
    header_user_icon: {
        width: 24,
        height: 24,
        borderRadius: 24,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    userIcon: {
        position: 'absolute',
        bottom: 1
    }
})


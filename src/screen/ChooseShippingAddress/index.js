import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, Pressable, KeyboardAvoidingView, TextInput } from 'react-native';
import { styles } from './styles';
import { Images } from '../../common/Images'
import Icon from 'react-native-vector-icons/dist/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import CheckBox from '@react-native-community/checkbox';
import { scrollInterpolator, animatedStyles } from '../../Utils/animations';
import Carousel from 'react-native-snap-carousel';
const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.9);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 1.2);
const CardList = [
    {
        type: 'Home',
        name: 'dev',
        address: '742 Main Street, North Oxford MA 1537 …',
        country: 'Usa'
    },
    {
        type: 'Office',
        name: 'vaibhav',
        address: '742 Main Street, North Oxford MA 1537 …',
        country: 'India'
    },
    {
        type: 'Office',
        name: 'vaibhav',
        address: '742 Main Street, North Oxford MA 1537 …',
        country: 'Canada'
    },
]
export default class ChooseShippingAddress extends Component {

    constructor(props) {
        super(props)
        this.state = {
            right: false,
            paymentMethod: 'payPal',
            cardNUmberField1: '',
            cardNUmberField2: '',
            cardNUmberField3: '',
            cardNUmberField4: '',
            cardHolderName: '',
            SelectedAmount: null,
            cvv: '',
            expiryMonth: '',
            expiryYear: '',
            saveCardAllow: true,
            savedCardList: [
                {
                    cardName: '',
                    cardNumber: '',
                    cardHolderName: '',
                    cvv: '',
                    isSelected: false
                }
            ],
            CardList: [
                {
                    type: 'Home',
                    name: 'dev',
                    address: '742 Main Street, North Oxford MA 1537 …',
                    country: 'Usa'
                },
                {
                    type: 'Office',
                    name: 'vaibhav',
                    address: '742 Main Street, North Oxford MA 1537 …',
                    country: 'India'
                },
                {
                    type: 'Office',
                    name: 'vaibhav',
                    address: '742 Main Street, North Oxford MA 1537 …',
                    country: 'Canada'
                },
            ],
            addNewCard: false
        }
    }
    SelectedAmount = (val) => {
        this.setState({ SelectedAmount: val }, () => {
            console.log("Selected Amount : " + this.state.SelectedAmount)
        })
    }
    isSelectedbtn = (index) => {
        this.setState({ selectedIndex: index });
        console.log("Selected Amount : " + index)
    }
    setPaymentMethod = (paymentMethod) => {
        this.setState({ paymentMethod })
    }

    makePayment = () => {

    }

    focusNextField = (nextField) => {
        this.refs[nextField].focus();
    }

    setCardNumber = (value, field) => {
        if (field === 1) {
            this.setState({ cardNUmberField1: value }, () => {
                this.focusNextField(field + 1)
            })
        }
        if (field === 2) {
            this.setState({ cardNUmberField2: value }, () => {
                this.focusNextField(field + 1)
            })
        }
        if (field === 3) {
            this.setState({ cardNUmberField3: value }, () => {
                this.focusNextField(field + 1)
            })
        }
        if (field === 4) {
            this.setState({ cardNUmberField4: value })
        }
    }

    setCardHodlerName = (cardHolderName) => {
        this.setState({ cardHolderName })
    }

    setExpiryMonth = (expiryMonth) => {
        this.setState({ expiryMonth }, () => {
            if (expiryMonth.length === 2)
                this.focusNextField('expiryYear')
        })
    }

    setExpiryYear = (expiryYear) => {
        this.setState({ expiryYear })
    }

    setCVV = (cvv) => {
        this.setState({ cvv })
    }

    setSaveCardValue = () => {
        this.setState({ saveCardAllow: !this.state.saveCardAllow })
    }

    startStopToAddNewCard = () => {
        this.setState({ addNewCard: !this.state.addNewCard })
    }
    submit = () => {
        this.props.navigation.navigate('TicketingScreen')

    }
    _renderItem = ({ item, index }) => {
        return (
            <View style={[styles.card_view_container, { borderWidth: this.state.right === true ? 1 : 0, borderColor: this.state.right === true ? '#fff' : null }]}>
                <View style={styles.inside_card_view_container}>
                    <View style={styles.name_n_radio_btn}>
                        <View style={styles.name_n_details_view}>
                            <Text style={styles.address_type_text_style}>{item.type}</Text>
                            <Text style={styles.user_card_name_text_style}>{item.name}</Text>
                        </View>
                        <View style={styles.radio_btn_view}>

                            {this.state.right === true ?
                                <TouchableOpacity style={styles.right_sign_view}>
                                    <Icon name='checkmark' size={16} color={'#232323'}></Icon>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => this.isSelectedbtn(index)} style={styles.round_radio_style}>
                                    <View style={[styles.selected_btn_style, { backgroundColor: this.state.selectedIndex === index ? '#ddd' : null }]}></View>
                                </TouchableOpacity>
                            }


                        </View>
                    </View>
                    <View style={styles.address_view}>
                        <Text style={styles.addresss_text_style}>{item.address}</Text>
                    </View>
                    <View style={styles.country_view}>
                        <Text style={styles.country_text_style}>{item.country}</Text>
                    </View>
                    <View style={styles.edit_n_delete_view}>
                        <TouchableOpacity>
                            <Text style={styles.edit_text}>Edit</Text>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Text style={styles.edit_text}>Delete</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

        );
    }
    render() {
        return (
            <ScrollView style={styles.ScrollView}>
                <View style={styles.main_container}>
                    <View style={styles.title_section_view}>
                        <View style={styles.title_n_close_view}>
                            <View style={styles.title_view}>
                                <Text style={styles.title_text_title_view}>Choose a Shipping Address</Text>
                            </View>
                            <View style={styles.close_btn_view}>
                                <Icon name='close' size={16} color={'#fff'}></Icon>
                            </View>
                        </View>
                        <View style={styles.decription_text_view}>
                            <Text style={styles.decription_text_style}>Choose from your saved addresses or add a new address</Text>
                        </View>
                    </View>
                    <View style={{ width: '120%', marginLeft: -10 }}>
                        <Carousel
                            ref={(c) => this.carousel = c}
                            data={this.state.CardList}
                            renderItem={this._renderItem}
                            sliderWidth={SLIDER_WIDTH}
                            itemWidth={ITEM_WIDTH}
                            inactiveSlideShift={0}
                            onSnapToItem={index => this.setState({ activeIndex: index })}
                            scrollInterpolator={scrollInterpolator}
                            slideInterpolatedStyle={animatedStyles}
                            useScrollView={true}
                        />
                    </View>
                    <Pressable onPress={this.startStopToAddNewCard}
                        style={({ pressed }) => [
                            styles.addNewCardButtonView, pressed && { backgroundColor: '#767676' }
                        ]}
                    >
                        <Pressable onPress={this.startStopToAddNewCard}
                            style={({ pressed }) => [
                                styles.addNewCardPlusButtonView, pressed && { backgroundColor: '#767676' }
                            ]}
                        >
                            <MaterialCommunityIcons name="plus" color="#ddd" size={18} />
                        </Pressable >
                        <Text style={styles.addNewCardButtonText}>Add New Address</Text>
                    </Pressable >

                    <TouchableOpacity onPress={() => this.submit()} style={styles.delivery_btn_style}>
                        <Text style={styles.btn_text_style}>Deliver to this Address</Text>
                    </TouchableOpacity>
                    <View style={styles.Disclaimer_text_view}>
                        <Text style={styles.Disclaimer_title_text_style}>Disclaimer:</Text>
                        <Text style={styles.Disclaimer_details_text_style}>Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam
                        quis nostrud exercitation ullamco laboris nisi ut
                        aliquip ex ea commodo consequat. Duis aute irure
                        dolor in reprehenderit in voluptate velit esse cillum
                        dolore eu fugiat nulla pariatur. Excepteur sint occae
                        cat cupidatat non proident, sunt in culpa qui officia</Text>
                    </View>
                </View>
            </ScrollView >
        );
    }
}
import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, ImageBackground, KeyboardAvoidingView, TextInput } from 'react-native';
import { styles } from './styles';
import { Header } from '../../components'
import { Images } from '../../common/Images'
import StepIndicator from 'react-native-step-indicator';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height


const labels = ["Select Ticket & Quantity", "Checkout", "Generate ticket",];
const customStyles = {
    stepIndicatorSize: 1,
    currentStepIndicatorSize: 10,
    separatorStrokeWidth: 1,
    currentStepStrokeWidth: 5,
    stepStrokeCurrentColor: '#fff',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#fe7013',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#fe7013',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#fe7013',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 12,
    currentStepIndicatorLabelFontSize: 12,
    stepIndicatorLabelCurrentColor: '#fe7013',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 12,
    currentStepLabelColor: '#ddd',

}

export default class SelectTicketScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            currentPosition: 1,
            index: 0,
            show: true,
            activeIndex: 0,
            sizeType: [
                { size: "S" },
                { size: "M" },
                { size: "L" },
                { size: "XL" },
                { size: "XXL" },
            ],
            selectedButton: null,
            ShowCardList: [
                {
                    ticketType: 'VIP + Ticket',
                    ConcertPoint: [{ Points: 'Live Concert Pass' }, { Points: 'Zoom Chat with the Artist after the show' }, , { Points: '48 hours VOD access' }],
                    TicketNo: 1,
                    SelectSize: null
                },
                {
                    ticketType: 'VIP + Ticket',
                    ConcertPoint: [{ Points: 'Live Concert Pass' }, { Points: 'Zoom Chat with the Artist after the show' }, , { Points: '48 hours VOD access' }],
                    TicketNo: 2,
                    SelectSize: null
                },
            ],
            RegularCardList: [
                {
                    ticketType: 'Regular',
                    ConcertPoint: [{ Points: 'Live Concert Pass' }, { Points: 'Zoom Chat with the Artist after the show' }, , { Points: '48 hours VOD access' }],
                    TicketNo: 3
                },
            ]
        }
        this.selectionOnPress = this.selectionOnPress.bind(this);
    }
    selectionOnPress(sizeType, index) {
        var ShowCardList = this.state.ShowCardList
        ShowCardList[index].SelectSize = sizeType
        this.setState({ ShowCardList });
    }

    render() {
        return (
            <KeyboardAvoidingView behaviour="height" style={styles.main_container}>
                <ScrollView contentContainerStyle={styles.scrlView_container}>
                    <Header />
                    <View style={styles.ticket_text_n_close_btn_view}>
                        <View style={styles.blank_view}></View>
                        <View style={styles.ticket_title_view}>
                            <Text style={styles.tickets_text_style}>Tickets</Text>
                        </View>
                        <View style={styles.close_btn_view}>
                            <TouchableOpacity onPress={this.goBack} style={{ marginRight: 23 }}>
                                <Ionicons name='close' size={18} color='#dddddd'></Ionicons>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ backgroundColor: '#101113', marginHorizontal: 44 }}>
                        <StepIndicator
                            customStyles={customStyles}
                            currentPosition={this.state.currentPosition}
                            labels={labels}
                            stepCount={3}
                        />
                    </View>
                    <View style={styles.main_container_view}>
                        <View style={styles.title_view}>
                            <FontAwesome name='chevron-left' size={18} color='#dddddd'></FontAwesome>
                            <Text style={styles.select_tickets_text_style}>Select Ticket & Quantity</Text>
                        </View>
                        {/* ////folding 1nd side  */}
                        {
                            this.state.ShowCardList.map((item, key) => (
                                <View style={{ marginTop: 8, flex: 1 }}>
                                    <View style={styles.folding_ticket_one_view}>
                                        <View style={styles.Left_side_view_one_side}>
                                            <View style={{ marginLeft: 18 }}>
                                                <Text style={styles.vip_n_ticket_text_style}>{item.ticketType}</Text>
                                                {
                                                    item.ConcertPoint.map((itm, idx) =>
                                                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                                                                {/* <Text style={styles.event_ticket_type_text_style}>• </Text> */}
                                                                <Text style={styles.live_concert_pass_text_style}>• {itm.Points}</Text>
                                                            </View>
                                                        </View>
                                                    )
                                                }
                                            </View>
                                        </View>
                                        <View style={styles.Right_side_view_one_side}>
                                            <View style={{ marginRight: 25, flex: 1 }}>
                                                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                                    <TouchableOpacity style={styles.dlr_btn_round_style}>
                                                        <MaterialIcons name='delete' size={10} color='#878889'></MaterialIcons>
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={{ flex: 1 }}>
                                                    <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'flex-end' }}>
                                                        <Text style={styles.tickets_count_text}>01</Text>
                                                        <Text style={styles.tickets_No_text}>Ticket No.</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    </View>

                                    <View style={styles.folding_ticket_second_view} >
                                        <View style={styles.Left_side_view_second_side}>
                                            <Image style={styles.tshirt_img} source={Images.tshirt}></Image>
                                        </View>
                                        <View style={styles.Right_side_view_second_side}>
                                            <Text style={styles.select_tshirt_text}>SELECT TSHIRT SIZE</Text>
                                            <View style={styles.size_btn_view}>

                                                {
                                                    this.state.sizeType.map((itm, idx) => {
                                                        return (
                                                            <TouchableOpacity onPress={() => this.selectionOnPress(itm.size, key)} style={[styles.select_size_btn, { backgroundColor: item.SelectSize == itm.size ? '#fff' : null }]}>
                                                                <Text style={styles.select_size_btn_text_style}>{itm.size}</Text>
                                                            </TouchableOpacity>
                                                        )
                                                    })
                                                }
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            ))
                        }
                        {
                            this.state.RegularCardList.map((item, key) => (
                                <View>
                                    <View style={styles.Regular_ticket_one_view}>
                                        <View style={styles.regular_left_side_view}>
                                            <View style={styles.inside_left_side_view}>
                                                <View style={styles.title_left_side}>
                                                    <Text style={styles.Regular_text_style}>{item.ticketType}</Text>
                                                </View>
                                                {
                                                    item.ConcertPoint.map((itm, idx) =>
                                                        <View style={styles.regular_point_view_left_side}>
                                                            {/* <Text style={styles.event_ticket_type_text_style}>• </Text> */}
                                                            <Text style={styles.live_concert_pass_text_style}>• {itm.Points}</Text>
                                                        </View>
                                                    )
                                                }
                                            </View>

                                        </View>
                                        <View style={styles.regular_right_side_view}>
                                            <View style={styles.inside_right_side_view}>
                                                <View style={styles.regular_right_top_view}>
                                                    <TouchableOpacity style={styles.dlr_btn_round_style}>
                                                        <MaterialIcons name='delete' size={10} color='#878889'></MaterialIcons>
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={styles.regular_right_bottom_view}>
                                                    <Text style={styles.tickets_count_text}>{item.TicketNo}</Text>
                                                    <Text style={styles.tickets_No_text}>Ticket No.</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.checkout_view}>
                                        <TouchableOpacity style={styles.checkout_btn_style}
                                            onPress={() => {
                                                this.props.navigation.navigate('ChooseShippingAddress')

                                            }}
                                        >
                                            <Text style={styles.checkout_text_style}>Continue</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            ))
                        }
                    </View>
                </ScrollView>
            </KeyboardAvoidingView >
        );
    }
}


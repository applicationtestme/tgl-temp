import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, Pressable, KeyboardAvoidingView, TextInput, ImageBackground } from 'react-native';
import { styles } from './styles';
import { Images } from '../../../../common/Images'
import Icon from 'react-native-vector-icons/dist/Ionicons';
import CheckBox from '@react-native-community/checkbox';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Carousel from 'react-native-snap-carousel';
import { scrollInterpolator, animatedStyles } from '../../../../Utils/animations';
const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.9);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 1.2);

const cardDetailListData = [0, 0, 0, 0, 0, 0]
const cardDetailList = [
    {
        bankName: 'Bank of America',
        cardNo: '1020',
        cardHolderName: 'VAIBHAV KHARAT',
        isSelect: 0
    },
    {
        bankName: 'Bank of America',
        cardNo: '1020',
        cardHolderName: 'VAIBHAV KHARAT',
        isSelect: 0
    },
    {
        bankName: 'Bank of America',
        cardNo: '1020',
        cardHolderName: 'VAIBHAV KHARAT',
        isSelect: 0
    },
    // {
    //     bankName: 'Bank of America',
    //     cardNo: '1020',
    //     cardHolderName: 'VAIBHAV KHARAT',
    //     isSelect:0
    // },
    // {
    //     bankName: 'Bank of America',
    //     cardNo: '1020',
    //     cardHolderName: 'VAIBHAV KHARAT',
    //     isSelect:1
    // },
    // {
    //     bankName: 'Bank of America',
    //     cardNo: '1020',
    //     cardHolderName: 'VAIBHAV KHARAT',
    //     isSelect:0
    // }
]

export class TshirtPaymnet extends Component {

    constructor(props) {
        super(props)
        this.state = {
            paymentMethod: 'card',
            cardNUmberField1: '',
            cardNUmberField2: '',
            cardNUmberField3: '',
            cardNUmberField4: '',
            cardHolderName: '',
            cvv: '',
            expiryMonth: '',
            expiryYear: '',
            saveCardAllow: true,
            cardDetailList: [
                {
                    bankName: 'Bank of America',
                    cardNo: '1020',
                    cardHolderName: 'VAIBHAV KHARAT',
                    isSelect: 0
                },
                {
                    bankName: 'Bank of America',
                    cardNo: '1020',
                    cardHolderName: 'VAIBHAV KHARAT',
                    isSelect: 0
                },
                {
                    bankName: 'Bank of America',
                    cardNo: '1020',
                    cardHolderName: 'VAIBHAV KHARAT',
                    isSelect: 0
                },
                // {
                //     bankName: 'Bank of America',
                //     cardNo: '1020',
                //     cardHolderName: 'VAIBHAV KHARAT',
                //     isSelect:0
                // },
                // {
                //     bankName: 'Bank of America',
                //     cardNo: '1020',
                //     cardHolderName: 'VAIBHAV KHARAT',
                //     isSelect:1
                // },
                // {
                //     bankName: 'Bank of America',
                //     cardNo: '1020',
                //     cardHolderName: 'VAIBHAV KHARAT',
                //     isSelect:0
                // }
            ],

        }
    }

    setPaymentMethod = (paymentMethod) => {
        this.setState({ paymentMethod })
    }

    makePayment = () => {

    }

    focusNextField = (nextField) => {
        this.refs[nextField].focus();
    }

    setCardNumber = (value, field) => {
        if (field === 1) {
            this.setState({ cardNUmberField1: value }, () => {
                this.focusNextField(field + 1)
            })
        }
        if (field === 2) {
            this.setState({ cardNUmberField2: value }, () => {
                this.focusNextField(field + 1)
            })
        }
        if (field === 3) {
            this.setState({ cardNUmberField3: value }, () => {
                this.focusNextField(field + 1)
            })
        }
        if (field === 4) {
            this.setState({ cardNUmberField4: value })
        }
    }

    setCardHodlerName = (cardHolderName) => {
        this.setState({ cardHolderName })
    }

    setExpiryMonth = (expiryMonth) => {
        this.setState({ expiryMonth }, () => {
            if (expiryMonth.length === 2)
                this.focusNextField('expiryYear')
        })
    }

    setExpiryYear = (expiryYear) => {
        this.setState({ expiryYear })
    }

    setCVV = (cvv) => {
        this.setState({ cvv })
    }

    setSaveCardValue = () => {
        this.setState({ saveCardAllow: !this.state.saveCardAllow })
    }

    HandelCardPayment = (index) => {
        var cardDetailList = this.state.cardDetailList
        for (var i = 0; i < cardDetailList.length; i++) {
            if (index == i) {
                cardDetailList[i].isSelect = 1
            } else {
                cardDetailList[i].isSelect = 0
            }
        }
        this.setState({ cardDetailList })
    }

    _renderItem = ({ item, index }) => {
        return (
            <ImageBackground
                style={{ width: '100%', borderRadius: 10, overflow: 'hidden', marginTop: 13 }}
                source={Images.gradientCard}>
                <View style={{ marginTop: 16, marginHorizontal: 10, flexDirection: 'row', }}>
                    <View style={{ flex: 4, flexDirection: 'row', }}>
                        <View style={styles.round_selected_btn}>
                            <TouchableOpacity
                                onPress={() => { this.HandelCardPayment(index) }}
                                style={styles.round_radio_style}>
                                <View style={[styles.selected_btn_style, { backgroundColor: item.isSelect == 1 ? '#ddd' : null }]}></View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 1, marginLeft: 16 }}>
                            <Text style={styles.selectedCardStyle} >{item.bankName} </Text>
                            <Text style={styles.selectedCardStyle} >**** {item.cardNo}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-end', marginRight: 13 }}>
                        <MaterialCommunityIcons name="dots-vertical" color="#ddd" size={25} />
                    </View>
                </View>
                <View style={{ flex: 1, flexDirection: 'row', marginTop: 0, marginBottom: 5 }}>
                    <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', marginBottom: 10 }}>
                        <View style={{ marginRight: 10 }}>
                            <Image
                                style={{ height: 60, width: 100 }}
                                source={Images.visaLogo} />
                            <Text style={styles.cardTextAplicantNameStyle}>{item.cardHolderName}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <View style={{ borderWidth: 1, borderRadius: 10, marginTop: 20, paddingHorizontal: 35, borderColor: "#fff", alignItems: 'center', justifyContent: 'center', marginBottom: 23 }}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                                <TextInput style={{}} placeholder={'CVC'} />
                            </View>
                        </View>
                    </View>
                </View>
            </ImageBackground>
            // </View>
        )
    }

    render() {
        const { address } = this.props;
        const {
            paymentMethod, cardNUmberField1, cardNUmberField2, cardNUmberField3, cardNUmberField4,
            cardHolderName, cvv, expiryMonth, expiryYear, saveCardAllow
        } = this.state;
        return (
            <View>
                <View style={styles.addressDisplayView}>
                    <View style={styles.addressDetails}>
                        <Text style={styles.addressTitle}>Deliver to</Text>
                        <Text style={styles.addressValueStyle}>{address}</Text>
                    </View>
                    <Pressable onPress={this.props.onAddressChange}
                        style={({ pressed }) => [
                            styles.changeButton, pressed && { backgroundColor: '#767676' }
                        ]}
                    >
                        <Text style={styles.changeButtonText}>Change</Text>
                    </Pressable >
                </View>
                <View style={styles.paymentContainer}>
                    <View style={styles.paymentHeader}>
                        <Text style={styles.addressTitle}>Select Payment Method</Text>
                    </View>
                    <View style={styles.paymentMethodsView}>
                        <TouchableOpacity style={styles.paymentItem} onPress={() => this.setPaymentMethod('card')}>
                            <View style={styles.circleStyle}>
                                <Icon name="checkmark-sharp" color="#000" />
                            </View>
                            <Text style={styles.paymentMethodName}>Credit Card</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.paymentItem} onPress={() => this.setPaymentMethod('payPal')}>
                            <Image source={Images.paypalTabLogo} style={{ height: 20, width: 30 }} />
                            <Text style={styles.paymentMethodName}>Paypal</Text>
                        </TouchableOpacity>
                        {
                            paymentMethod === 'payPal' &&
                            <View style={styles.paypalMainContainer}>
                                <Text style={styles.addressTitle}>Pay using PayPal</Text>
                                <Pressable onPress={this.submit}
                                    style={({ pressed }) => [
                                        styles.paypalButton, pressed && { backgroundColor: '#767676' }
                                    ]}
                                >
                                    <Image source={Images.paypalLogo} style={styles.paypalLogoStyle}></Image>
                                </Pressable >
                            </View>
                        }
                        {
                            paymentMethod === 'card' &&
                            <View style={styles.cardDesignView}>
                                <Text style={styles.cardSavedCardTxt}>Saved Card</Text>
                                <View style={{ width: '10%', marginLeft: -10 }}>
                                    <Carousel
                                        ref={(c) => this.carousel = c}
                                        data={this.state.cardDetailList}
                                        renderItem={this._renderItem}
                                        sliderWidth={SLIDER_WIDTH - 20}
                                        itemWidth={ITEM_WIDTH}
                                        inactiveSlideShift={0}
                                        onSnapToItem={index => this.setState({ activeIndex: index })}
                                        scrollInterpolator={scrollInterpolator}
                                        slideInterpolatedStyle={animatedStyles}
                                        useScrollView={true}
                                    />
                                </View>

                                <View style={styles.addCardView}>
                                    <View style={styles.addCardHeader}>
                                        <Text style={styles.cardHeaderText}>Add Card</Text>
                                        <TouchableOpacity style={styles.crossButtonStyle}>
                                            <Icon name="close" color="#fff" size={20} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.cardView}>
                                        <Text style={styles.cardNameStyle}>Bank of America</Text>
                                        <Text style={styles.cardLabelStyle}>CARD NUMBER</Text>
                                        <View style={styles.cardNumberInputsview}>
                                            <TextInput
                                                ref="1"
                                                keyboardType={'number-pad'}
                                                style={styles.textBoxStyle}
                                                maxLength={1}
                                                value={cardNUmberField1}
                                                onChangeText={(value) => this.setCardNumber(value, 1)}
                                            />
                                            <TextInput
                                                ref="2"
                                                keyboardType={'number-pad'}
                                                style={styles.textBoxStyle}
                                                maxLength={1}
                                                value={cardNUmberField2}
                                                onChangeText={(value) => this.setCardNumber(value, 2)}
                                            />
                                            <TextInput
                                                ref="3"
                                                keyboardType={'number-pad'}
                                                style={styles.textBoxStyle}
                                                maxLength={1}
                                                value={cardNUmberField3}
                                                onChangeText={(value) => this.setCardNumber(value, 3)}
                                            />
                                            <TextInput
                                                ref="4"
                                                keyboardType={'number-pad'}
                                                style={styles.textBoxStyle}
                                                maxLength={1}
                                                value={cardNUmberField4}
                                                onChangeText={(value) => this.setCardNumber(value, 4)}
                                            />

                                        </View>
                                        <Text style={styles.cardLabelStyle}>CARD HOLDER NUMBER</Text>
                                        <TextInput
                                            style={styles.textBoxStyle}
                                            value={cardHolderName}
                                            onChangeText={(value) => this.setCardHodlerName(value)}
                                        />

                                        <View style={styles.cardValidationDetails}>
                                            <View style={[styles.cardValidationItem, { flex: 1 }]}>
                                                <Text style={styles.cardValidationLabelStyle}>CVV</Text>
                                                <TextInput
                                                    style={styles.textBoxStyle}
                                                    maxLength={3}
                                                    keyboardType={'number-pad'}
                                                    value={cvv}
                                                    onChangeText={(value) => this.setCVV(value)}
                                                />
                                            </View>
                                            <View style={styles.cardValidationItem}>
                                                <Text style={styles.cardValidationLabelStyle}>EXPIRES</Text>
                                                <View style={styles.expiryDateStyle}>
                                                    <TextInput
                                                        ref="expiryMonth"
                                                        placeholder="MM"
                                                        placeholderTextColor="gray"
                                                        maxLength={2}
                                                        keyboardType={'number-pad'}
                                                        style={styles.textBoxStyle}
                                                        value={expiryMonth}
                                                        onChangeText={(value) => this.setExpiryMonth(value)}
                                                    />
                                                    <Text style={styles.dateSlashStyle}>/</Text>
                                                    <TextInput
                                                        ref="expiryYear"
                                                        placeholder="YY"
                                                        placeholderTextColor="gray"
                                                        maxLength={2}
                                                        keyboardType={'number-pad'}
                                                        style={styles.textBoxStyle}
                                                        value={expiryYear}
                                                        onChangeText={(value) => this.setExpiryYear(value)}
                                                    />
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.saveCardView}>
                                        <CheckBox
                                            disabled={false}
                                            value={saveCardAllow}
                                            onValueChange={this.setSaveCardValue}
                                            tintColors={{ true: '#4b4c4e' }}
                                        />
                                        <Text style={styles.cardValidationLabelStyle}>Save this Card for future Payments</Text>
                                    </View>
                                </View>
                            </View>
                        }
                    </View>
                </View>
                <View style={styles.paymentCalculationView}>
                    <View style={styles.calculationRow}>
                        <Text style={styles.calculationLeftText}>Subtotal</Text>
                        <Text style={styles.calculationRightText}>$55</Text>
                    </View>
                    <View style={styles.calculationRow}>
                        <Text style={[styles.calculationLeftText, { flex: 0 }]}>Tax</Text>
                        <Icon name="help-circle-outline" color="#ddd" size={20} style={styles.helpIconStyle} />
                        <Text style={styles.calculationRightText}>$5</Text>
                    </View>
                    <View style={[styles.calculationRow, styles.finalTotalStyle]}>
                        <Text style={styles.calculationLeftText}>Total</Text>
                        <Text style={styles.calculationRightTotalText}>$60</Text>
                    </View>
                </View>

                {
                    paymentMethod === 'card' &&
                    <Pressable onPress={this.makePayment}
                        style={({ pressed }) => [
                            styles.makePaymentButton, pressed && { backgroundColor: '#767676' }
                        ]}
                    >
                        <Text style={styles.makePaymentButtonText}>Make Payment</Text>
                    </Pressable >
                }

            </View>
        );
    }
}
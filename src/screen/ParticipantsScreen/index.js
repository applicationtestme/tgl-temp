import React, { Component } from 'react';
import { InteractionManager, Dimensions, Text, ScrollView, View, Image, TouchableOpacity, ImageBackground, KeyboardAvoidingView, TextInput } from 'react-native';
import { styles } from './styles';
import { Header, UserRow, GettingReady } from '../../components'
import { Images } from '../../common/Images';
import { Fonts } from '../../common/fonts';
import Icon from 'react-native-vector-icons/dist/Ionicons';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export default class ParticipantsScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            all_tab: true,
            vip_tab: false,
            following_tab: false,
            usersList: [
                {
                    userName: 'Anuja',
                    badges: 19,
                    address: 'Tennessey, USA',
                    isVip: 1,
                    follow: 0
                },
                {
                    userName: 'Bruce',
                    badges: 21,
                    address: 'Kolkata, India',
                    isVip: 0,
                    follow: 1
                },
                {
                    userName: 'Anuja',
                    badges: 2,
                    address: 'Tennessey, USA',
                    isVip: 1,
                    follow: 0
                },
                {
                    userName: 'Anuja',
                    badges: 31,
                    address: 'Tennessey, USA',
                    isVip: 0,
                    follow: 0
                },
                {
                    userName: 'Anuja',
                    badges: 19,
                    address: 'Tennessey, USA',
                    isVip: 1,
                    follow: 0
                },
                {
                    userName: 'Bruce',
                    badges: 21,
                    address: 'Kolkata, India',
                    isVip: 0,
                    follow: 1
                },
                {
                    userName: 'Anuja',
                    badges: 2,
                    address: 'Tennessey, USA',
                    isVip: 1,
                    follow: 0
                },
                {
                    userName: 'Anuja',
                    badges: 31,
                    address: 'Tennessey, USA',
                    isVip: 0,
                    follow: 1
                },
                {
                    userName: 'Anuja',
                    badges: 19,
                    address: 'Tennessey, USA',
                    isVip: 1,
                    follow: 0
                },
                {
                    userName: 'Bruce',
                    badges: 21,
                    address: 'Kolkata, India',
                    isVip: 0,
                    follow: 0
                },
                {
                    userName: 'Anuja',
                    badges: 2,
                    address: 'Tennessey, USA',
                    isVip: 1,
                    follow: 0
                },
                {
                    userName: 'Anuja',
                    badges: 31,
                    address: 'Tennessey, USA',
                    isVip: 0,
                    follow: 1
                },
                {
                    userName: 'Anuja',
                    badges: 19,
                    address: 'Tennessey, USA',
                    isVip: 1,
                    follow: 0
                },
                {
                    userName: 'Bruce',
                    badges: 21,
                    address: 'Kolkata, India',
                    isVip: 0,
                    follow: 0
                },
                {
                    userName: 'Anuja',
                    badges: 2,
                    address: 'Tennessey, USA',
                    isVip: 1,
                    follow: false
                },
                {
                    userName: 'Anuja',
                    badges: 31,
                    address: 'Tennessey, USA',
                    isVip: 0,
                    follow: 0
                }
            ],
            isReady: false
        }
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            this.setState({
                isReady: true
            })
        });
    }

    all_tab_view = () => {
        this.setState({ all_tab: true, vip_tab: false, following_tab: false });
    }
    vip_tab_view = () => {
        this.setState({ vip_tab: true, all_tab: false, following_tab: false });
    }
    following_tab_view = () => {
        this.setState({ following_tab: true, vip_tab: false, all_tab: false });
    }
    onfollowbtn = (user, index) => {
        var usersList = this.state.usersList
        usersList[index].follow = 1
        this.setState({ usersList })
    }

    goBack = () => {
        this.props.navigation.goBack()
    }

    render() {
        const { usersList } = this.state
        if (!this.state.isReady) {
            return (
                <GettingReady />
            )
        } else {
            return (
                <KeyboardAvoidingView behaviour="height" style={styles.main_container}>
                    <Header />
                    <View style={styles.participants_title_container}>
                        <View style={styles.blank_space_view_title}>
                        </View>
                        <View style={styles.title_name_n_chat_tag_view}>
                            <Text style={styles.participants_text_style}>Participants</Text>
                            <Text style={styles.global_chat_title_text_style}>Global Chat</Text>
                        </View>
                        <View style={styles.close_btn_view}>
                            <TouchableOpacity onPress={this.goBack} style={styles.close_btn_round_view_style}>
                                <Icon name='close' size={18} color='#dddddd'></Icon>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.participants_title_container}>
                        <TouchableOpacity onPress={() => this.all_tab_view()}
                            style={[styles.tab_btn_style, { borderBottomWidth: this.state.all_tab === true && this.state.vip_tab === false && this.state.following_tab === false ? 3 : null, fontFamily: this.state.all_tab === true && this.state.vip_tab === false && this.state.following_tab === false ? Fonts.OpenSans_semibold : Fonts.OpenSans_regular, backgroundColor: this.state.all_tab === true && this.state.vip_tab === false && this.state.following_tab === false ? '#373739' : null }]}>
                            <Text style={styles.tab_btn_text_style}>All (14)</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.vip_tab_view()} style={[styles.tab_btn_style, { borderBottomWidth: this.state.vip_tab === true && this.state.all_tab === false && this.state.following_tab === false ? 3 : null, fontFamily: this.state.vip_tab === true && this.state.all_tab === false && this.state.following_tab === false ? Fonts.OpenSans_semibold : Fonts.OpenSans_regular, backgroundColor: this.state.vip_tab === true && this.state.all_tab === false && this.state.following_tab === false ? '#373739' : null }]}>
                            <Text style={styles.tab_btn_text_style}>VIP (15)</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.following_tab_view()} style={[styles.tab_btn_style, { borderBottomWidth: this.state.following_tab === true && this.state.all_tab === false && this.state.vip_tab === false ? 3 : null, fontFamily: this.state.following_tab === true && this.state.all_tab === false && this.state.vip_tab === false ? Fonts.OpenSans_semibold : Fonts.OpenSans_regulars, backgroundColor: this.state.following_tab === true && this.state.all_tab === false && this.state.vip_tab === false ? '#373739' : null }]}>
                            <Text style={styles.tab_btn_text_style}>Following (3)</Text>
                        </TouchableOpacity>
                    </View>
                    <ScrollView contentContainerStyle={styles.scrlView_container}>
                        {
                            usersList.map((user, index) => (
                                <UserRow user={user} participants_screens={true} onfollow={() => this.onfollowbtn(user, index)} />
                            ))
                        }

                    </ScrollView>
                </KeyboardAvoidingView>
            );
        }
    }
}


import EventDetatilStoreWatchTodo from './EventDetatilStore/saga'

import { all, fork } from 'redux-saga/effects';
export default function* rootSaga() {
  yield all([
    fork(EventDetatilStoreWatchTodo),
  ]);
};
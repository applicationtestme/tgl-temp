import { call, put, takeEvery, takeLatest, all } from 'redux-saga/effects'
import { delay } from 'redux-saga'
export function* addNewTask(action) {
  try {
    yield put({ type: 'IS_FETCHING' })
    const response = yield call(axios.post, 'http://jsonplaceholder.typicode.com/todos/', {
      userId: 7,
      title: action.newTask,
      completed: false,
    })
    yield put({ type: 'ADD_TASK_SUCCESSED', newTask: action.newTask })
  } catch (err) {
    yield put({ type: 'LOAD_FAIL' })
  }
}

export function callApi(page) {
  return fetch(`https://reqres.in/api/users?page=${page}`)
    .then(res => res.json())
    .then(response => { return response })
}

export function* loadTask(action) {
  try {
    let userData = yield call(callApi, 2);
    alert(JSON.stringify(userData))
    yield put({ type: 'LOAD_FAIL', userData })
  } catch (err) {
    yield put({ type: 'LOAD_FAIL' })
  }
}
export default function* watchTodo() {
  yield all([takeEvery('ADD_TASK', addNewTask), takeEvery('LOAD_TASK', loadTask)])
}

// const watchGetUserExpenses = takeEvery('ADD_TASK', loadTask)
import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Images } from '../common/Images'
import { Fonts } from '../common/fonts';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
export class UserChatActionButtons extends Component {
    doActionForChat = () => {

    }

    render() {
        return (
            <View style={styles.userChatActionsContainer}>
                <TouchableOpacity onPress={this.props.SendTip} style={styles.chatActionButton}>
                    <Image source={Images.moneyBagIcon} />
                    <Text style={styles.chatActionButtonText}>Tip</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.doActionForChat} style={styles.chatActionButton}>
                    <Image source={Images.clappingIcon} />
                    <Text style={styles.chatActionButtonText}>Clap</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.doActionForChat} style={styles.chatActionButton}>
                    <Image source={Images.whistleIcon} />
                    <Text style={styles.chatActionButtonText}>Whistle</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    userChatActionsContainer: {
        paddingHorizontal: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderTopColor: '#979797',
        borderTopWidth: 1,
        paddingVertical: 8,
        backgroundColor: '#0c0d0f'
    },
    chatActionButton: {
        width: (width - 52) / 3,
        backgroundColor: '#252628',
        paddingVertical: 5,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        borderRadius: 35
    },
    chatActionButtonText: {
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 12,
        color: '#fff',
        marginLeft: 5
    },
})


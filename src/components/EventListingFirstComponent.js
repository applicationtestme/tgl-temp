import React, { Component } from 'react';
import { Dimensions, Text, ScrollView, View, Image, TouchableOpacity, StyleSheet, ImageBackground } from 'react-native';
import { Fonts } from '../common/fonts';
import LinearGradient from 'react-native-linear-gradient';
import { Images } from '../common/Images';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
export class EventListingFirstComponent extends Component {
    constructor() {
        super()

    }
    render() {
        return (
            this.props.EventBannerTypeData.map((item, index) => {
                return (
                    <View style={styles.event_for_view_container}>

                        <ImageBackground source={Images.lady_gaga_concer} style={styles.lady_gaga_concert_img_bg_style}>
                            <LinearGradient colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0,0.35)', '#000']} style={styles.linearGradient2}>
                                <View style={{ marginLeft: 25, }}>

                                    <View style={styles.artist_name_n_concert_name_container}>
                                        <View>
                                            <Image style={styles.lady_gaga_user_pic_style} source={{ uri: item.artistProfileImage }}></Image>
                                        </View>
                                        <View style={{ alignItems: 'flex-start', marginTop: 10 }}>
                                            <Text style={styles.artist_name_title_text_event_for_you_style}>{item.artistName}</Text>
                                            <Text style={styles.concert_title_text_style}>{item.eventName}</Text>
                                            <Text style={styles.concert_timing_text_style}>{item.eventTime}</Text>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ flex: 0 }}>
                                                    <TouchableOpacity style={styles.buy_tickets_concert_artist_btn_style}>
                                                        <Text style={styles.buy_ticket_concert_artist_text_style}>Buy Tickets</Text>
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={{ marginLeft: '40%' }}>
                                                    <TouchableOpacity style={{}}>
                                                        <Image style={{}} source={Images.right_arrow_padding}></Image>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </LinearGradient>
                        </ImageBackground>
                    </View>
                )
            })
        )
    }
}
const styles = StyleSheet.create({
    lady_gaga_concert_img_bg_style: {
        backgroundColor: 'transparent',
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    artist_name_title_text_event_for_you_style: {
        color: '#dddddd',
        fontFamily: Fonts.OpenSans_regular,
        fontSize: 10,
        marginTop: -15
    },
    linearGradient2: {
        opacity: 1,
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    artist_name_n_concert_name_container: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginTop: 75
    },
    lady_gaga_user_pic_style: {
        marginBottom: 24,
    },
    concert_title_text_style: {
        fontFamily: Fonts.PlayfairDisplay_Bold,
        color: '#dddddd',
        fontSize: 16,
    },
    concert_timing_text_style: {
        fontFamily: Fonts.OpenSans_regular,
        color: '#dddddd',
        fontSize: 8,
        marginTop: 3,
        marginBottom: 8
    },
    buy_tickets_concert_artist_btn_style: {
        backgroundColor: '#d8d8d8',
        borderRadius: 5,
    },
    buy_ticket_concert_artist_text_style: {
        color: '#1b1c20',
        fontFamily: Fonts.OpenSans_semibold,
        fontSize: 10,
        marginVertical: 8,
        marginHorizontal: 30
    },
    event_for_view_container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 16,
        height: 184
    },
})


